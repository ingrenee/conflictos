<?php
class Monitoreo_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_for_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.monitoreo')->row_array();
	}

	public function get_by_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.monitoreo')->row_array();
	}

	function get_by_conflicto($conflicto_id) {
		$w['conflictos_id'] = $conflicto_id;

		return $this->db->select('*')->where($w)->order_by('fecha', 'asc')->get('conflictos.monitoreo')->result_array();
	}

}

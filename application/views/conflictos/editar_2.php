<h4>Analisis del conflicto/denuncia</h4>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Codigo:</label>
	<div class="col-sm-4">
		<input type="text" name="codigo" readonly="true" value="<?php _vi($row, 'codigo');?>" id="codigo" data-rule-required="true" class="form-control" value=""   title="">
	</div>
	<label class="control-label col-sm-2" for="email">Estado:</label>
	<div class="col-sm-3">
		<select name="estado" id="estado" class="form-control" data-rule-required="true">
			<option value="">Seleccione</option>

			 <?php foreach ($estados as $key => $value): ?>
			<option  value="<?php echo $value['id']; ?>" <?php _vs($row, 'estado', $value['id']);?>>  <?php echo $value['nombre']; ?> </option>
			 <?php endforeach?>


		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Nombre </label>
	<div class="col-sm-9">
		<input type="text" data-rule-required="true"  value="<?php _vi($row, 'nombre');?>" class="form-control input-sm" id="nombre" name="nombre" placeholder="">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Departamento</label>
	<div class="col-sm-3">
		<select name="departamento_cod" id="departamento" class="form-control input-sm" data-rule-required="true" >
			<option>Seleccione</option>
			<?php foreach ($departamentos as $k => $v) {
	?>
			<option   value="<?PHP echo $k; ?>" <?php _vs($row, 'departamento_cod', $k);?>> <?PHP echo $v; ?> </option>
			<?PHP
}?>
		</select>
	</div>
	<label class="control-label col-sm-1 col-xs-12" for="email">Provincia</label>
	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		<select name="provincia_cod" id="provincia" class="form-control input-sm"  data-rule-required="true" >
			<option value="">seleccione</option>
			<?php foreach ($provincia as $k => $v) {
	?>
			<option   value="<?PHP echo $k; ?>" <?php _vs($row, 'provincia_cod', $k);?>> <?PHP echo $v; ?> </option>
			<?PHP
}?>
		</select>
	</div>
	<label class="control-label col-sm-1 col-xs-12" for="email">Distrito</label>
	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		<select name="distrito_cod" id="distrito" class="form-control input-sm"  data-rule-required="true" >
			<option value="">seleccione</option>
			<?php foreach ($distrito as $k => $v) {
	?>
			<option   value="<?PHP echo $k; ?>" <?php _vs($row, 'distrito_cod', $k);?>> <?PHP echo $v; ?> </option>
			<?PHP
}?>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Comunidad:</label>
	<div class="col-sm-4">
		<input type="text" name="comunidad" value="<?php _vi($row, 'comunidad');?>" class="form-control input-sm" data-rule-required="true"  id="" placeholder="">
	</div>
	<label class="control-label col-sm-2" for="email">Fecha:</label>
	<div class="col-sm-3">
		<input type="text" name="fecha" id="fecha" maxlength="10" minlength="10" data-rule-maxlength=10 data-rule-minlength=10 data-rule-validDate="true"   class="fecha form-control input-sm"  data-rule-required="true"  value="<?php
$row['fecha'] = date('d/m/Y', strtotime($row['fecha']));
_vi($row, 'fecha');?>" title="">
	</div>
</div>
<h4>Perfil del conflicto </h4>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Antecedentes:</label>
	<div class="col-sm-9">
		<input type="text" name="antecedentes" value="<?php _vi($row, 'antecedentes');?>" data-rule-required="true"  class="form-control input-sm" id="antecedentes" placeholder="">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Situacion Actual:</label>
	<div class="col-sm-9">
		<input type="text" name="situacion_actual"  value="<?php _vi($row, 'situacion_actual');?>" class="form-control input-sm" id="situacion_actual" data-rule-required="true"  placeholder="">
	</div>
</div>
<hr>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Hechos:</label>
	<div class="col-sm-9">

		<ol id="hechos_clonar_content">

 <?php if (count($hechos) > 0): ?>



		 <?php foreach ($hechos as $k => $v): ?>

<?php

$v['hechos_fecha'] = date('d/m/Y', strtotime($v['hechos_fecha']));

?>
		 		<li id="hechos_clonar" class="hechos_clon">
				<div class="row">
			<input name="hechos_id[]" type="hidden" value="<?php echo $v['id']; ?>">
					<div class="col-sm-1">
						<a href="javascript:void(0);" data-id="<?php echo $v['id']; ?>" class="btn btn-danger boton_delete_hechos pull-right"> Quitar </a>
					</div>
					<div class="col-sm-8">
						<input name="hechos_descripcion[]" placeholder="Descripción" value="<?php echo $v['hechos_descripcion']; ?>" type="text" class="form-control">
					</div>
					<div class="col-sm-3">
						<input type="text" name="hechos_fecha[]" placeholder="fecha" id="input" class="fecha  datepicker_recurring_start form-control" value="<?php echo $v['hechos_fecha']; ?>" >
					</div>
					<div class="clearfix"></div>
				</div>
			</li>

		 <?php endforeach?>

<?php else: ?>


		<li id="hechos_clonar" class="hechos_clon">
				<div class="row">
					<div class="col-sm-1">
						<a href="javascript:void(0);" class="btn btn-danger boton_quitar_hechos pull-right"> Quitar </a>
					</div>
					<div class="col-sm-8">
						<input name="hechos_descripcion[]" placeholder="Descripción" value="" type="text" class="form-control">
					</div>
					<div class="col-sm-3">
						<input type="text" name="hechos_fecha[]" placeholder="fecha" id="input" class="fecha  datepicker_recurring_start form-control" value="" >
					</div>
					<div class="clearfix"></div>
				</div>
			</li>
		 <?php endif?>
		</ol>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-10">
				<a href="#" class="btn btn-default" id="boton_clonar_hechos"> Agregar Hecho </a>
			</div>
		</div>
	</div>
</div>
<hr>

<div class="form-group">
	<label class="control-label col-sm-3" for="email">Tipo:</label>
	<div class="col-sm-9">

 <?php foreach ($tipos as $k => $v): ?>
 	<div class="checkbox">
 		<label>
 			<input type="checkbox" name="tipos[]" <?php

if (in_array($v['id'], $mis_tipos)):
	echo "checked";
endif;
?> value="<?php echo $v['id']; ?>">
 			<?php echo $v['nombre']; ?>
 		</label>
 	</div>
 <?php endforeach?>


	</div>
</div>


<div class="form-group">
	<label class="control-label col-sm-3" for="email">Problema:</label>
	<div class="col-sm-9">
		<input type="text" name="problema" value="<?php _vi($row, 'problema');?>" id="problema" class="form-control input-sm" value="" data-rule-required="true" >
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Poblacion afectada:</label>
	<div class="col-sm-4">
		<input  type="text" data-rule-number="true" name="poblacion_afectada" id="poblacion_afectada" class="form-control input-sm input-sm" value="<?php _vi($row, 'poblacion_afectada');?>" rows="3" data-rule-required="true" >
	</div>
	<label class="control-label col-sm-2" for="email">Demanda:</label>
	<div class="col-sm-3">
		<input type="text" name="demanda" value="<?php _vi($row, 'demanda');?>" id="demanda" class="form-control input-sm input-sm" value="" data-rule-required="true" >
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Causas:</label>
	<div class="col-sm-9">
		<input type="text" name="causas" id="causas" value="<?php _vi($row, 'causas');?>" class="form-control input-sm input-sm" data-rule-required="true" >
	</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-default" id="boton_form_2_back">Volver</button>
	</div>
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_2">Continuar</button>
	</div>
</div>




<table class="table table-condensed table-striped">

	<thead>

		<tr> <td> DNI </td> 

			<td> Nombres  </td> 

			<td> Consejo Regional </td> 

			<td> Nucleo local </td> 
			<td> Periodo actual </td> 
			<td> Estado (Abierto/cerrado) </td> 
			<td>   </td> 

			<td>   </td> 
		</tr>
	</thead>

	<tbody>



		<?php  


		foreach ($rows as $k => $v):


			?>

		<tr> <td> <?php  echo $v['productores_dni']; ?> </td> 

			<td> <?php  echo $v['nombres']; ?> (<?php  echo $v['codigo'] ?>) </td> 

			<td> <?php  echo $v['consejos_regionales_nombre']; ?> </td> 
			

			<td> <?php  echo $v['nucleos_nombre']; ?> </td> 
			<td> <?php  echo $v['periodos_nombre']; ?> </td> 
			<td> <?php  if( (int)$v['estado'] != 1): ?>

				Abierto 
			<?php  else: ?>
			Cerrado
		<?php  endif; ?>
	</td> 

	<td> <a href="<?php echo site_url('plan_produccion/index/'.$v['id']); ?>" class="btn btn-primary btn-xs">Editar </a> </td> 
</tr>

<?php
endforeach;
?>
</tbody>
</table>
<h4>Analisis del conflicto/denuncia</h4>
<!-- ############################### -->
<div class="form-group">
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label  -sm-3" for="email">Codigo:</label>
			<font><?php _vi($row, 'codigo');?></font>
		</div>
		<div class="col-sm-6">
			<label class="control-label  -sm-2" for="email">Estado:</label>
			<font>
				<?php foreach ($estados as $key => $value):
	if ($row['estado'] == $value['id']):
		echo $value['nombre'];
	endif;
endforeach;?>
			</font>
		</div>
	</div>
</div>
<!-- ############################### -->
<div class="row">
	<div class="col-sm-12">
		<label class="control-label  -sm-3" for="email">Nombre: </label>
		<font><?php _vi($row, 'nombre');?></font>
	</div>
</div>
<!-- ############################### -->
<div class="row">
	<div class="col-sm-3">
		<label class="control-label  -sm-3" for="email">Departamento:</label>
		<font><?php foreach ($departamentos as $k => $v) {
	if ($row['departamento_cod'] == $k):
		echo $v;
	endif;
}?></font>

	</div>
	<div class="col-sm-3">
		<label class="control-label  -sm-1  2" for="email">Provincia:</label>

<font>		<?php foreach ($provincia as $k => $v) {
	?>
		<?php
if ($row['provincia_cod'] == $k):
		echo $v;
	endif;
	?>
		<?PHP
}?></font>
	</div>
	<div class="col-sm-3">
		<label class="control-label  -sm-1 c " for="email">Distrito: 	</label>
		<font>
			<?php foreach ($distrito as $k => $v) {
	?>
			<?php
if ($row['distrito_cod'] == $k):
		echo $v;
	endif;
	?>
			<?PHP
}?>
		</font>
	</div>
</div>
<!-- ############################### -->
<div class="row  ">
	<div class="col-sm-6">
		<label class="control-label  -sm-3" for="email">Comunidad:</label>
		<font><?php _vi($row, 'comunidad');?></font>
	</div>
	<div class="col-sm-4">
		<label class="control-label  -sm-2" for="email">Fecha:</label>
		<font><?php
$row['fecha'] = date('d/m/Y', strtotime($row['fecha']));
_vi($row, 'fecha');?>
		</font>
	</div>
</div>
<hr>
<div class="clearfix"></div>




	<h4>Perfil del conflicto </h4>


<div class="row">
<div class="col-sm-12">
	<label class="control-label  -sm-3" for="email">Antecedentes:</label>
	<font><?php _vi($row, 'antecedentes');?></font>
</div>
</div>

<div class="row">
<div class="col-sm-12">
	<label class="control-label  -sm-3" for="email">Situacion Actual:</label>
	<font><?php _vi($row, 'situacion_actual');?></font>
</div>
</div>




 <div class="row">
		 	 <div class="col-sm-12">

		 	 	 <label></label>
		 	 	 <h4>Hechos</h4>
		 	 </div>
		 </div>




<div class="row">

<div class="col-sm-12">
	<?php if (count($hechos) > 0): ?>
	<?php foreach ($hechos as $k => $v): ?>
	<?php
$v['hechos_fecha'] = date('d/m/Y', strtotime($v['hechos_fecha']));
?><div class="well well-sm">
	<div class="row">

			<div class="col-sm-8">
				<label>Descripción:</label>
				<font><?php echo $v['hechos_descripcion']; ?></font>
			</div>
			<div class="col-sm-3">
				<label>Fecha: </label>
				<font><?php echo $v['hechos_fecha']; ?></font>
			</div>

		</div>
	</div>
	<?php endforeach?>
	<?php else: ?>
	<?php endif?>
</div>
</div>
<hr>

<div class="row">
<label class="control-label  col-sm-12">Tipo:</label>
<div class="col-sm-12">
	<?php foreach ($tipos as $k => $v): ?>
	<div class="checkbox">
		<label>
			  <?php
if (in_array($v['id'], $mis_tipos)):
	echo "[X]";
endif;
?>
			<?php echo $v['nombre']; ?>
		</label>
	</div>
	<?php endforeach?>
</div>
</div>
<!-- ############################### -->
<div class="row">
<div class="col-sm-12">
	<label class="control-label  -sm-3" for="email">Problema:</label>
	<font><?php _vi($row, 'problema');?></font>
</div>
</div>
<!-- ############################### -->
<div class="row">
<div class="col-sm-12">
	<label class="control-label  -sm-3" for="email">Poblacion afectada:</label>
	<font><?php _vi($row, 'poblacion_afectada');?></font>
</div>
<div class="col-sm-12">
	<label class="control-label  -sm-2" for="email">Demanda:</label>
	<font><?php _vi($row, 'demanda');?></font>
</div>
</div>
<!-- ############################### -->
<div class="row">
<div class="col-sm-12">
	<label class="control-label  -sm-3" for="email">Causas:</label>
	<font> <?php _vi($row, 'causas');?></font>
</div>
</div>
<hr>
<?PHP

function xml_attribute($object, $attribute) {
	if (isset($object[$attribute])) {

		$s = (string) $object[$attribute];

		return addslashes($s);
	}

}

 function _helper_custom_name_file($file)
 {

 $temp=explode('_',$file);
  unset($temp[0]);

    return  implode('_', $temp);
 }
function _helper_acciones() {
	$r['no-acciones'] = 0;
	$r['tomado-conocimiento'] = 1;
	$r['revisado'] = 2;
	$r['denuncia-hecho'] = 3;
	$r['acciones-frecuentes'] = 4;

	return $r;
}
function _helper_status($status) {
	$status = (int) $status;
	switch ($status) {
	case 1:
		echo "Activo";
		break;

	default:
		echo "Inactivo";
		break;
	}

}
function _helper_active($t, $v, $c) {
	if ($t == $v):
		return $c;
	endif;
}
function _helper_icono($icono = 'search', $return = false) {

	return _help_icono($icono, $return);

}
function _help_mensajes($zona = TRUE) {

	$tipo = '';
	$CI = &get_instance();

	if ($zona !== TRUE):

		if (is_null($CI->session->flashdata('mensaje_zona'))) {
			return false;
		}

		if ($CI->session->flashdata('mensaje_zona') != $zona) {
			return false;
		}

	endif;

	$tipo = 'alert-danger';
	if (is_null($CI->session->flashdata('mensaje'))) {
		return false;
	}

	if ($CI->session->flashdata('tipo_mensaje') == 0):
		$tipo = 'alert-success';
	endif;
	?>
        <div class="row tareo-mensaje" style="margin-top:20px;">
            <div class="col-md-12 col-sm-12  ">
                <div id="mensaje-httpget" class="alert  <?php
echo $tipo; ?> alert-dismissible " role="alert">
                    <?php
echo $CI->session->flashdata('mensaje'); ?>
                </div>
            </div></div>
            <?php
}
function _vi($row, $k, $r = false) {
	if (!$r):
		echo set_value($k, $row[$k]);
	else:
		return set_value($k, $row[$k]);
	endif;
}

function _vs($row, $k, $v, $r = false) {

	$default = ($row[$k] == $v) ? true : false;
	if (!$r):

		echo set_select($k, $v, $default);
	else:
		return set_select($k, $v, $default);
	endif;
}

function _helper_parser($vars, $content) {

	foreach ($vars as $k => $v) {

		$content = str_replace('{{' . trim($k) . '}}', $v, $content);
	}

	return $content;
}
function _helper_breadcrum($options = array(), $flag = false, $char = ' &raquo; ') {
	$str = '';
	foreach ($options as $k => $v):

		if (is_array($v)):
			$icono = '';
			if (isset($v['icon'])):
				$icono = _help_icono($v['icon'], true);
			endif;

			if (isset($v['href'])):

				$str .= '<a href="' . $v['href'] . '">' . $v['text'] . ' ' . $char . '</a>';
			else:

				$str .= $icono . ' ' . $v['text'] . $char;
			endif;

		else:

			$str .= $v . $char;

		endif;

	endforeach;

	if (!$flag):
		echo $str;
	else:
		return $str;

	endif;
}
function _help_icono($icono = 'search', $return = false) {

	$m = '<span class="glyphicon glyphicon-' . $icono . '" aria-hidden="true"></span>';

	if ($return):

		return $m;

	else:

		echo $m;

	endif;

}
function _clean_format($s) {

	$s = strtolower($s);

	$s = str_replace(',', '', $s);
	$s = str_replace(' ', '', $s);
	return $s;
}
function _helper_login_login() {
	$CI = &get_instance();

	$temp = $CI->session->userdata('login_data')['varAdminLogin'];

	return $temp;
}

function _helper_login_id() {
	$CI = &get_instance();

	$temp = $CI->session->userdata('login_data')['intAdminId'];

	return $temp;
}

function _help_magic_post($field, $arr) {
	$CI = &get_instance();
	$arr[$field] = $CI->input->post($field);
	return $arr;
}
function _helper_set_mensaje($t) {
	$CI = &get_instance();
	$CI->session->set_flashdata('_mensajes', $t);
}
function _helper_get_mensaje() {
	$CI = &get_instance();
	$r = $CI->session->flashdata('_mensajes');

	if (isset($r) && !empty($r) && is_array($r)):

		$icono = '';
		if (isset($r['error']) && $r['error'] == 0):
			$icono = _help_icono('ok-circle', true);
		endif;
		?>
																			<div class="alert alert-<?PHP echo $r['tipo']; ?> alert-dismissible" role="alert">

																			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																			</button>
																			<?php echo $icono; ?> <?php echo $r['mensaje']; ?>
																			</div>
																			<?PHP
endif;

}
function _post_review($t) {

	switch ($t) {
	case 'Y':
		return 'Requires Revisions ';
		break;

	case 'N':
		return 'No Revisions Required ';
		break;

	case 'C':
		return 'Cleared by Reviewer ';
		break;

	case '-':
		return 'Escalate Review';
		break;

	default:
		return '__';
		break;
	}
}

function _if($a = '', $b = '', $igual = '', $diferente = '', $mayor = '', $menor = '') {

	if ($a == $b):

		return $igual;

	elseif ($a != $b):
		return $diferente;

	elseif ($a > $b):
		return $mayor;

	elseif ($a < $b):

		return $menor;
	endif;

}

function _help_jqxwidget() {

	$t = 'jqxcore.js,jqxdata.js,jqxbuttons.js,jqxscrollbar.js,jqxmenu.js,jqxcheckbox.js,jqxlistbox.js,jqxdropdownlist.js,
	jqxgrid.js,jqxgrid.sort.js,jqxgrid.pager.js,jqxgrid.selection.js,jqxgrid.edit.js,jqxgrid.columnsresize.js,jqxgrid.filter.js,jqxcalendar.js,jqxdatetimeinput.js,jqxloader.js,jqxnotification.js';

	$a = explode(',', $t);

	?>

 <link rel="stylesheet" href="<?php echo base_url('html/jqwidgets/jqwidgets/styles/jqx.base.css'); ?>" type="text/css" />

 <?php foreach ($a as $k => $v): ?>
<script type="text/javascript" src="<?php echo base_url('html/jqwidgets/'); ?>/jqwidgets/<?php echo $v; ?>"></script>
 <?php endforeach?>





 	<?php
}
/*
Navicat PGSQL Data Transfer

Source Server         : pg
Source Server Version : 90410
Source Host           : localhost:5434
Source Database       : siarayacucho
Source Schema         : conflictos

Target Server Type    : PGSQL
Target Server Version : 90410
File Encoding         : 65001

Date: 2017-01-25 02:47:20
*/


-- ----------------------------
-- Table structure for monitoreo_documentos
-- ----------------------------
DROP TABLE IF EXISTS "conflictos"."monitoreo_documentos";
CREATE TABLE "conflictos"."monitoreo_documentos" (
"id" int4 DEFAULT nextval('"conflictos".monitoreo_responsables_id_seq'::regclass) NOT NULL,
"monitoreo_id" int4 NOT NULL,
"create_user" varchar(20) COLLATE "default",
"modify_user" varchar(20) COLLATE "default",
"create_date" timestamp(6),
"modify_date" timestamp(6),
"file" text COLLATE "default",
"descripcion" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Indexes structure for table monitoreo_documentos
-- ----------------------------
CREATE INDEX "monitoreo_responsables_fkindex1_copy" ON "conflictos"."monitoreo_documentos" USING btree (monitoreo_id);

-- ----------------------------
-- Primary Key structure for table monitoreo_documentos
-- ----------------------------
ALTER TABLE "conflictos"."monitoreo_documentos" ADD PRIMARY KEY ("id");

<h4>Actor que reporta el conflicto -  la denuncia</h4>
<div class="form-group">
	<label class="control-label col-sm-3 col-md-3" for="email">Nombres y apellidos:</label>
	<div class="col-sm-9 col-md-9">
		<input type="text" class="form-control input-sm" data-rule-required="true" id="actor_reporta_nombre" name="actor_reporta_nombre" placeholder="" value="<?php _vi($row, 'actor_reporta_nombre');?>">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">DNI:</label>
	<div class="col-sm-4">
		<input type="text" name="actor_reporta_dni" maxlength="8" data-rule-maxlength=8 minlength="8" data-rule-number=true data-rule-minlength=8 value="<?php _vi($row, 'actor_reporta_dni');?>" class="form-control input-sm" data-rule-required=true id="actor_reporta_dni" placeholder="">
	</div>
	<label class="control-label col-sm-2" for="email">Tipo de actor:</label>
	<div class="col-sm-3">
		<select name="actor_reporta_tipo" id="actor_reporta_tipo" data-rule-required="true" class="form-control input-sm">
			<option value="">Seleccione</option>
			<option value="primario" <?php _vs($row, 'actor_reporta_tipo', 'primario');?>>Primario</option>
			<option value="secundario" <?php _vs($row, 'actor_reporta_tipo', 'secundario');?>>Secundario</option>
			<option value="otro" <?php _vs($row, 'actor_reporta_tipo', 'otro');?>>Otro</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Observación:</label>
	<div class="col-sm-9">
		<input type="text" name="actor_reporta_observacion" value="<?php _vi($row, 'actor_reporta_observacion');?>" data-rule-required="true" class="form-control input-sm" id="actor_reporta_observacion" placeholder="">
	</div>
</div>
<h4>Otras fuentes de información</h4>
<div id="otras_fuentes_content">

  <?php if (count($otras) > 0): ?>

 <?php foreach ($otras as $kk => $vv):

?>
	<div id="otras_fuentes" class="otras_fuentes">
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Nombres y apellidos:</label>
			<div class="col-sm-9">
				<input type="text" name="otras_nombre[]" value="<?php _vi($vv, 'nombres');?>" class="form-control input-sm" id="" placeholder="">
			</div>
		</div>
		<div class="form-group">
		 <input type="hidden" name="otras_id[]" id="inputOtras_id[]" class="form-control" value="<?php _vi($vv, 'id');?>">
			<label class="control-label col-sm-3" for="email">Tipo:</label>
			<div class="col-sm-4">
				<select name="otras_tipo[]" id="input" class="form-control input-sm"  >
					<option value="radio" <?php _vs($vv, 'tipo', 'radio');?> >Radio</option>
					<option value="periodico"  <?php _vs($vv, 'tipo', 'periodico');?> >Periodico</option>
					<option value="articulo"  <?php _vs($vv, 'tipo', 'articulo');?> >Articulo</option>
					<option value="web"  <?php _vs($vv, 'tipo', 'web');?> >Web</option>
					<option value="otro"  <?php _vs($vv, 'tipo', 'otro');?> >Otro</option>
				</select>
			</div>
			<div class="col-sm-5">
				<input type="text" name="otras_descripcion[]" id="" class="form-control input-sm" value="<?php _vi($vv, 'descripcion');?>"   placeholder="descripción" title="">
			</div>
		</div>

<?php if (!empty($vv['documento'])): ?>
		 <div class="form-group">
<label class="control-label col-sm-3" for="email">Documento Adjunto:</label>
 <div class="col-sm-9">


<p class="form-control-static">
<a href="<?php echo base_url('uploads/documentos/' . $vv['documento']); ?>" target="_blank">		 <?php echo $vv['documento']; ?></a>
</p>
</div></div>
 <?php else: ?>
 		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Adjuntar documento:</label>
			<input accept='image/*'
			id=""
			name="file[]"
			type="file"
			class="custom-input-file-hidden"
			>
		</div>
 <?php endif;?>








		 <div class="form-group">
		  <div class="col-sm-12">

		  	<a href="javascript:void(0)" class="btn btn-danger boton_delete pull-right" data-id="<?php echo $vv['id']; ?>"> Quitar </a>
		  </div>

		 </div>
		<hr>
	</div>

 <?php endforeach;?>
  <?php else: ?>
<section id="otras_fuentes_content">
	<div id="otras_fuentes" class="otras_fuentes">
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Nombres y apellidos:</label>
			<div class="col-sm-9">
				<input type="text" name="otras_nombre[]" class="form-control input-sm" id="" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Tipo:</label>
			<div class="col-sm-4">
				<select name="otras_tipo[]" id="input" class="form-control input-sm"  >
					<option value="radio">Radio</option>
					<option value="periodico">Periodico</option>
					<option value="articulo">Articulo</option>
					<option value="web">Web</option>
					<option value="otro">Otro</option>
				</select>
			</div>
			<div class="col-sm-5">
				<input type="text" name="otras_descripcion[]" id="" class="form-control input-sm" value=""   placeholder="descripción" title="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Adjuntar documento:</label>
			<input accept='image/*'
			id=""
			name="file[]"
			type="file"
			class="custom-input-file-hidden"
			>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<a href="javascript:void(0);" class="btn btn-danger boton_quitar pull-right"> Quitar </a>
			</div>
		</div>
		<hr>
	</div>
</section>
  <?php endif?>

</div>
<div class="form-group">
	<div class="col-sm-3 col-sm-offset-9">
		<button type="button" class="btn btn-default btn-block " id="boton_clonar">Agregar otra fuente</button>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_1">Continuar</button>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		Editar usuario  : <label> <?php
echo $usuario['username']; ?> </label>
	</div>
	<div class="panel-body">
		<?php
_help_mensajes();?>
		<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
		<form method="post">
			<input type="hidden" class="form-control" name="id" value="<?php
echo $id; ?>">
			<div class="form-group">
				<!---
				<div class="form-group">
						<div class="row">
												<div class="col-sm-4"> Nombre de usuario: </div>
						<div class="col-sm-4"> <input class="form-control" name="username" value="<?php
echo $usuario['username']; ?>"> </div>
					</div>
				</div>
				-->
				<div class="row">
					<div class="col-sm-4"> Nombres: </div>
					<div class="col-sm-4"> <input class="form-control" name="nombres" value="<?php
echo $usuario['nombres']; ?>"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Cargo: </div>
					<div class="col-sm-4"> <input value="<?php
echo $usuario['cargo']; ?>" class="form-control" name="cargo"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Institución: </div>
					<div class="col-sm-4"> <input value="<?php
echo $usuario['institucion']; ?>" class="form-control" name="institucion"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Email: </div>
					<div class="col-sm-4">
						<input readonly class="form-control  " type="email"  name="email" value="<?php
echo $usuario['email']; ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Dni: </div>
					<div class="col-sm-4">
						<input readonly class="form-control numero numeros" maxlength="8" name="dni" value="<?php
echo $usuario['dni']; ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Clave: </div>
					<div class="col-sm-4"> <input class="form-control" type="password" name="password"> </div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">   <button type="submit"  class="btn btn-primary"> Editar  </button> </div>
			</div>
		</form>
	</div>
</div>
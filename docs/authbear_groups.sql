/*
Navicat PGSQL Data Transfer

Source Server         : pg
Source Server Version : 90217
Source Host           : localhost:5434
Source Database       : siarayacucho
Source Schema         : conflictos

Target Server Type    : PGSQL
Target Server Version : 90217
File Encoding         : 65001

Date: 2016-09-21 10:31:59
*/


-- ----------------------------
-- Table structure for "conflictos"."authbear_groups"
-- ----------------------------
 
CREATE TABLE "conflictos"."authbear_groups" (
"id" int4 DEFAULT nextval('"conflictos".groups_id_seq'::regclass) NOT NULL,
"name" varchar(20) NOT NULL,
"description" varchar(100) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of authbear_groups
-- ----------------------------
INSERT INTO "conflictos"."authbear_groups" VALUES ('1', 'ADMIN', 'Administrator');
INSERT INTO "conflictos"."authbear_groups" VALUES ('2', 'EVALUADOR', 'Registro de fichas de evaluacion');
INSERT INTO "conflictos"."authbear_groups" VALUES ('3', 'DIGITADOR', 'Registro de informacion basica del productor, fichas de  plan de produccion.');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Checks structure for table "conflictos"."authbear_groups"
-- ----------------------------
ALTER TABLE "conflictos"."authbear_groups" ADD CHECK (id >= 0);

-- ----------------------------
-- Primary Key structure for table "conflictos"."authbear_groups"
-- ----------------------------
ALTER TABLE "conflictos"."authbear_groups" ADD PRIMARY KEY ("id");

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Conflicto > Listado de evaluaciones</h3>
	</div>
	<div class="panel-body">

 <div class="well well-sm well-success">
 Conflicto: <?php echo $conflicto['nombre']; ?>
 </div>


       <div id="toolbar" class="hidden">
          <select class="form-control">

             <option value="">Exportar</option>
             <option value="all">Exportar todo</option>
             <option value="selected">Exportar selección</option>
          </select>
       </div>


 <table id="table" data-toggle="table" data-url="<?php echo site_url('evaluacion/listado_evaluaciones/' . $conflictos_id); ?>"

			data-side-pagination="server"
               data-pagination="true"
               data-page-list="[5, 10, 20, 50, 100, 200]"
               data-search="true"
data-filter-control="true"
           data-filter-show-clear="true"

           data-show-export="false"
data-export-options='{
         "fileName": "testo",
         "exportHiddenColumns": ["propuesta"],
         "worksheetName": "test1",
         "jspdf": {
           "autotable": {
             "styles": { "rowHeight": 20, "fontSize": 10 },
             "headerStyles": { "fillColor": 255, "textColor": 0 },
             "alternateRowStyles": { "fillColor": [60, 69, 79], "textColor": 255 }
           }
         }
       }'
           data-toolbar="#toolbar"
data-show-columns="true"

 >
    <thead>
        <tr>
            <th data-field="id" data-sortable="true">ID</th>
            <th data-field="aprendizaje" data-sortable="true" data-filter-control="input">Aprendizaje</th>
             <th data-field="responsable" data-sortable="true"  data-filter-control="input">Acciones</th>
            <th data-field="fecha" data-tableexport-value="export cddddddddontent" data-sortable="true" data-filter-control="input">Fecha</th>

 <th data-field="propuesta" data-tableexport-display="always" data-visible="false"    >Propuesta</th>

            <th data-field="id"  data-formatter="accionesFormatter">Acciones</th>
        </tr>
    </thead>
</table>



	</div>
</div>
 <script type="text/javascript">

 	function accionesFormatter(value, row) {
       // var icon = row.id % 2 === 0 ? 'glyphicon-star' : 'glyphicon-star-empty'
       var icon='glyphicon-pencil';
       var icon2='glyphicon glyphicon-list';
link= _helper_site_url('evaluacion/evaluacion_editar/'+$.md5(row.id));
link2= _helper_site_url('evaluacion/evaluacion_ver/'+$.md5(row.id));
       return '<a href="'+link+'" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon + '"></i>  Editar evaluacion </a> <a href="'+link2+'"                            target="" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon2 + '"></i>  Ver evaluacion </a>';
    }


    var $table = $('#table');
    $(function () {
    });

 </script>

 <script>
    var $table = $('#table');
    $(function () {
        $('#toolbar').find('select').change(function () {
            $table.bootstrapTable('destroy').bootstrapTable({
                exportDataType: $(this).val(),
                exportHiddenColumns: ["propuesta"]
            });

        });
    })
</script>
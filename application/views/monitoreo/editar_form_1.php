<div class="well well-sm">
	<h4>Monitoreo</h4>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Fase del conflicto:</label>
		<div class="col-sm-3">
			<select name="fase" id="fase" class="form-control">
				<option  value="latente">Latente
				</option>
				<option  value="manifiesto">Manifiesto</option>
				<OPTION value="dialogo">Diálogo</OPTION>
				<OPTION value="transformacion">Transformación</OPTION>
			</select>
		</div>
		<label class=" col-sm-3 control-label">Fecha:</label>
		<div class="col-sm-3">
			<input type="text" name="fecha" id="fecha" maxlength="10" minlength="10" data-rule-maxlength=10 data-rule-minlength=10 data-rule-validDate="true"   class="fecha form-control input-sm"  data-rule-required="true"  value="<?php
$row['fecha'] = date('d/m/Y', strtotime($row['fecha']));
_vi($row, 'fecha');?>" title="">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-3 control-label">Desencadenante:</label>
		<div class="col-sm-9">
			<input type="text" name="desencadenante" id="inputDesencadenante" class="form-control" value="<?php echo $row['desencadenante']; ?>" required="required" pattern="" title="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Mecanismo de protesta:</label>
		<div class="col-sm-3">
			<select name="mecanismo_1" id="mecanismo_1" class="form-control" required="required">
				<option value="institucional">Institucional</option>
				<option value="no-institucional">No Institucional</option>
			</select>
		</div>
		<div class="col-sm-3">
			<select name="mecanismo_2" id="mecanismo_2" class="form-control" required="required">
				<option value="oficios">Oficios</option>
				<option value="cartas">Cartas</option>
				<option value="memoriales">Memoriales</option>
			</select>
		</div>
	</div>
</div>

<section id="otros_responsables_content">

		<?php
foreach ($responsables as $k => $v): ?>

		<div class="row otros_responsables">
		 <input type="hidden" value="<?php echo $v['id']; ?>" name="responsable_id[]" id="responsable_id">
			<div class="col-sm-1">
				<bottom href="#" data-id="<?php echo $v['id']; ?>" class="btn btn-xs btn-danger boton_delete_responsables">
				<?php _helper_icono('trash')?></bottom>
			</div>
			<div class="col-sm-11">
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Responsable del caso</label>
					<div class="col-sm-9">
						<input type="text" name="responsable[]" id="inputResponsable" class="form-control" value="<?php echo $v['responsable']; ?>" required="required" pattern="" title="">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Observaciones</label>
					<div class="col-sm-9">
						<input type="text" name="observaciones[]" id="observaciones" class="form-control" value="<?php echo $v['observaciones']; ?>" required="required" pattern="" title="">
					</div>
				</div>

				<hr>
			</div>
		</div>
		<?php endforeach?>

</section>
<div class="form-group">
	<div class="col-sm-3 col-sm-offset-9">
		<button type="button" class="btn btn-default btn-block " id="boton_clonar_responsables">Agregar </button>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_1">Continuar</button>
	</div>
</div>
<script type="text/javascript">
jQuery(function(){
jQuery('#fase').val('<?php echo $row['fase']; ?>');
jQuery('#mecanismo_1').val('<?php echo $row['mecanismo_1']; ?>');
jQuery('#mecanismo_2').val('<?php echo $row['mecanismo_2']; ?>');
});
</script>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Registro del conflicto  > Datos generales</h3>
		<h4>2 de 3</h4>
	</div>
	<div class="panel-body"><form action="" method="POST" class="form-horizontal" role="form">
		<h4>Analisis del conflicto/denuncia</h4>

			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Codigo:</label>
				<div class="col-sm-4">
					<input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title="">
				</div>
				<label class="control-label col-sm-2" for="email">Estado:</label>
				<div class="col-sm-3">
					<select name="" id="input" class="form-control" required="required">
						<option value=""></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Nombre </label>
				<div class="col-sm-9">
					<input type="text" class="form-control input-sm" id="CODIGO" name="actor" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Departamento</label>
				<div class="col-sm-3">
					<select name="" id="input" class="form-control input-sm" required="required">
						<option value=""></option>
					</select>
				</div>
				<label class="control-label col-sm-1" for="email">Provincia</label>
				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					<select name="" id="input" class="form-control input-sm" required="required">
						<option value=""></option>
					</select>
				</div>
				<label class="control-label col-sm-1" for="email">Distrito</label>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<select name="" id="input" class="form-control input-sm" required="required">
						<option value=""></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Comunidad:</label>
				<div class="col-sm-4">
					<input type="text" name="" class="form-control input-sm" id="" placeholder="">
				</div>
				<label class="control-label col-sm-2" for="email">Fecha:</label>
				<div class="col-sm-3">
					<input type="text" name="" id="input" class="form-control input-sm" value="" required="required" pattern="" title="">
				</div>
			</div>
			<h4>Perfil del conflicto </h4>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Antecedentes:</label>
				<div class="col-sm-9">
					<input type="text" name="observacion" class="form-control input-sm" id="" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Situacion Actual:</label>
				<div class="col-sm-9">
					<input type="text" name="otras_nombre[]" class="form-control input-sm" id="" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Hechos:</label>
				<div class="col-sm-4">
					<textarea name="hechos" id="inputHechos" class="form-control input-sm" rows="3" required="required"></textarea>
				</div>
				<label class="control-label col-sm-2" for="email">Tipo:</label>
				<div class="col-sm-3">
					<select name="" id="input" class="form-control input-sm" required="required">
						<option value=""></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Problema:</label>
				<div class="col-sm-9">
					<input type="text" name="" id="input" class="form-control input-sm" value="" required="required" pattern="" title="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Poblacion afectada:</label>
				<div class="col-sm-4">
					<textarea name="hechos" id="inputHechos" class="form-control input-sm input-sm" rows="3" required="required"></textarea>
				</div>
				<label class="control-label col-sm-2" for="email">Demanda:</label>
				<div class="col-sm-3">
					<input type="text" name="" id="input" class="form-control input-sm input-sm" value="" required="required" pattern="" title="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Causas:</label>
				<div class="col-sm-9">
					<input type="text" name="" id="input" class="form-control input-sm input-sm" value="" required="required" pattern="" title="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-block btn-primary">Guardar y Continuar</button>
				</div>
			</div>
		</form>
	</div>
</div>
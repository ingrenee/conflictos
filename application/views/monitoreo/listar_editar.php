<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Monitoreo > Listado de conflictos registrados</h3>
	</div>
	<div class="panel-body">


 <table id="table" data-toggle="table" data-url="<?php echo site_url('monitoreo/listar_para_editar'); ?>"

			data-side-pagination="server"
               data-pagination="true"
               data-page-list="[5, 10, 20, 50, 100, 200]"
               data-search="true"
data-filter-control="true"
           data-filter-show-clear="true"
 >
    <thead>
        <tr>
            <th data-field="id" data-sortable="true">ID</th>
            <th data-field="nombre" data-sortable="true" data-filter-control="input">Nombre del conflicto</th>
            <th data-field="estado" data-sortable="true"  data-filter-control="select">Estado</th>
            <th data-field="actor_reporta_nombre"  data-sortable="true"  data-filter-control="input" >Actor que reporta</th>
            <th data-field="actor_reporta_dni" data-sortable="true" data-filter-control="input">Actor - DNI</th>
            <th data-field="id"  data-formatter="accionesFormatter">Acciones</th>
        </tr>
    </thead>
</table>



	</div>
</div>
 <script type="text/javascript">

 	function accionesFormatter(value, row) {
       // var icon = row.id % 2 === 0 ? 'glyphicon-star' : 'glyphicon-star-empty'
       var icon='glyphicon-plus';
       var icon2='glyphicon glyphicon-list';
link= _helper_site_url('monitoreo/agregar/'+$.md5(row.id));
link2= _helper_site_url('monitoreo/listar/'+$.md5(row.id));
       return '<a href="'+link+'" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon + '"></i>  Agregar Monitoreo </a> <a href="'+link2+'"                            target="" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon2 + '"></i>  Listar Monitoreo </a>';
    }


    var $table = $('#table');
    $(function () {
    });

 </script>
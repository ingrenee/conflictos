/*
Navicat PGSQL Data Transfer

Source Server         : pg
Source Server Version : 90217
Source Host           : localhost:5434
Source Database       : siarayacucho
Source Schema         : conflictos

Target Server Type    : PGSQL
Target Server Version : 90217
File Encoding         : 65001

Date: 2016-09-21 10:32:09
*/


-- ----------------------------
-- Table structure for "conflictos"."authbear_users"
-- ----------------------------
 
CREATE TABLE "conflictos"."authbear_users" (
"id" int4 DEFAULT nextval('"conflictos".users_id_seq'::regclass) NOT NULL,
"ip_address" varchar(15),
"username" text NOT NULL,
"password" text NOT NULL,
"salt" varchar(255),
"email" varchar(100),
"activation_code" varchar(40),
"forgotten_password_code" varchar(40),
"forgotten_password_time" int4,
"remember_code" varchar(40),
"created_on" int4,
"last_login" int4,
"active" int4,
"first_name" varchar(50),
"last_name" varchar(50),
"company" varchar(100),
"phone" varchar(20),
"nombres" text,
"dni" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of authbear_users
-- ----------------------------
INSERT INTO "conflictos"."authbear_users" VALUES ('1', '127.0.0.1', '42864625', 'c4ca4238a0b923820dcc509a6f75849b', '', 'admin@admin.com', '', null, null, null, '1268889823', '1439782914', '1', 'Admin', 'istrator', 'ADMIN', '0', 'Administrador', '42864625');
INSERT INTO "conflictos"."authbear_users" VALUES ('1', '127.0.0.1', 'admin', 'c4ca4238a0b923820dcc509a6f75849b', '', 'admin@admin.com', '', null, null, null, '1268889823', '1439782914', '1', 'Admin', 'istrator', 'ADMIN', '0', 'Renee Michael Morales Calhua', '42864625');
INSERT INTO "conflictos"."authbear_users" VALUES ('4', null, '42864625', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null);
INSERT INTO "conflictos"."authbear_users" VALUES ('4', null, '42864625', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null);
INSERT INTO "conflictos"."authbear_users" VALUES ('5', null, '4', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null);
INSERT INTO "conflictos"."authbear_users" VALUES ('5', null, '4', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null);
INSERT INTO "conflictos"."authbear_users" VALUES ('6', null, 'usuario_digitador', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Matias Vera', '12345678');
INSERT INTO "conflictos"."authbear_users" VALUES ('6', null, 'usuario_digitador', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Matias Vera', '12345678');
INSERT INTO "conflictos"."authbear_users" VALUES ('7', null, 'usuario_evaluador', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Manuel Vargas', '12323456');
INSERT INTO "conflictos"."authbear_users" VALUES ('7', null, 'usuario_evaluador', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Manuel Vargas', '12323456');
INSERT INTO "conflictos"."authbear_users" VALUES ('8', null, 'admin2', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Manuel Vargas', '12323323');
INSERT INTO "conflictos"."authbear_users" VALUES ('8', null, 'admin2', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Manuel Vargas', '12323323');
INSERT INTO "conflictos"."authbear_users" VALUES ('9', null, 'PRUEBA', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'PRUEBA', '12121212');
INSERT INTO "conflictos"."authbear_users" VALUES ('9', null, 'PRUEBA', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'PRUEBA', '12121212');
INSERT INTO "conflictos"."authbear_users" VALUES ('10', null, '43130485', '827ccb0eea8a706c4c34a16891f84e7b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'admin', '43130485');
INSERT INTO "conflictos"."authbear_users" VALUES ('11', null, '00000001', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Test', '00000001');
INSERT INTO "conflictos"."authbear_users" VALUES ('12', null, '00000002', 'c2890d44d06bafb6c7b4aa194857ccbc', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'test2', '00000002');
INSERT INTO "conflictos"."authbear_users" VALUES ('13', null, '00000005', '827ccb0eea8a706c4c34a16891f84e7b', null, null, null, null, null, null, null, null, '1', null, null, null, null, 'Hilda Caceres', '00000005');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Checks structure for table "conflictos"."authbear_users"
-- ----------------------------
ALTER TABLE "conflictos"."authbear_users" ADD CHECK (id >= 0);
ALTER TABLE "conflictos"."authbear_users" ADD CHECK (active >= 0);

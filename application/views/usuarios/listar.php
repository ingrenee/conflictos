<?php _help_mensajes();?>
<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
<table class="table table-condensed table-striped">
	<thead>
		<tr> <th> #id </th>
		<!-- <td> Nombre de usuario  </td> -->
		<td> Nombres </td>
		<td> DNI  </td>
		<td> Estado </td>
		<td>   </td>
	</tr>
</thead>
<tbody>
	<?php
foreach ($rows as $k => $v):
?>
	<tr> <td> <?php echo $v['id']; ?> </td>
	<!--	<td> <?php echo $v['username']; ?>  </td>
	-->
	<td> <?php echo $v['nombres']; ?>  </td>
	<td> <?php echo $v['dni']; ?>  </td>
	<td> <?php if ((int) $v['active'] == 1): ?>
		<a href="<?php echo site_url('usuarios/estado/2/' . $v['id']); ?>" class="btn btn-primary btn-xs" title="Desactivar"> Activo </a>
		<?php else: ?>
		<a href="<?php echo site_url('usuarios/estado/1/' . $v['id']); ?>" class="btn btn-warning  btn-xs" title="Desbloquear"> Bloqueado </a>
		<?php endif;?>
	</td>
	<td> <a href="<?php echo site_url('usuarios/editar/' . $v['id']); ?>" class="btn btn-primary btn-xs"> <?php echo _helper_icono('pencil'); ?> Editar </a> </td>
	<td> <a href="<?php echo site_url('usuarios/grupos_rel/' . $v['id']); ?>" class="btn btn-primary btn-xs">Asignar grupos </a> </td>

		<td> <a href="<?php echo site_url('usuarios/rol_rel/' . $v['id']); ?>" class="btn btn-primary btn-xs">Asignar rol </a> </td>
</tr>
<?php
endforeach;
?>
</tbody>
</table>

<?php

$acciones = _helper_acciones();
?>



<table class="hidden highchart"
  data-graph-container=".grafico"
  data-graph-type="line"
  data-graph-yaxis-X-stacklabels-enabled="0"
  data-graph-yaxis-1-tick-interval="1"
  data-graph-yaxis-1-max=4
data-graph-yaxis-1-min=0
data-graph-xaxis-rotation="-90"
  >

  <caption>Acciones tomadas en el conflicto</caption>
  <thead>
    <tr>
      <th data-graph-yaxis-1-tick-interval="1">Fecha</th>
      <th>Acciones</th>

    </tr>
  </thead>
  <tbody>

 <?php foreach ($rows as $k => $v): ?>

 	    <tr>

      <td><?php echo @explode(' ', $v['fecha'])[0]; ?></td>
      <td><?php echo $acciones[$v['acciones']]; ?></td>

    </tr>

 <?php endforeach?>



  </tbody>
</table>

 <div class="row">
   <div class="col-sm-12">

 <div class="grafico" style=" max-width: 700px; width: 100%;">

 </div>
   </div>
 </div>


  <script type="text/javascript">
 	$(document).ready(function() {
 $('table.highchart').highchartTable({


yaxis: [{
            min: 0,
            max: 6,
            tickInterval: 1,

             lineColor: '#FF0000',
            lineWidth: 1,
        }
        ]

 });
});

 </script>
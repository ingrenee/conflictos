<?PHP class Procedimientos_model extends CI_Model {
    public $title;
    public $content;
    public $date;
    public $db2;
    public $base='Premium..';
    public $query;
    public $types=array();
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }
    /*[sp_web_tareo_observacion_info]*/
    public function sp_web_tareo_data_listado_historico($param=array())
    {
        $query=$this->call('sp_web_tareo_data_listado_historico',$param);
        return $this->db->query($query);
    }
//sp_web_tareo_transferencias_anular_todas
    public function sp_query()
    {
     echo $this->query;
 }
 public function query()
 {

  $this->sp_query();
}
public function sp($sp,$param)
{
    $this->query=$this->call($sp,$param);
    return $this->db->query($this->query);
}

 public function json_productor_parcelas($dni, $periodos_id,$parcelas_id)
 {
    $query="select Y.parcelas_id, Y.cultivos_id,  z.cultivo_nombre, Y.fecha_cosecha, Y.fecha_siembra,
     Y.rendimientos_kilos from conflictos.plan_produccion X,
 conflictos.plan_produccion_agricola Y,
conflictos.cultivos Z 
where  Z.cultivos_id=Y.cultivos_id and  Y.plan_produccion_id=X.id and X.productores_dni='".$dni."' and x.periodos_id='".$periodos_id."' 
and parcelas_id=".$parcelas_id;

$query2=" select X.nombres, X.paterno, X.materno, X.direccion as direccion, X.email, X.celular, X.telefono from conflictos.productores X WHERE X.dni='".$dni."'";

$info_parcelas=$this->db->query($query)->result_array();
$info=$this->db->query($query2)->row_array();
$info['rows2']=$info_parcelas;

 return $info;

 }


public function sp_select($sp,$param)
{
    $this->query=$this->call($sp,$param);

    $this->query='select *  from '.$this->query;

    return  $this->db->query($this->query);
}
public function sp_table($sp,$param)
{
    $this->query=$this->call($sp,$param);

    $this->query='select *  from '.$this->query;

    return  $this->db->query($this->query);
}

public function sp_ins_upd($sp,$param)
{
    $this->query=$this->call($sp,$param);

    $this->query='select   '.$this->query;

    return  $this->db->query($this->query);
}
function  query_select($query,$c='')
{

   $query=$this->types=$this->config->item('select')[$query].$c;
   return $this->db->query($query);
}

function  call($sp, $param)
{
    $flag=true;
    $parametros='';

    // obtenermos la definicion de los parametros del SP 
    if(count($param)>0  ):
        $this->types=$this->config->item('sp')[$sp];

    endif;

    
    /* Todos los SP  con parametros deben de  tener una definicionn PG verifica el tipo de variable*/
    if(count($param)>0  &&  ($this->types==NULL)):
        echo "TODOS LOS SP(".$sp.") COM PARAMETROS DEBEN DE SER DEFINIDOS EN config/conflictos-sp";
    exit();
    endif;

$i=0;
    foreach( $param as $k => $v):
        if($flag):

            $parametros=$this->verify_type($v,$i);
        $flag=false;
        else:
            $parametros=$parametros.','.$this->verify_type($v,$k);
        endif;
        $i++;
        endforeach;
        $s='  conflictos.'.$sp.'('.$parametros.')';


        return $s;
    }
    function  verify_type($v,$k)
    {    //echo "::>". $k .'::'.var_dump($v).'<br>';
    if($this->types[$k]==1):
        return  is_numeric($v)?$v:0;
    elseif($this->types[$k]==3):
        return  ($this->validateDate($v))?"'".$v."'":'NULL';

    else:
        return "'".$v."'";
    endif;

        /*
        if(is_numeric($v)):
            return  $v;
        else:
            return "'".$v."'";
        endif;
        */
    }

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }
}

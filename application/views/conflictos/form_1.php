<h4>Actor que reporta el conflicto -  la denuncia</h4>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Nombres y apellidos:</label>
	<div class="col-sm-9">
		<input type="text" class="form-control input-sm" data-rule-required="true" id="actor_reporta_nombre" name="actor_reporta_nombre" placeholder="">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">DNI:</label>
	<div class="col-sm-4">
		<input type="text" name="actor_reporta_dni" maxlength="8" data-rule-maxlength=8 minlength="8" data-rule-number=true data-rule-minlength=8 class="form-control input-sm" data-rule-required=true id="actor_reporta_dni" placeholder="">
	</div>
	<label class="control-label col-sm-2" for="email">Tipo de actor:</label>
	<div class="col-sm-3">
		<select name="actor_reporta_tipo" id="actor_reporta_tipo" data-rule-required="true" class="form-control input-sm.">
			<option value="">Seleccione</option>
			<option value="primario">Primario</option>
			<option value="secundario">Secundario</option>
			<option value="otro">Otro</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Observación:</label>
	<div class="col-sm-9">
		<input type="text" name="actor_reporta_observacion" data-rule-required="true" class="form-control input-sm" id="actor_reporta_observacion" placeholder="">
	</div>
</div>
<h4>Otras fuentes de información</h4>
<section id="otras_fuentes_content">
	<div id="otras_fuentes" class="otras_fuentes">
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Nombres y apellidos:</label>
			<div class="col-sm-9">
				<input type="text" name="otras_nombre[]" class="form-control input-sm" id="" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Tipo:</label>
			<div class="col-sm-4">
				<select name="otras_tipo[]" id="input" class="form-control input-sm"  >
					<option value="radio">Radio</option>
					<option value="periodico">Periodico</option>
					<option value="articulo">Articulo</option>
					<option value="web">Web</option>
					<option value="otro">Otro</option>
				</select>
			</div>
			<div class="col-sm-5">
				<input type="text" name="otras_descripcion[]" id="" class="form-control input-sm" value=""   placeholder="descripción" title="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Adjuntar documento:</label>
			<input accept='image/*'
			id=""
			name="file[]"
			type="file"
			class="custom-input-file-hidden"
			>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<a href="javascript:void(0);" class="btn btn-danger boton_quitar pull-right"> Quitar </a>
			</div>
		</div>
		<hr>
	</div>
</section>
<div class="form-group">
	<div class="col-sm-3 col-sm-offset-9">
		<button type="button" class="btn btn-default btn-block " id="boton_clonar">Agregar otra fuente</button>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_1">Continuar</button>
	</div>
</div>
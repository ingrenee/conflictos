<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Registro del conflicto  > Agregar documentos de  Monitoreo</h3>

		<!-- <h4 class="subtitle">1 de 3</h4> -->
	</div>
	<div class="panel-body"><?php echo validation_errors(); ?>
	<form action="" 
	id="form_agregar_conflicto" 
	enctype="multipart/form-data" 
	method="POST" 
	class="form-horizontal" 
	role="form">
	 <section id="form_1">


<div class="well well-sm">
	<h4>Monitoreo</h4>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Fase del conflicto:</label>
		<div class="col-sm-3">
			<select name="fase" id="fase" class="form-control">
				<option  value="latente">Latente
				</option>
				<option  value="manifiesto">Manifiesto</option>
				<OPTION value="dialogo">Diálogo</OPTION>
				<OPTION value="transformacion">Transformación</OPTION>
			</select>
		</div>
		<label class=" col-sm-3 control-label">Fecha:</label>
		<div class="col-sm-3">
			<input type="text" name="fecha" id="fecha" maxlength="10" minlength="10" data-rule-maxlength=10 data-rule-minlength=10 data-rule-validDate="true"   class="fecha form-control input-sm"  data-rule-required="true"  value="<?php
$row['fecha'] = date('d/m/Y', strtotime($row['fecha']));
_vi($row, 'fecha');?>" title="">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-3 control-label">Desencadenante:</label>
		<div class="col-sm-9">
			<input type="text" name="desencadenante" id="inputDesencadenante" class="form-control" value="<?php echo $row['desencadenante']; ?>" required="required" pattern="" title="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Mecanismo de protesta:</label>
		<div class="col-sm-3">
			<select name="mecanismo_1" id="mecanismo_1" class="form-control" required="required">
				<option value="institucional">Institucional</option>
				<option value="no-institucional">No Institucional</option>
			</select>
		</div>
		<div class="col-sm-3">
			<select name="mecanismo_2" id="mecanismo_2" class="form-control" required="required">
				<option value="oficios">Oficios</option>
				<option value="cartas">Cartas</option>
				<option value="memoriales">Memoriales</option>
			</select>
		</div>
	</div>
	 <div class="cleafix"></div>
</div>

	 </section>

 


<div class="form-group">
  <div class="col-sm-12">
  	 <h3> Agregar documento(s)</h3>
  </div>
 <div class="col-sm-4">
 <input type="file" name="file[]" data-required="true"  multiple class="form-control">	
 </div>
 
 <div class="col-sm-4">
 	<button type="submit" class="btn btn-primary">Cargar</button>
 </div>
  
  </div>
		</form>

 <?php  if(count($documentos)>0): ?>
 <h3>Documentos agregados:</h3>
 <ul>

 	 <?php foreach ($documentos as $k => $v): ?>
 	 	<li><a href="<?php  echo site_url('monitoreo/borrar_documento/'.md5($v['id'])); ?>" onclick="return  confirm('Esta seguro que desea borrar el documento?')"  class="btn btn-primary btn-xs">Quitar</a> 
<a href="<?php echo base_url('uploads/monitoreo/'.$v['file']); ?>" target="_blank"><?php  echo _helper_custom_name_file($v['file']); ?> </a>
 	 	


 	 	 </li>
 	 <?php endforeach ?>
 </ul>
 <?php  else: ?>
 
 <p>No hay documentos en este monitoreo.</p>
  <?php  endif; ?>
	</div>
</div>
<h4>Actores involucrados</h4>
<div class="form-group">
	<label class=" col-sm-12" for="email">Actores Primarios: </label>
	<div class="col-sm-12">
	<hr>
		<ul>
			<li>
				<div class="col-sm-1"></div>
				<div class="col-sm-3">Posiciones</textarea>
				</div>
				<div class="col-sm-3">Interes</div>
				<div class="col-sm-3">Valores</div>
				<div class="col-sm-2">Necesidades</div>
			</li>
		</ul>
		<ol id="primario_content">
			<li  id="primario_clon" class="primario_clon">
				<div class="row">
					<!--
					posiciones TEXT    ,
					intereses TEXT    ,
					valores TEXT    ,
					necesidades TEXT
					-->
					<div class="col-sm-1"> <a class="btn btn-danger boton_quitar_primario" >Quitar</a></div>
					<div class="col-sm-3">
						<textarea class="form-control" name="posiciones[]"></textarea>
					</div>
					<div class="col-sm-3"><textarea class="form-control" name="intereses[]"></textarea></div>
					<div class="col-sm-3"><textarea class="form-control" name="valores[]"></textarea></div>
					<div class="col-sm-2"><textarea class="form-control" name="necesidades[]"></textarea></div>
				</div>
			</li>
		</ol>
		 <hr>
	</div>

	<div class="form-group">
	<div class="col-sm-3 col-sm-offset-9">
		<button type="button" class="btn btn-default btn-block " id="boton_clonar_primario">Agregar Actor Primario</button>
	</div>
</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Actores Secundarios: </label>
	<div class="col-sm-9">
		<textarea name="actores_2" id="actores_2" class="form-control" rows="3" data-rule-required="true"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Actores Terciarios: </label>
	<div class="col-sm-9">
		<textarea name="actores_3" id="actores_3" class="form-control" rows="3"  data-rule-required="true"></textarea>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-default" id="boton_form_3_back">Volver</button>
	</div>
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_3">Guardar</button>
	</div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Authbear extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *      http://example.com/index.php/welcome
	 *  - or -
	 *      http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
		$this->load->model('Usuarios_model', 'modelo_usuarios');

	}
	public function sin_acceso() {
		$data = array();

		$data['content'] = $this->load->view('authbear/sin_acceso', $data, true);

		$this->load->view('template-mono', $data);
	}

	public function logout() {
		$_SESSION['isLogin'] = false;
		$_SESSION = array();

		unset($_SESSION);

		// Finalmente, destruir la sesión.
		session_destroy();

		redirect(site_url('authbear/login'));
	}
	public function login() {
		$data = array();
		$_SESSION['error_login'] = '';

		$rows = array();
		// Creamos un listado de  concejos regionales

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Email', 'required', array('email' => 'Ingresa un email valido.'));
		$this->form_validation->set_rules('password', 'password', 'required', array('required' => 'Ingresa un password. %s.'));

		if ($this->form_validation->run() == FALSE) {
		} else {

			/* validar usuario */

			$param = array();
			$w['email'] = $this->input->post('username');
			$w['password'] = md5($this->input->post('password'));

			$query = $this->db->where($w)->get('conflictos.authbear_users');

			$usuario = $query->row_array();

			if (count($usuario) > 0):

				/* buscamos todos los roles que tiene asignado el usuario*/
				$param = array();
				$param[] = $usuario['id'];
				/*FOR r IN SELECT * FROM sgpteam.authbear_users_groups
        WHERE authbear_users_id = _a   order by id desc*/
				$w = array();
				$w['authbear_users_id'] = $usuario['id'];
				$query2 = $this->db->where($w)->get('conflictos.authbear_users_groups');

				$roles_usuario = $query2->result_array();

				// listado de grupos
				//

				$grupos = $this->db->get('conflictos.authbear_groups')->result_array();

				$grupos_temp = array();
				foreach ($grupos as $key => $value) {
					$grupos_temp[$value['id']] = $value['name'];
				}

				////
				///
				$roles = array();
				if (count($roles_usuario) > 0):

					// $_SESSION['usuario_roles']=

					foreach ($roles_usuario as $k => $v):
						$roles[$v['authbear_groups_id']] = strtoupper($grupos_temp[$v['authbear_groups_id']]);
					endforeach;
				endif;

				$_SESSION['usuario_roles'] = $roles;
				$_SESSION['isLogin'] = true;
				$_SESSION['usuario_data'] = $usuario;

				//print_r($_SESSION['paths']);
				redirect(site_url('home'));
			else:
				$this->session->set_flashdata('error', 'El email/clave no coinciden.');
				$_SESSION['error_login'] = 'El email/clave no coinciden.';
			endif;
		}

		$data['content'] = $this->load->view('authbear/login', $data, true);

		$this->load->view('template-mono', $data);
	}
}

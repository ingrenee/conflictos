<div class="hidden">

		<div id="otros_responsables" class="otros_responsables row">
			<div class="col-sm-1">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger boton_quitar_responsables "> 	<?php _helper_icono('trash')?> </a>
			</div>
			<div class="col-sm-11">
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Responsable del caso</label>
					<div class="col-sm-9">
						<input type="text" name="responsable[]" id="inputResponsable" class="form-control" value="" required="required" pattern="" title="">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Observaciones</label>
					<div class="col-sm-9">
						<input type="text" name="observaciones[]" id="observaciones" class="form-control" value="" required="required" pattern="" title="">
					</div>
				</div>

				<hr>
			</div>
		</div>

</div>
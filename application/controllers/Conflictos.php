<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include 'html/html2pdf-4.5.1/html2pdf.class.php';

require_once 'html/html2pdf-4.5.1/vendor/autoload.php';
require_once 'html/dompdf/autoload.inc.php';
include 'html/MPDF57/mpdf.php';

class Conflictos extends CI_Controller {
	var $schema = 'conflictos.';
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Estados_model', 'estados_modelo');
		$this->load->model('Tipos_model', 'tipos_modelo');
		$this->load->model('Monitoreo_model', 'monitoreo_modelo');
		$this->load->model('Evaluaciones_model', 'evaluaciones_modelo');
		$this->load->model('Monitoreo_responsables_model', 'monitoreo_responsables_modelo');
		//Monitoreo_responsables_model
	}
	public function index() {
		$data = array();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('actor_reporta_nombre', 'Actor', 'required');

		if ($this->form_validation->run() == FALSE) {
			//$tipos = $this->db->select('*')->get($this->schema . 'tipos')->result_array();
			$tipos = $this->tipos_modelo->get_active();
			$data['tipos'] = $tipos;

			//$estados = $this->db->select('*')->get($this->schema . 'estados')->result_array();

			$estados = $this->estados_modelo->get_active();
			$data['estados'] = $estados;

			$departamentos = $this->get_departamentos();
			$data['departamentos'] = $departamentos;

			$data['content'] = $this->load->view('conflictos/index', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {

			/* section 1 */
			$data['actor_reporta_nombre'] = $this->input->post('actor_reporta_nombre');
			$data['actor_reporta_dni'] = $this->input->post('actor_reporta_dni');
			$data['actor_reporta_tipo'] = $this->input->post('actor_reporta_tipo');
			$data['actor_reporta_observacion'] = $this->input->post('actor_reporta_observacion');

			/* section 2 */
			$data['codigo'] = $this->input->post('codigo');
			$data['estado'] = $this->input->post('estado');
			$data['nombre'] = $this->input->post('nombre');

			$data['departamento_cod'] = $this->input->post('departamento_cod');
			$data['provincia_cod'] = $this->input->post('provincia_cod');
			$data['distrito_cod'] = $this->input->post('distrito_cod');
			$data['comunidad'] = $this->input->post('comunidad');
			$data['fecha'] = $this->input->post('fecha');

			/* section 3 */
			$data['antecedentes'] = $this->input->post('antecedentes');
			$data['situacion_actual'] = $this->input->post('situacion_actual');
			//$data['hechos_descripcion'] = $this->input->post('hechos_descripcion');
			$data['hechos_tipo'] = $this->input->post('hechos_tipo');

			$data['problema'] = $this->input->post('problema');
			$data['poblacion_afectada'] = $this->input->post('poblacion_afectada');
			$data['demanda'] = $this->input->post('demanda');
			$data['causas'] = $this->input->post('causas');

			$data['actores_1'] = $this->input->post('actores_1');
			$data['actores_2'] = $this->input->post('actores_2');
			$data['actores_3'] = $this->input->post('actores_3');

			$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

			//echo "<pre>";
			//print_r($data);
			$this->db->insert($this->schema . 'conflictos', $data);

			$conflicto_id = $this->db->insert_id();

			$conflicto_id_ceros = str_pad($conflicto_id, 5, "0", STR_PAD_LEFT);

			$codigo_final = 'C' . $conflicto_id_ceros . '-' . date('Y');

			$subdata['codigo'] = $codigo_final;

			$this->db->where('id', $conflicto_id)->update($this->schema . 'conflictos', $subdata);

			$otras_nombre_array = $this->input->post('otras_nombre');
			$otras_tipo_array = $this->input->post('otras_tipo');
			$otras_descripcion_array = $this->input->post('otras_descripcion');

			$hechos_descripcion_array = $this->input->post('hechos_descripcion');
			$hechos_fecha_array = $this->input->post('hechos_fecha');

			$tipos_array = $this->input->post('tipos');

			$posiciones_array = $this->input->post('posiciones');
			$intereses_array = $this->input->post('intereses');
			$valores_array = $this->input->post('valores');
			$necesidades_array = $this->input->post('necesidades');

			$imagenes = $this->subir();

			$data = array();

			foreach ($posiciones_array as $k => $v) {

				$data['posiciones'] = $v;
				$data['intereses'] = $intereses_array[$k];
				$data['valores'] = $valores_array[$k];
				$data['necesidades'] = $necesidades_array[$k];

				$data['conflictos_id'] = $conflicto_id;

				//$data['create_date'] = date('Y-m-d H:i:s');
				//$data['modify_date'] = date('Y-m-d H:i:s');
				$this->db->insert($this->schema . 'actores_primarios', $data);

			}

			$data = array();

			foreach ($tipos_array as $k => $v) {

				$data['tipos_id'] = $v;

				$data['conflictos_id'] = $conflicto_id;

				//$data['create_date'] = date('Y-m-d H:i:s');
				//$data['modify_date'] = date('Y-m-d H:i:s');
				$this->db->insert($this->schema . 'tipos_has_conflictos', $data);

			}

			$data = array();

			if (is_array($otras_nombre_array) && count($otras_nombre_array) > 0):
				foreach ($otras_nombre_array as $k => $v) {

					if (!empty($v)):
						$data['nombres'] = $v;
						$data['tipo'] = $otras_tipo_array[$k];
						$data['descripcion'] = $otras_descripcion_array[$k];
						$data['conflictos_id'] = $conflicto_id;
						$data['documento'] = (isset($imagenes[$k])) ? $imagenes[$k] : '';
						$data['create_date'] = date('Y-m-d H:i:s');
						$data['modify_date'] = date('Y-m-d H:i:s');
						$this->db->insert($this->schema . 'fuentes', $data);
					endif;
				}

			endif;
			$data = array();
			//echo var_dump($hechos_descripcion_array);
			if (is_array($hechos_descripcion_array) && count($hechos_descripcion_array) > 0):
				foreach ($hechos_descripcion_array as $k => $v) {

					if (!empty($v)):
						$data['hechos_descripcion'] = $v;
						$data['hechos_fecha'] = $hechos_fecha_array[$k];

						$data['conflictos_id'] = $conflicto_id;

						$data['create_date'] = date('Y-m-d H:i:s');

						$this->db->insert($this->schema . 'hechos', $data);
					endif;
				}
			endif;

			$_SESSION['codigo_final'] = $codigo_final;

			redirect(site_url('conflictos/finalizado/' . md5($conflicto_id)));
		}

	}

	function subir() {
		$imagenes = array();

		if (isset($_FILES['file']) && count($_FILES['file']) > 0 && (isset($_FILES['file']['name'][0]))):
			for ($i = 0; $i < count($_FILES['file']['name']); $i++):
				$target_path = "uploads/documentos/";
				$ext = explode('.', basename($_FILES['file']['name'][$i]));
				$file_name = md5(uniqid()) . '_' . basename($_FILES['file']['name'][$i]) . "." . $ext[count($ext) - 1];
				$target_path = $target_path . $file_name;

				if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path)) {

					//$in = array();

					$imagenes[$i] = $file_name;

					//$files_attach[] = base_url($target_path);

				} else {

				}
			endfor;
		endif;

		return $imagenes;

	}
	public function get_departamentos() {
		$w['tipo'] = 'departamento';

		$rows = $this->db->select('id,nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();
		$temp = array();
		foreach ($rows as $k => $v) {
			$temp[$v['id']] = $v['nombre'];
		}

		return $temp;
	}

	public function get_provincia($id) {
		$w['tipo'] = 'provincia';
		$w['id'] = $id;

		$rows = $this->db->select('id,nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();
		$temp = array();
		foreach ($rows as $k => $v) {
			$temp[$v['id']] = $v['nombre'];
		}

		return $temp;
	}

	public function get_distrito($id) {
		$w['tipo'] = 'distrito';
		$w['id'] = $id;

		$rows = $this->db->select('id,nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();
		$temp = array();
		foreach ($rows as $k => $v) {
			$temp[$v['id']] = $v['nombre'];
		}

		return $temp;
	}

	public function etapa_2() {
		$data = array();
		$data['content'] = $this->load->view('conflictos/etapa_2', $data, true);

		$this->load->view('template', $data, FALSE);
	}

	public function list_editar() {
		$data = array();
		$data['content'] = $this->load->view('conflictos/list_editar', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function listar_para_editar() {

		$order = $this->input->get('order');
		$sort = $this->input->get('sort');
		$filter = $this->input->get('filter');

		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$order = ($sort == '') ? 'desc' : $order;
		$sort = ($sort == '') ? 'id' : $sort;

		$filter = json_decode($filter, true);
		$flag = true;
		if (count($filter) > 0):
			foreach ($filter as $k => $v) {
				if ($flag):
					$this->db->like($k, $v);
					$flag = false;
				else:
					$this->db->or_like($k, $v);
				endif;
			}
		endif;
		/*

			order:asc
			offset:10
			limit:10

		*/
		$r = $this->db->select('*')->order_by($sort, $order)->limit($limit, $offset)->get($this->schema . 'conflictos')->result_array();

		$total = $this->db->select('id')->get($this->schema . 'conflictos')->num_rows();

		$final['total'] = $total;
		$final['rows'] = $r;
		echo json_encode($final);
	}
	function listar_acciones_por_conflicto() {

		$order = $this->input->get('order');
		$sort = $this->input->get('sort');
		$filter = $this->input->get('filter');

		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$order = ($sort == '') ? 'desc' : $order;
		$sort = ($sort == '') ? 'id' : $sort;

		$filter = json_decode($filter, true);
		$flag = true;
		if (count($filter) > 0):
			foreach ($filter as $k => $v) {
				if ($flag):
					$this->db->like($k, $v);
					$flag = false;
				else:
					$this->db->or_like($k, $v);
				endif;
			}
		endif;
		/*

			order:asc
			offset:10
			limit:10

		*/
		$r = $this->db->select('*')->order_by($sort, $order)->limit($limit, $offset)->get($this->schema . 'acciones_por_conflicto_2')->result_array();

		$total = $this->db->select('id')->get($this->schema . 'conflictos')->num_rows();

		$final['total'] = $total;
		$final['rows'] = $r;
		echo json_encode($final);
	}

	function delete() {

		$id = $this->input->post('id');
		$this->db->where('id', $id)->limit(1)->delete($this->schema . 'fuentes');
	}

	function delete_hecho() {

		$id = $this->input->post('id');
		$this->db->where('id', $id)->limit(1)->delete($this->schema . 'hechos');
	}

	function delete_primario() {

		$id = $this->input->post('id');
		$this->db->where('id', $id)->limit(1)->delete($this->schema . 'actores_primarios');
	}

	function imprimir($id) {

		$data['row'] = $row = $this->db->select('*')->limit(1)->where('id', $id)->get($this->schema . 'conflictos')->row_array();

		$tipos = $this->db->select('*')->get($this->schema . 'tipos')->result_array();
		$data['tipos'] = $tipos;

		$estados = $this->db->select('*')->get($this->schema . 'estados')->result_array();
		$data['estados'] = $estados;

		$mis_tipos = $this->db->select('tipos_id')->where('conflictos_id', $id)->get($this->schema . 'tipos_has_conflictos')->result_array();
		$mis_tipos_array = array();
		foreach ($mis_tipos as $k => $v) {
			$mis_tipos_array[] = $v['tipos_id'];
		}
		$data['mis_tipos'] = $mis_tipos_array;

		$data['otras'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'fuentes')->result_array();

		$data['hechos'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'hechos')->result_array();

		$data['primarios'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'actores_primarios')->result_array();

		$departamentos = $this->get_departamentos();
		$provincia = $this->get_provincia($row['provincia_cod']);
		$distrito = $this->get_distrito($row['distrito_cod']);
		$data['departamentos'] = $departamentos;

		$data['provincia'] = $provincia;
		$data['distrito'] = $distrito;

		$data['content'] = $this->load->view('conflictos/imprimir', $data, true);

		$this->load->view('template-imprimir', $data, FALSE);
	}

	function pdf($id) {

		$data['row'] = $row = $this->db->select('*')->limit(1)->where('id', $id)->get($this->schema . 'conflictos')->row_array();

		$data['otras'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'fuentes')->result_array();
		$departamentos = $this->get_departamentos();
		$provincia = $this->get_provincia($row['provincia_cod']);
		$distrito = $this->get_distrito($row['distrito_cod']);
		$data['departamentos'] = $departamentos;

		$data['provincia'] = $provincia;
		$data['distrito'] = $distrito;

		$data['content'] = $this->load->view('conflictos/imprimir', $data, true);

		$content = $this->load->view('template-pdf', $data, true);

		try
		{
			$html2pdf = new HTML2PDF('P', 'A4', 'es');
			$html2pdf->setTestTdInOnePage(false);
			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
			$html2pdf->Output('exemple11.pdf');
		} catch (HTML2PDF_exception $e) {
			echo $e;
			exit;
		}

	}

	function exportar_pdf($id) {

		//$tipos = $this->db->select('*')->get($this->schema . 'tipos')->result_array();
		$tipos = $this->tipos_modelo->get_active();
		$data['tipos'] = $tipos;

		//$estados = $this->db->select('*')->get($this->schema . 'estados')->result_array();
		$estados = $this->estados_modelo->get_active();
		$data['estados'] = $estados;

		/****************/
		$mis_tipos = $this->db->select('tipos_id')->where('conflictos_id', $id)->get($this->schema . 'tipos_has_conflictos')->result_array();
		$mis_tipos_array = array();
		foreach ($mis_tipos as $k => $v) {
			$mis_tipos_array[] = $v['tipos_id'];
		}
		$data['mis_tipos'] = $mis_tipos_array;
		/****************/

		$data['row'] = $row = $this->db->select('*')->limit(1)->where('id', $id)->get($this->schema . 'conflictos')->row_array();

		$data['otras'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'fuentes')->result_array();

		$data['hechos'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'hechos')->result_array();

		$data['primarios'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'actores_primarios')->result_array();

		$data['evaluaciones'] = $this->evaluaciones_modelo->get_all_parent($id);
		$data['monitoreo'] = $monitoreo = $this->monitoreo_modelo->get_by_conflicto($id);

		foreach ($monitoreo as $k => $v) {

			$data['monitoreo_responsables'][$v['id']] = $this->monitoreo_responsables_modelo->get_all_parent($v['id'], false);

		}

		$departamentos = $this->get_departamentos();
		$provincia = $this->get_provincia($row['provincia_cod']);
		$distrito = $this->get_distrito($row['distrito_cod']);
		$data['departamentos'] = $departamentos;

		$data['provincia'] = $provincia;
		$data['distrito'] = $distrito;

		$data['content'] = $this->load->view('conflictos/ficha_pdf', $data, true);

		$content = $this->load->view('template-pdf', $data, true);

		$mpdf = new mPDF();
		$mpdf->WriteHTML($content);
		$mpdf->Output();
/*
// instantiate and use the dompdf class
$dompdf = new Dompdf();

$dompdf->loadHtml($content);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
 */
/*
try
{
$html2pdf = new HTML2PDF('P', 'A4', 'es');
$html2pdf->setTestTdInOnePage(false);
$html2pdf->writeHTML($content);
$html2pdf->Output('exemple11.pdf');
} catch (HTML2PDF_exception $e) {
echo $e;
exit;
}
 */
	}
	function editar($id) {
		$data = array();

		$this->load->library('form_validation');

		$this->form_validation->set_rules('actor_reporta_nombre', 'Actor', 'required');

		if ($this->form_validation->run() == FALSE) {

			//$tipos = $this->db->select('*')->get($this->schema . 'tipos')->result_array();
			$tipos = $this->tipos_modelo->get_active();
			$data['tipos'] = $tipos;

			//$estados = $this->db->select('*')->get($this->schema . 'estados')->result_array();
			$estados = $this->estados_modelo->get_active();
			$data['estados'] = $estados;

			/****************/
			$mis_tipos = $this->db->select('tipos_id')->where('conflictos_id', $id)->get($this->schema . 'tipos_has_conflictos')->result_array();
			$mis_tipos_array = array();
			foreach ($mis_tipos as $k => $v) {
				$mis_tipos_array[] = $v['tipos_id'];
			}
			$data['mis_tipos'] = $mis_tipos_array;
			/****************/

			$row = $this->db->select('*')->limit(1)->where('id', $id)->get($this->schema . 'conflictos')->row_array();

			$data['row'] = $row;
			$data['otras'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'fuentes')->result_array();

			$data['hechos'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'hechos')->result_array();

			$data['primarios'] = $this->db->select('*')->order_by('id', 'asc')->where('conflictos_id', $id)->get($this->schema . 'actores_primarios')->result_array();

			$departamentos = $this->get_departamentos();
			$provincia = $this->get_provincia($row['provincia_cod']);
			$distrito = $this->get_distrito($row['distrito_cod']);
			$data['departamentos'] = $departamentos;

			$data['provincia'] = $provincia;
			$data['distrito'] = $distrito;

			$data['content'] = $this->load->view('conflictos/editar', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {

			/* section 1 */
			$conflictos_id = $this->input->post('id');
			$data['actor_reporta_nombre'] = $this->input->post('actor_reporta_nombre');
			$data['actor_reporta_dni'] = $this->input->post('actor_reporta_dni');
			$data['actor_reporta_tipo'] = $this->input->post('actor_reporta_tipo');
			$data['actor_reporta_observacion'] = $this->input->post('actor_reporta_observacion');

			/* section 2 */
			$data['codigo'] = $this->input->post('codigo');
			$data['estado'] = $this->input->post('estado');
			$data['nombre'] = $this->input->post('nombre');

			$data['departamento_cod'] = $this->input->post('departamento_cod');
			$data['provincia_cod'] = $this->input->post('provincia_cod');
			$data['distrito_cod'] = $this->input->post('distrito_cod');
			$data['comunidad'] = $this->input->post('comunidad');
			$data['fecha'] = $this->input->post('fecha');

			/* section 3 */
			$data['antecedentes'] = $this->input->post('antecedentes');
			$data['situacion_actual'] = $this->input->post('situacion_actual');
			//$data['hechos_descripcion'] = $this->input->post('hechos_descripcion');
			$data['hechos_tipo'] = $this->input->post('hechos_tipo');

			$data['problema'] = $this->input->post('problema');
			$data['poblacion_afectada'] = $this->input->post('poblacion_afectada');
			$data['demanda'] = $this->input->post('demanda');
			$data['causas'] = $this->input->post('causas');

			$data['actores_1'] = $this->input->post('actores_1');
			$data['actores_2'] = $this->input->post('actores_2');
			$data['actores_3'] = $this->input->post('actores_3');

			//	$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

			$this->db->where('id', $conflictos_id)->limit(1)->update($this->schema . 'conflictos', $data);
			//$conflicto_id = $this->db->insert_id();
			/*********************************************************************/
			$conflicto_id_ceros = str_pad($conflictos_id, 5, "0", STR_PAD_LEFT);

			$codigo_final = 'C' . $conflicto_id_ceros . '-' . date('Y');

			$subdata['codigo'] = $codigo_final;

			$this->db->where('id', $conflictos_id)->update($this->schema . 'conflictos', $subdata);
			/*********************************************************************/

			$otras_nombre_array = $this->input->post('otras_nombre');
			$otras_id_array = $this->input->post('otras_id');
			$otras_tipo_array = $this->input->post('otras_tipo');
			$otras_descripcion_array = $this->input->post('otras_descripcion');

			$hechos_descripcion_array = $this->input->post('hechos_descripcion');
			$hechos_fecha_array = $this->input->post('hechos_fecha');
			$hechos_id_array = $this->input->post('hechos_id');

			$tipos_array = $this->input->post('tipos');

/* primario */
			$posiciones_array = $this->input->post('posiciones');
			$intereses_array = $this->input->post('intereses');
			$valores_array = $this->input->post('valores');
			$necesidades_array = $this->input->post('necesidades');

/* fin primario*/

			$data = array();
			$this->db->where('conflictos_id', $id)->delete($this->schema . 'actores_primarios');
			foreach ($posiciones_array as $k => $v) {

				$data['posiciones'] = $v;
				$data['intereses'] = $intereses_array[$k];
				$data['valores'] = $valores_array[$k];
				$data['necesidades'] = $necesidades_array[$k];

				$data['conflictos_id'] = $conflictos_id;

				//$data['create_date'] = date('Y-m-d H:i:s');
				//$data['modify_date'] = date('Y-m-d H:i:s');
				$this->db->insert($this->schema . 'actores_primarios', $data);

			}

			$data = array();

			$this->db->where('conflictos_id', $id)->delete($this->schema . 'tipos_has_conflictos');

			foreach ($tipos_array as $k => $v) {

				$data['tipos_id'] = $v;

				$data['conflictos_id'] = $conflictos_id;

				//$data['create_date'] = date('Y-m-d H:i:s');
				//$data['modify_date'] = date('Y-m-d H:i:s');
				$this->db->insert($this->schema . 'tipos_has_conflictos', $data);

			}

			$data = array();
//			echo $conflictos_id;
			foreach ($otras_nombre_array as $k => $v) {

				$data['nombres'] = $v;
				$id = $otras_id_array[$k];
				$data['tipo'] = $otras_tipo_array[$k];
				$data['descripcion'] = $otras_descripcion_array[$k];
				$data['conflictos_id'] = $conflictos_id;
				//$data['create_date'] = date('Y-m-d H:i:s');
				$data['modify_date'] = date('Y-m-d H:i:s');
				if ($id != ''):
					$this->db->where('id', $id)->limit(1)->update($this->schema . 'fuentes', $data);
				else:
					$data['create_date'] = date('Y-m-d H:i:s');
/*
echo "<pre>";
print_r($data);
 */
					$this->db->insert($this->schema . 'fuentes', $data);
					//$conflictos_id = $this->db->insert_id();
				endif;

			}

			$data = array();
			foreach ($hechos_descripcion_array as $k => $v) {

				if (empty($hechos_fecha_array[$k])):
					continue;
				endif;
				$data['hechos_descripcion'] = $v;
				$id = $hechos_id_array[$k];
				$data['hechos_fecha'] = $hechos_fecha_array[$k];

				$data['conflictos_id'] = $conflictos_id;
				//$data['create_date'] = date('Y-m-d H:i:s');
				//$data['modify_date'] = date('Y-m-d H:i:s');
				if ($id != ''):
					$this->db->where('id', $id)->limit(1)->update($this->schema . 'hechos', $data);
				else:
					$data['create_date'] = date('Y-m-d H:i:s');
					$this->db->insert($this->schema . 'hechos', $data);
					$conflictos_id = $this->db->insert_id();
				endif;

			}

			$_SESSION['codigo_final'] = $codigo_final;
			redirect(site_url('conflictos/actualizado/' . md5($conflictos_id)));
		}

	}

	function actualizado($id) {

		$data = array();

		$data['content'] = $this->load->view('conflictos/actualizado', $data, true);

		$this->load->view('template', $data, FALSE);
	}

	function finalizado($id) {

		$data = array();

		$data['content'] = $this->load->view('conflictos/finalizado', $data, true);

		$this->load->view('template', $data, FALSE);
	}
}

<h4>Analisis del conflicto/denuncia</h4>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Codigo:</label>
	<div class="col-sm-4">
		<input type="text" name="codigo" id="codigo" disabled="disabled" data-rule-required="true" class="form-control" value="AUTOGENERADO"   title="">
	</div>
	<label class="control-label col-sm-2" for="email">Estado:</label>
	<div class="col-sm-3">
		<select name="estado" id="estado" class="form-control" data-rule-required="true">
			<option value="">Seleccione</option>



			 <?php foreach ($estados as $k => $v): ?>

			<option value="<?php echo $v['id']; ?>"><?php echo $v['nombre']; ?></option>
			 <?php endforeach?>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Nombre </label>
	<div class="col-sm-9">
		<input type="text" data-rule-required="true"  class="form-control input-sm" id="nombre" name="nombre" placeholder="">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Departamento</label>
	<div class="col-sm-3">
		<select name="departamento_cod" id="departamento" class="form-control input-sm" data-rule-required="true" >
			<option>Seleccione</option>
			<?php foreach ($departamentos as $k => $v) {
	?>
			<option   value="<?PHP echo $k; ?>"> <?PHP echo $v; ?> </option>
			<?PHP
}?>
		</select>
	</div>
	<label class="control-label col-sm-1 col-xs-12" for="email">Provincia</label>
	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		<select name="provincia_cod" id="provincia" class="form-control input-sm"  data-rule-required="true" >
			<option value="">seleccione</option>
		</select>
	</div>
	<label class="control-label col-sm-1 col-xs-12" for="email">Distrito</label>
	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		<select name="distrito_cod" id="distrito" class="form-control input-sm"  data-rule-required="true" >
			<option value="">seleccione</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Comunidad:</label>
	<div class="col-sm-4">
		<input type="text" name="comunidad" class="form-control input-sm" data-rule-required="true"  id="" placeholder="">
	</div>
	<label class="control-label col-sm-2" for="email">Fecha:</label>
	<div class="col-sm-3">
		<input type="text" name="fecha" id="fecha" maxlength="10" minlength="10" data-rule-maxlength=10 data-rule-minlength=10 data-rule-validDate="true"   class="fecha form-control input-sm"  data-rule-required="true"  title="">
	</div>
</div>
<h4>Perfil del conflicto </h4>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Antecedentes:</label>
	<div class="col-sm-9">
		<input type="text" name="antecedentes" data-rule-required="true"  class="form-control input-sm" id="antecedentes" placeholder="">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Situacion Actual:</label>
	<div class="col-sm-9">
		<input type="text" name="situacion_actual" class="form-control input-sm" id="situacion_actual" data-rule-required="true"  placeholder="">
	</div>
</div>
<hr>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Hechos:</label>
	<div class="col-sm-9">
		<ol id="hechos_clonar_content">
			<li id="hechos_clonar" class="hechos_clon">
				<div class="row">
					<div class="col-sm-1">
						<a href="javascript:void(0);" class="btn btn-danger boton_quitar_hechos pull-right"> Quitar </a>
					</div>
					<div class="col-sm-9">
						<input name="hechos_descripcion[]" placeholder="Descripción" value="" type="text" class="form-control">
					</div>
					<div class="col-sm-2">
						<input type="text" name="hechos_fecha[]" placeholder="fecha" id="input" class="fecha  datepicker_recurring_start form-control" value="" >
					</div>
					<div class="clearfix"></div>
				</div>
			</li>
		</ol>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-10">
				<a href="#" class="btn btn-default" id="boton_clonar_hechos"> Agregar Hecho </a>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Tipo:</label>
	<div class="col-sm-9">

 <?php foreach ($tipos as $k => $v): ?>
 	<div class="checkbox">
 		<label>
 			<input type="checkbox" name="tipos[]" value="<?php echo $v['id']; ?>">
 			<?php echo $v['nombre']; ?>
 		</label>
 	</div>
 <?php endforeach?>


	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Problema:</label>
	<div class="col-sm-9">
		<input type="text" name="problema" id="problema" class="form-control input-sm" value="" data-rule-required="true" >
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Poblacion afectada:</label>
	<div class="col-sm-4">
		<input type="text" data-rule-number="true" name="poblacion_afectada" id="inputPoblacion_afectada" class="form-control" value="" required="required" pattern="" title="">
	</div>
	<label class="control-label col-sm-2" for="email">Demanda:</label>
	<div class="col-sm-3">
		<input type="text" name="demanda" id="demanda" class="form-control input-sm input-sm" value="" data-rule-required="true" >
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Causas:</label>
	<div class="col-sm-9">
		<input type="text" name="causas" id="causas" class="form-control input-sm input-sm" data-rule-required="true" >
	</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-default" id="boton_form_2_back">Volver</button>
	</div>
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_2">Continuar</button>
	</div>
</div>
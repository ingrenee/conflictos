       <form method="post" class="" id="<?php echo $form_id; ?>" data-target="<?php echo $data_target; ?>" action="<?php echo $action; ?>">
            <input type="hidden" name="ajax" value="1">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-3">
                        Departamento:
                        <select name="departamento" class="form-control input-sm" id="departamento">
                            <option value="">Seleccione</option>
                            <?php foreach ($departamentos as $k => $v): ?>
                            <option value="<?php echo $k; ?>">
                                <?php echo $v; ?>
                            </option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        Provincia:
                        <select class="form-control input-sm" name="provincia" id="provincia">
                            <option value=""> Seleccione </option>
                        </select>
                    </div>
                    <div class="col-sm-3">Distrito:
                        <select class="form-control input-sm" name="distrito" id="distrito">
                            <option value=""> Seleccione </option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary btn-sm"> Mostrar </button>
                    </div>
                </div>
            </div>
        </form>
<?php
class Conflictos_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_for_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.conflictos')->row_array();
	}

	public function get_where($w) {

		return $this->db->select('*')->where($w)->get('conflictos.conflictos')->result_array();
	}

	public function get_where_tipo($w) {

		return $this->db->select('*')->where('Y.tipos_id', $w['tipo'])->where('"Y"."conflictos_id"', '"X"."id"', false)->get('conflictos.conflictos X, conflictos.tipos_has_conflictos Y')

			->result_array();
	}
	function get_sin_estado() {
		/*	$ids = $this->db->select('id')->get('conflictos.estados')->result_array();
			$ides = array();
			foreach ($ids as $k => $v) {
				$ides[] = $v['id'];
			}

			return $this->db->select('*')->where_not_in('estado', ($ides))->get('conflictos.conflictos')->result_array();
		*/

		$this->db->select('X.*');
		$this->db->from('conflictos.conflictos X');

		$this->db->order_by('Y.id', 'asc');
		$this->db->where('Y.nombre is null');
		$this->db->join('conflictos.estados Y', 'X.estado = Y.id', 'left');
		return $this->db->get()->result_array();

	}
	public function get_by_estado($w = array()) {
		$this->db->select('Y.nombre as estado_nombre, Y.id as estado,count(0) as total');
		$this->db->from('conflictos.conflictos X');
		if (!empty($w)):
			$this->db->where($w);
		endif;
		$this->db->group_by('Y.id');
		$this->db->order_by('Y.id', 'asc');
		$this->db->join('conflictos.estados Y', 'X.estado = Y.id', 'left');
		return $this->db->get()->result_array();
	}

	function get_sin_tipo() {
		$this->db->select("*");
		$this->db->from('conflictos.tipos_has_conflictos X');

		$this->db->join('conflictos.conflictos Y', 'Y.id=X.conflictos_id ', 'RIGHT OUTER');
		$this->db->where('"X"."tipos_id" is null', '', false);
		return $r2 = $this->db->get()->result_array();

	}
	public function get_by_tipo($w) {
		$this->db->select('Y.nombre as tipo_nombre,Y.id as tipo, count(0) as total');
		$this->db->from('conflictos.tipos_has_conflictos_plus X');
		if (!empty($w)):
			$this->db->where($w);
		endif;
		$this->db->group_by('Y.id');
		$this->db->order_by('Y.id', 'asc');
		$this->db->join('conflictos.tipos Y', 'X.tipos_id = Y.id');

		$r1 = $this->db->get()->result_array();

		$this->db->select("'No seleccionado' as tipo_nombre,0 as tipo, count(0) as total");
		$this->db->from('conflictos.tipos_has_conflictos_plus X');
		if (!empty($w)):
			$this->db->where($w);
		endif;
		$this->db->group_by('X.tipos_id');

		$this->db->join('conflictos.conflictos Y', 'Y.id=X.conflictos_id ', 'RIGHT OUTER');
		$this->db->where('"X"."tipos_id" is null', '', false);
		$r2 = $this->db->get()->result_array();

		return array_merge($r1, $r2);

	}
	public function get_by_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.conflictos')->row_array();
	}

}

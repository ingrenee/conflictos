<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Monitoreo > Listado de registros</h3>
	</div>
	<div class="panel-body">

 <div class="well well-sm well-success">
 Conflicto: <?php echo $conflicto['nombre']; ?>
 </div>

 <table id="table" data-toggle="table" data-url="<?php echo site_url('monitoreo/listado_monitoreo/' . $conflictos_id); ?>"

			data-side-pagination="server"
               data-pagination="true"
               data-page-list="[5, 10, 20, 50, 100, 200]"
               data-search="true"
data-filter-control="true"
           data-filter-show-clear="true"
 >
    <thead>
        <tr>
            <th data-field="id" data-sortable="true">ID</th>
            <th data-field="fase" data-sortable="true" data-filter-control="select">Fase</th>
            <th data-field="fecha" data-sortable="true" data-filter-control="input">Fecha</th>
            <th data-field="acciones" data-sortable="true"  data-filter-control="select">Acciones</th>
            <th data-field="desencadenante"  data-sortable="true"  data-filter-control="input" >Desencadenante</th>

            <th data-field="id"  data-formatter="accionesFormatter">Acciones</th>
        </tr>
    </thead>
</table>



	</div>
</div>
 <script type="text/javascript">

 	function accionesFormatter(value, row) {
       // var icon = row.id % 2 === 0 ? 'glyphicon-star' : 'glyphicon-star-empty'
       var icon='glyphicon-pencil';
       var icon2='glyphicon glyphicon-list';
link= _helper_site_url('monitoreo/monitoreo_editar/'+$.md5(row.id));
link2= _helper_site_url('monitoreo/monitoreo_ver/'+$.md5(row.id));
link3= _helper_site_url('monitoreo/agregar_documentos/'+$.md5(row.id));
       return '<a href="'+link+'" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon + '"></i>  Editar Monitoreo </a> <a href="'+link2+'"                            target="" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon2 + '"></i>  Ver Monitoreo </a> <a href="'+link3+'"                            target="" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon2 + '"></i>  Agregar documentos </a>';
    }


    var $table = $('#table');
    $(function () {
    });

 </script>
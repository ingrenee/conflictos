<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reportes extends CI_Controller {
	var $id;
	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Tipos_model', 'tipos_modelo');
		$this->load->model('Conflictos_model', 'conflictos_modelo');
		$this->load->model('Ubigeo_model', 'ubigeo_modelo');
		$this->load->model('Monitoreo_model', 'monitoreo_modelo');
		$this->load->model('Estados_model', 'estados_modelo');
		// $this->load->model('Concejos_regionales_model', 'modelo_concejos_regionales');
	}

	function mostrar_grafico_acciones($id) {
		$data = array();

		$rows = $this->monitoreo_modelo->get_by_conflicto($id);

		$data['rows'] = $rows;

		$this->load->view('reportes/mostrar_grafico_acciones', $data);

	}
	function conflictos_por_estado() {

		$data = array();

		$ajax = $this->input->post('ajax');
		$departamento = (int) trim($this->input->post('departamento'));
		$provincia = (int) trim($this->input->post('provincia'));
		$distrito = (int) trim($this->input->post('distrito'));

		$w = array();
		if (!empty($departamento) && $departamento > 0):
			$w['X.departamento_cod'] = $departamento;
		endif;
		if (!empty($provincia) && $provincia > 0):
			$w['X.provincia_cod'] = $provincia;
		endif;
		if (!empty($distrito) && $distrito > 0):
			$w['X.distrito_cod'] = $distrito;
		endif;
		$rows = $this->conflictos_modelo->get_by_estado($w);

		$data['departamentos'] = $this->ubigeo_modelo->get_departamentos();
		$data['rows'] = $rows;

		if (!empty($ajax)):
			echo $data['content'] = $this->load->view('reportes/conflictos_por_estado_part', $data, true);
		else:

			$data['content'] = $this->load->view('reportes/conflictos_por_estado', $data, true);
			$this->load->view('template', $data);
		endif;

	}
	function conflictos_por_tipo() {

		$data = array();

		$ajax = $this->input->post('ajax');
		$departamento = (int) trim($this->input->post('departamento'));
		$provincia = (int) trim($this->input->post('provincia'));
		$distrito = (int) trim($this->input->post('distrito'));

		$w = array();
		if (!empty($departamento) && $departamento > 0):
			$w['X.departamento_cod'] = $departamento;
		endif;
		if (!empty($provincia) && $provincia > 0):
			$w['X.provincia_cod'] = $provincia;
		endif;
		if (!empty($distrito) && $distrito > 0):
			$w['X.distrito_cod'] = $distrito;
		endif;

		$rows = $this->conflictos_modelo->get_by_tipo($w);
		$data['departamentos'] = $this->ubigeo_modelo->get_departamentos();
		$data['rows'] = $rows;

		if (!empty($ajax)):
			echo $this->load->view('reportes/conflictos_por_tipo_part', $data, true);
		else:

			$data['content'] = $this->load->view('reportes/conflictos_por_tipo', $data, true);
			$this->load->view('template', $data);
		endif;

	}

	function acciones_por_conflicto() {

		$data = array();

		$data['content'] = $this->load->view('reportes/acciones_por_conflicto', $data, true);
		$this->load->view('template', $data);

	}

	function conflictos_por_tipo_excel($tipo = false) {

		if ($tipo === false || empty($tipo) || $tipo == 0):

			$rows = $this->conflictos_modelo->get_sin_tipo();
		else:
			$w['tipo'] = $tipo;
			$rows = $this->conflictos_modelo->get_where_tipo($w);

		endif;
		$data['departamentos'] = $this->ubigeo_modelo->get_departamentos();
		$data['provincias'] = $this->ubigeo_modelo->get_provincias();
		$data['distritos'] = $this->ubigeo_modelo->get_distritos();
		if ($tipo === false):
			$data['tipo'] = array('nombre' => '');
		else:
			$data['tipo'] = $this->tipos_modelo->get($tipo);
		endif;
		$data['rows'] = $rows;
		$data['content'] = $this->load->view('reportes/conflictos_por_tipo_excel', $data, true);
		$this->load->view('template-imprimir-2', $data);
	}
	function conflictos_por_estado_excel($estado = false) {

		if ($estado === false || empty($estado)):

			$rows = $this->conflictos_modelo->get_sin_estado();
		else:
			$w['estado'] = $estado;
			$rows = $this->conflictos_modelo->get_where($w);

		endif;
		$data['departamentos'] = $this->ubigeo_modelo->get_departamentos();
		$data['provincias'] = $this->ubigeo_modelo->get_provincias();
		$data['distritos'] = $this->ubigeo_modelo->get_distritos();
		if ($estado === false):
			$data['estado'] = array('nombre' => '');
		else:
			$data['estado'] = $this->estados_modelo->get($estado);
		endif;
		$data['rows'] = $rows;
		$data['content'] = $this->load->view('reportes/conflictos_por_estado_excel', $data, true);
		$this->load->view('template-imprimir-2', $data);
	}
	public function index() {
		$data = array();
		$data['tab'] = 1;
		$data['menu'] = 2;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre', 'Nombre', 'required|callback_verifica_nombre', array('verifica_nombre' => 'El estado ya existe.'));

		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['nombre'] = $this->input->post('nombre');
			$p['descripcion'] = ($this->input->post('descripcion'));

			$temp = $this->tipos_modelo->save($p);

			// $temp = _help_get_message($temp);
			if ($temp > 0):
				$this->session->set_flashdata('mensaje', ' Se agrego a un nuevo estado:');
			else:
				$this->session->set_flashdata('mensaje', 'Se agrego un nuevo estado');
			endif;
			redirect(site_url('tipos/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('tipos/agregar', $data, true);
		$data['content'] = $this->load->view('tipos/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	public function estado($estado = 1, $id = false) {
		$estado = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$this->modelo_usuarios->set_usuarios_estado($estado, $id);
		redirect(site_url('usuarios/listar'));
	}
	public function editar($id) {
		$data = array();
		$data['tab'] = 2;
		$id = (int) $this->uri->segment(3);
		$this->id = $id;
		$data['id'] = $id;
		if ($id <= 0) {
			redirect(site_url('tipos/index'));
		}

		$p = array();
		$p[] = $id;
		$data['row'] = $this->tipos_modelo->get($id);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('nombre', 'Nombre', 'required|callback_verifica_nombre', array('verifica_nombre' => 'El estado ya  existe.'));
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['id'] = $this->input->post('id');
			$p['nombre'] = ($this->input->post('nombre'));
			$p['descripcion'] = $this->input->post('descripcion');

			$temp = $this->tipos_modelo->update($p, $id);

			if ($temp == 1):
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente.');
			else:
				$this->session->set_flashdata('mensaje', 'No existe el grupo');
			endif;
			redirect(site_url('tipos/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('tipos/editar', $data, true);
		$data['content'] = $this->load->view('tipos/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	function verifica_nombre($str) {

		if ($this->tipos_modelo->get_verifica_nombre($str, $this->id) > 0) {
			return false;
		} else {
			return true;
		}
	}

	function listar() {
		$data = array();
		$data['menu'] = 1;
		$data['tab'] = 3;

		//usp_authbear_users_list
		$data['rows'] = $this->tipos_modelo->get_list()->result_array();

		$data['subcontent'] = $this->load->view('tipos/listar', $data, true);

		$data['content'] = $this->load->view('tipos/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	function quitar_grupo() {
		$id = (int) $this->uri->segment(3);
		$users_id = (int) $this->uri->segment(4);
		$p['id'] = $id;
		$temp = $this->modelo_usuarios->deleting_users_groups($id);

		$this->session->set_flashdata('mensaje', 'El grupo quitado');
		redirect(site_url('usuarios/grupos_rel/' . $users_id));
	}

}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Conflictos por tipo</h3>
    </div>
    <div class="panel-body">
        <table class="table table-bordered  highchart" data-graph-container=".grafico" data-graph-type="column" data-graph-yaxis-X-stacklabels-enabled="0" data-graph-yaxis-1-tick-interval="1" data-graph-yaxis-1-min=0 data-graph-xaxis-rotation="-45">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Número de conflictos</th>
                    <th data-graph-skip="1"> Excel</th>
                    <th data-graph-skip="1" class="hidden">PDF</th>
                </tr>
            </thead>
            <?php foreach ($rows as $k => $v): ?>
            <tr>
                <td>
                    <?php echo (empty($v['tipo_nombre'])) ? 'No definido' : $v['tipo_nombre']; ?>
                </td>
                <td class="total">
                    <?php echo $v['total']; ?>
                </td>
                <td>
                    <a target="_blank" class="btn btn-xs btn-primary" href="<?php echo site_url('reportes/conflictos_por_tipo_excel/' . $v['tipo']); ?>">
                        <?php _help_icono('save')?>
                    </a>
                </td>
                <td class="hidden"></td>
            </tr>
            <?php endforeach?>
        </table>
        <hr>
        <div class="grafico">
        </div>
    </div>
</div>
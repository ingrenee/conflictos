<?php
if (!isset($menu)):
	$menu = -1;
endif;
?>


<h1 class="titular"> Grupos  </h1>


<div class="btn-group">
	<a href="<?php echo site_url('grupos/listar'); ?>" type="button" class=" <?php echo _helper_active($menu, 1, 'active'); ?> btn btn-sm btn-default">
	<?php echo _helper_icono('list') ?> Listar grupos</a>
	<a href="<?php echo site_url('grupos'); ?>" type="button" class=" <?php echo _helper_active($menu, 2, 'active'); ?> btn btn-sm btn-default">
 <?php echo _helper_icono('plus') ?>
	Agregar grupo</a>

</div>

<hr>

<?php echo $subcontent; ?>


<div class="panel panel-default" id="print">
    <div class="panel-heading">
        <h3 class="panel-title">Conflictos por estado</h3>
    </div>
    <div class="panel-body">
        <!--  barra -->

        <!--  fin barra -->
        <table class="table table-bordered ignore-pdf  highchart" data-graph-container=".grafico" data-graph-type="column" data-graph-yaxis-X-stacklabels-enabled="0" data-graph-yaxis-1-tick-interval="1" data-graph-yaxis-1-min=0 data-graph-xaxis-rotation="-90">
            <thead>
                <tr>
                    <th>Estado</th>
                    <th>Número de conflictos</th>
                    <th data-graph-skip="1"> Excel</th>
                    <th class="hidden" data-graph-skip="1">PDF</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $k => $v): ?>
                <tr>
                    <td>
                        <?php echo (empty($v['estado_nombre'])) ? 'No definido' : $v['estado_nombre']; ?>
                    </td>
                    <td class="total">
                        <?php echo $v['total']; ?>
                    </td>
                    <td>
                        <a target="_blank" class="btn btn-xs btn-primary" href="<?php echo site_url('reportes/conflictos_por_estado_excel/' . $v['estado']); ?>">
                            <?php _help_icono('save')?>
                        </a>
                    </td>
                    <td class="hidden"></td>
                </tr>
                <?php endforeach?>
            </tbody>
            <tr>
                <td>Total:</td>
                <td class="js_sumar" data-target="total"></td>
            </tr>
        </table>
        <div class="grafico" id="ignorepdf">
        </div>
    </div>
</div>
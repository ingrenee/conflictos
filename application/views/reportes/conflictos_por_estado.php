<button id="cmd" class="hidden">generate PDF</button>
<script type="text/javascript">
// create canvas function from highcharts example http://jsfiddle.net/highcharts/PDnmQ/
(function(H) {
    H.Chart.prototype.createCanvas = function(divId) {
        var svg = this.getSVG(),
            width = parseInt(svg.match(/width="([0-9]+)"/)[1]),
            height = parseInt(svg.match(/height="([0-9]+)"/)[1]),
            canvas = document.createElement('canvas');

        canvas.setAttribute('width', width);
        canvas.setAttribute('height', height);

        if (canvas.getContext && canvas.getContext('2d')) {

            canvg(canvas, svg);

            return canvas.toDataURL("image/jpeg");

        } else {
            alert("Your browser doesn't support this feature, please use a modern browser");
            return false;
        }

    }
}(Highcharts));



$(function() {


    (function(H) {
        H.Chart.prototype.createCanvas = function(divId) {
            var svg = this.getSVG(),
                width = parseInt(svg.match(/width="([0-9]+)"/)[1]),
                height = parseInt(svg.match(/height="([0-9]+)"/)[1]),
                canvas = document.createElement('canvas');

            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);

            if (canvas.getContext && canvas.getContext('2d')) {

                canvg(canvas, svg);

                return canvas.toDataURL("image/jpeg");

            } else {
                alert("Your browser doesn't support this feature, please use a modern browser");
                return false;
            }

        }
    }(Highcharts));



    var specialElementHandlers = {
        '#ignorepdf': function(element, renderer) {

            return false;
        }
    };
    $('#cmd').click(function() {
        var doc = new jsPDF();

        var chartHeight = 80;

        // All units are in the set measurement for the document
        // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
        doc.setFontSize(40);

        html = $('#print').html();
        html = jQuery(jQuery(html).find("#ignorepdf").remove()).html();
        doc.fromHTML(html, 15, 15, {
            'width': 600,
            'elementHandlers': specialElementHandlers
        });
        //loop through each chart
        $('.grafico').each(function(index) {
            var imageData = $(this).highcharts().createCanvas();

            // add image to doc, if you have lots of charts,
            // you will need to check if you have gone bigger
            // than a page and do doc.addPage() before adding
            // another image.

            /**
             * addImage(imagedata, type, x, y, width, height)
             */

            //chartHeight=150;
            doc.addImage(imageData, 'JPEG', 45, (index * chartHeight) + 40, 120, chartHeight);
        });

        doc.save('sample-file.pdf');
    });
});
</script>

 <?php
$subdata = array();
$subdata['data_target'] = '#ajax_contenido';
$subdata['action'] = site_url('reportes/conflictos_por_estado');
$subdata['form_id'] = 'form_ajax';
$this->load->view('reportes/form_ubigeo', $subdata);?>


<div id="ajax_contenido">
    <?php $this->load->view('reportes/conflicto_por_estado_part');?>

</div>

<script type="text/javascript">
$(document).ready(function() {
mostrar_grafico
();

 jQuery('body').on('custom_form_ajax_complete', function () {

   mostrar_grafico();
 });
});


</script>

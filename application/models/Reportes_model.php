<?php
class Reportes_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_for_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.monitoreo')->row_array();
	}

	public function get_by_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.monitoreo')->row_array();
	}

}

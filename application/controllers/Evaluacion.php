<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluacion extends CI_Controller {

	var $schema = 'conflictos.';

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Conflictos_model', 'conflictos_model');
		$this->load->model('Evaluaciones_model', 'evaluaciones_model');

	}

	function delete_responsables($responsables_id) {

		$this->responsables_model->delete_by_id($responsables_id);

	}

	public function agregar($conflictos_id = false) {
		$data = array();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$conflicto = $this->db->where('md5(id::TEXT)', $conflictos_id, true)->get($this->schema . 'conflictos')->row_array();

		$this->form_validation->set_rules('aprendizaje', 'Aprendizaje', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['content'] = $this->load->view('evaluacion/index', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {

			/* section 1 */
			$data['aprendizaje'] = $this->input->post('aprendizaje');
			$data['fecha'] = $this->input->post('fecha');
			$data['propuesta'] = $this->input->post('propuesta');
			$data['responsable'] = $this->input->post('responsable');

			$data['conflictos_id'] = $conflicto['id'];
			$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

			//echo "<pre>";
			//print_r($data);
			$this->db->insert($this->schema . 'evaluaciones', $data);

			$evaluacion_id = $this->db->insert_id();

			redirect(site_url('evaluacion/finalizado/' . md5($conflictos_id) . '/' . md5($evaluacion_id)));
		}

	}

	public function evaluacion_editar($evaluaciones_id) {

		$data = array();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$evaluaciones = $row = $this->evaluaciones_model->get_by_id($evaluaciones_id);

		$conflicto = $this->conflictos_model->get_for_id(md5($evaluaciones['conflictos_id']));

		$data['row'] = $row;
		$this->form_validation->set_rules('aprendizaje', 'Aprendizaje', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['content'] = $this->load->view('evaluacion/editar_index', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {

			$data = array();
			/* section 1 */
			$data['aprendizaje'] = $this->input->post('aprendizaje');
			$data['fecha'] = $this->input->post('fecha');
			$data['propuesta'] = $this->input->post('propuesta');
			$data['responsable'] = $this->input->post('responsable');

			$data['conflictos_id'] = $conflicto['id'];
			//$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

			//echo "<pre>";
			//print_r($data);
			$w = array();
			$w['id'] = $evaluaciones['id'];
			$this->db->where($w)->update($this->schema . 'evaluaciones', $data);

			$evaluaciones_id = $evaluaciones['id'];

			redirect(site_url('evaluacion/finalizado_editado/' . md5($conflicto['id']) . '/' . md5($evaluaciones_id)));
		}

	}

	function evaluacion_ver($evaluaciones_id) {
		$row = $this->evaluaciones_model->get_by_id($evaluaciones_id);

		$data['conflicto'] = $this->conflictos_model->get_by_id(md5($row['conflictos_id']));
		$data['row'] = $row;
		$data['content'] = $this->load->view('evaluacion/evaluacion_ver', $data, true);

		$this->load->view('template', $data, FALSE);
	}

	public function listar_editar() {
		$data = array();
		$data['content'] = $this->load->view('evaluacion/listar_editar', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	public function listar($conflictos_id) {
		$data = array();
		$data['conflictos_id'] = $conflictos_id;
		$data['conflicto'] = $this->conflictos_model->get_for_id($conflictos_id);
		$data['content'] = $this->load->view('evaluacion/listar', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function finalizado_editado($conflictos_id, $monitoreo_id) {

		$data = array();

		$data['content'] = $this->load->view('evaluacion/finalizado_editado', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function finalizado($conflictos_id, $monitoreo_id) {

		$data = array();

		$data['content'] = $this->load->view('evaluacion/finalizado', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function listado_evaluaciones($conflictos_id) {

		$order = $this->input->get('order');
		$sort = $this->input->get('sort');
		$filter = $this->input->get('filter');

		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$order = ($sort == '') ? 'desc' : $order;
		$sort = ($sort == '') ? 'id' : $sort;

		$filter = json_decode($filter, true);
		$flag = true;
		if (count($filter) > 0):
			foreach ($filter as $k => $v) {
				if ($flag):
					$this->db->like($k, $v);
					$flag = false;
				else:
					$this->db->or_like($k, $v);
				endif;
			}
		endif;
		/*

			order:asc
			offset:10
			limit:10

		*/
		$w['md5(conflictos_id::TEXT)'] = $conflictos_id;
		$r = $this->db->select('*')->where($w)->order_by($sort, $order)->limit($limit, $offset)->get($this->schema . 'evaluaciones')->result_array();

		$total = $this->db->select('id')->where($w)->get($this->schema . 'evaluaciones')->num_rows();

		$final['total'] = $total;
		$final['rows'] = $r;
		echo json_encode($final);
	}
	function listar_para_editar() {

		$order = $this->input->get('order');
		$sort = $this->input->get('sort');
		$filter = $this->input->get('filter');

		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$order = ($sort == '') ? 'desc' : $order;
		$sort = ($sort == '') ? 'id' : $sort;

		$filter = json_decode($filter, true);
		$flag = true;
		if (count($filter) > 0):
			foreach ($filter as $k => $v) {
				if ($flag):
					$this->db->like($k, $v);
					$flag = false;
				else:
					$this->db->or_like($k, $v);
				endif;
			}
		endif;
		/*

			order:asc
			offset:10
			limit:10

		*/
		$r = $this->db->select('*')->order_by($sort, $order)->limit($limit, $offset)->get($this->schema . 'conflictos')->result_array();

		$total = $this->db->select('id')->get($this->schema . 'conflictos')->num_rows();

		$final['total'] = $total;
		$final['rows'] = $r;
		echo json_encode($final);
	}
}

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Registro del conflicto  > Datos generales</h3>
		<h4>2 de 3</h4>
	</div>
	<div class="panel-body">
		<h4>Actores involucrados</h4>
		<form action="" method="POST" class="form-horizontal" role="form">

			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Actores Primarios: </label>
				<div class="col-sm-9">
					<textarea name="" id="input" class="form-control" rows="3" required="required"></textarea>
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Actores Secundarios: </label>
				<div class="col-sm-9">
					<textarea name="" id="input" class="form-control" rows="3" required="required"></textarea>
				</div>
			</div>



			<div class="form-group">
				<label class="control-label col-sm-3" for="email">Actores Terciarios: </label>
				<div class="col-sm-9">
					<textarea name="" id="input" class="form-control" rows="3" required="required"></textarea>
				</div>
			</div>




			<div class="form-group">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-block btn-primary">Guardar y Continuar</button>
				</div>
			</div>
		</form>
	</div>
</div>
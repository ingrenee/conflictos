
 

 <script>
 var  sgpteam_id_departamento='<?php
// $conflictos_id_departamento=900;
echo (int)$conflictos_id_departamento; ?>';
 var  sgpteam_url_data='<?php
 //$conflictos_url_data='http://www.renee-local.com/proyectos/conflictos/index.php';
echo $conflictos_url_data; ?>';
 var  por_pagina='<?php
/// $conflictos_pagina=10;
echo $conflictos_pagina; ?>';
 </script>
 <?php
/*
 
drupal_add_css(drupal_get_path('module', 'conflictos') . '/css/bootstrap.min.css');
drupal_add_css(drupal_get_path('module', 'conflictos') . '/css/conflictos.css');
drupal_add_js(drupal_get_path('module', 'conflictos') . '/js/jquery-1.11.2.js');
drupal_add_js(drupal_get_path('module', 'conflictos') . '/js/handlebars-v3.0.3.js');
drupal_add_js(drupal_get_path('module', 'conflictos') . '/js/sgpteam.js');
 */
?>

<link rel="stylesheet" href="sites/all/modules/conflictos/css/bootstrap.min.css">
<link rel="stylesheet" href="sites/all/modules/conflictos/css/sgpteam.css?<?PHP echo rand(9,999999); ?>">
<script src="sites/all/modules/conflictos/js/jquery-1.11.2.js"></script>
<script src="sites/all/modules/conflictos/js/handlebars-v3.0.3.js"></script>
<script src="sites/all/modules/conflictos/js/sgpteam.js?<?PHP echo rand(9,999999); ?>"></script>

<!--
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/conflictos.css?<?PHP echo rand(9,999999); ?>">
<script src="js/jquery-1.11.2.js"></script>
<script src="js/handlebars-v3.0.3.js"></script>
<script src="js/conflictos.js?<?PHP echo rand(9,999999); ?>"></script>

-->
<?php

//echo drupal_get_path('module', 'conflictos') ;
//print_r($results);

?>
 <script type="text/x-handlebars-template" id="template-parcelas-modal">
    
  
 <div class="" id="modal-info-parcelas"></div>
<div id="modal-info-parcelas-content">
  <div class="row">
    
    <div class="col-sm-11 col-centered"  >
      <a href="javascript:void(0)" class="cerrar">[X] Cerrar</a>
      <div class="clearfix"></div>
    </div>
  </div>
   <div class="row">
     
<div class="col-sm-11 col-centered inner">
  
   <div class="row">
     
 <div class="col-sm-12 titular">
   
<h5>Ficha Resumen de Productor</h5>
 </div>
   </div>
 <div class="row">
   
<table class="table table-condensed table-bordered">
  <tr>
    <th width="65">Nombres:</th>
    <td>{{nombres}}, {{paterno}} {{materno}}</td>
  </tr>
    <tr>
    <th>Dirección:</th>
    <td>{{direccion}}</td>
  </tr>
    <tr>
    <th>Teléfonos:</th>
    <td>{{telefono}} |  {{celular}} </td>
  </tr>
    <tr>
    <th>Email:</th>
    <td>{{email}}</td>
  </tr>
  
</table>
 </div>
    
 <div class="row">
      
      <div class="col-sm-12 titular"><h5>Productos que ofrece:</h5></div>
  
    </div>
       <div class="row">
      
       
 <table class="table table-condensed table-bordered">
   <thead>
     
      <tr>
      <th>Nombre<br>producto</th>
      <th>Fecha de<br> siembra</th>
      <th>Fecha de<br> cosecha</th>
      <th>Volumen<br> (Kilos)</th>
    </tr>
   </thead>
    {{#each rows2}}
  <tr>
      <td>{{cultivo_nombre}}</td>
      <td>{{fecha_siembra}}</td>
      <td>{{fecha_cosecha}}</td>
      <td>{{rendimientos_kilos}}</td>
    </tr>
     
  {{/each}}
  
 </table>
    </div>
</div>
   </div>
</div>

</script>
 <script type="text/x-handlebars-template" id="template-tabs-parcelas">
  <ul class="nav nav-tabs">
  {{#each rows}}
  <li role="presentation"><a href="javascript:void(0)" data-periodos-id="{{periodos_id}}" data-dni="{{dni}}" data-puntos="{{puntos}}" data-parcela-id="{{id}}">{{nombre}}</a></li>
 {{/each}}
</ul>
 </script>
<script type="text/x-handlebars-template" id="template2">
<div id="datatable">
<table id="" class="table table-hover table-striped table-condensed">

<thead>
  
<tr class="ro w css_ encabezado">
<th class="col-sm- 4">  Nombre del conflicto </th>
<th class="col-sm- 2">Fecha</th>
<th class="col-sm- 2">Actor que reporta</th>
<th class="col-sm- 2">Estado</th>
<th class="col-sm- 2">Tipos</th>
</tr>
</thead>



{{#each rows}}
 
<tr class="sgp-pro ductor ro w" data-dni="{{dni}}" data-periodos="{{periodos_id}}"  data-puntos="{{puntos}}">
<td class="col-sm- 4">
{{nombre}}
</td>
<td class="col-sm- 2">
  {{fecha}}
</td>
<td class="col-sm- 2">
  {{actor_reporta_nombre}}
</td>
<td class="col-sm- 2">
  {{estado_texto}}
</td>
<td class="col-sm- 2">
 {{tipos_texto}}
 
</td>
</tr>
 
{{/each}}
</table>

</div>
<ul class="paginacion">
 
 <li class="anterior" >  Anterior </li>
 {{#paginador}}
 <li class="siguiente">  Siguiente </li>
 {{/paginador}}
</ul>

</script>
<script type="text/x-handlebars-template" id="template">
{{#each rows}}
 
 <option value="{{id}}"> {{nombre}} </option>
{{/each}}
</script>
 
<div class="sgp">
<div class="row">
   <div class="col-sm-12"> 
<div class="sgp-nav">
<div class="sgp-logo">        </div>
 
<!--
 <ul>  <li> <a href="#sgp-normativas" > Normativas </a></li>  <li> <a href="#sgp-lista-productores" class="active" id=""> Productores </a></li> </ul>
-->

</div>
   </div>
</div>
<div class="sgp-normativas sgp-tab" id="sgp-normativas" style="display:none">
<?php
if (isset($pagina) && $pagina): ?>
 <?php
    $node = (node_view($pagina)); ?>
 
 <?php
    echo $node['#node']->body['und'][0]['safe_value']; ?>
  <?php
endif; ?>
</div>
<?php
?>
<div class="sgp-lista-productores sgp-tab" id="sgp-lista-productores">
<div class="row">
 <div class="col-sm-12">
  <div class="sgp-titulo">
 Conflictos registrados 
  </div> 
  
 </div>
</div>
 <form action="" id="csl" method="post">
  <input type="hidden" name="pagina">
  <input type="hidden" name="excel" value="1">
  <input type="hidden" name="por_pagina">
  <input type="hidden" id="departamentos_id" name="departamentos_id">
 <div class="row">
  <div class="col-sm-3">
  <select class="form-control"  id="form_provincias_id"  name="provincias_id"><option> Seleccione </option></select>
</div>
 <div class="col-sm-3">
   <select class="form-control" id="form_distritos_id"  name="distritos_id"><option> Seleccione </option></select>
</div>

<div class="col-sm-3 hidden">
   <select class="form-control" id="form_comunidades_id"  name="com"><option> Seleccione </option></select>
</div> 
 <div class="col-sm-3">
  <a href="" class="btn btn-primary" id="sgp-buscar"> Buscar </a>
  </div>

   <div class="col-sm-3">
  <button id="btnExport" class="btn btn-primary" type="button"> Excel </button>
  </div>
</form>

  </div>
 
<div class="row sgp-contenido">
<div class="col-sm-12" id="sgp-listado-productores">
 
<div class="sgp-productor row">
 
</div>
 
</div>
 
</div>
</div>
</div>
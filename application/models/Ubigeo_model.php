<?php
class Ubigeo_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_departamentos() {

		$w['tipo'] = 'departamento';

		$rows = $this->db->select('departamentos_id as id, nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();

		$temp = array();

		foreach ($rows as $k => $v) {
			$temp[$v['id']] = $v['nombre'];

		}

		return $temp;

	}

	public function get_provincias() {

		$w['tipo'] = 'provincia';

		$rows = $this->db->select('provincias_id as id, nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();

		$temp = array();

		foreach ($rows as $k => $v) {
			$temp[$v['id']] = $v['nombre'];

		}

		return $temp;

	}

	public function get_distritos() {

		$w['tipo'] = 'distrito';

		$rows = $this->db->select('distritos_id as id, nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();

		$temp = array();

		foreach ($rows as $k => $v) {
			$temp[$v['id']] = $v['nombre'];

		}

		return $temp;

	}

	public function get_by_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.monitoreo')->row_array();
	}

}

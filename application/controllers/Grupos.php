<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Grupos extends CI_Controller {

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Grupos_model', 'modelo_grupos');
		// $this->load->model('Concejos_regionales_model', 'modelo_concejos_regionales');
	}
	public function index() {
		$data = array();
		$data['tab'] = 1;
		$data['menu'] = 2;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required|callback_verifica_name', array('verifica_name' => 'El grupo ya existe.'));

		$this->form_validation->set_rules('description', 'Descripción', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['name'] = $this->input->post('name');
			$p['description'] = ($this->input->post('description'));

			$temp = $this->modelo_grupos->set_grupo($p);

			// $temp = _help_get_message($temp);
			if ($temp > 0):
				$this->session->set_flashdata('mensaje', ' Se agrego a un nuevo grupo:');
			else:
				$this->session->set_flashdata('mensaje', 'Se agrego un nuevo usuario');
			endif;
			redirect(site_url('grupos/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('grupos/agregar', $data, true);
		$data['content'] = $this->load->view('grupos/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	public function estado($estado = 1, $id = false) {
		$estado = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$this->modelo_usuarios->set_usuarios_estado($estado, $id);
		redirect(site_url('usuarios/listar'));
	}
	public function editar() {
		$data = array();
		$data['tab'] = 2;
		$id = (int) $this->uri->segment(3);
		$data['id'] = $id;
		if ($id <= 0) {
			redirect(site_url('grupos/index'));
		}

		$p = array();
		$p[] = $id;
		$data['row'] = $this->modelo_grupos->get_grupo($id);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Nombre del grupo', 'required|callback_verifica_name', array('verifica_name' => 'El grupo ya  existe.'));
		$this->form_validation->set_rules('description', 'Descripción', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['id'] = $this->input->post('id');
			$p['name'] = ($this->input->post('name'));
			$p['description'] = $this->input->post('description');

			$temp = $this->modelo_grupos->update_grupos($p, $id);

			if ($temp == 1):
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente.');
			else:
				$this->session->set_flashdata('mensaje', 'No existe el grupo');
			endif;
			redirect(site_url('grupos/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('grupos/editar', $data, true);
		$data['content'] = $this->load->view('grupos/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	function verifica_name($str) {

		if ($this->modelo_grupos->get_verifica_name($str) > 0) {
			return false;
		} else {
			return true;
		}
	}

	function listar() {
		$data = array();
		$data['menu'] = 1;
		$data['tab'] = 3;

		//usp_authbear_users_list
		$data['rows'] = $this->modelo_grupos->get_list()->result_array();

		$data['subcontent'] = $this->load->view('grupos/listar', $data, true);

		$data['content'] = $this->load->view('grupos/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	function quitar_grupo() {
		$id = (int) $this->uri->segment(3);
		$users_id = (int) $this->uri->segment(4);
		$p['id'] = $id;
		$temp = $this->modelo_usuarios->deleting_users_groups($id);

		$this->session->set_flashdata('mensaje', 'El grupo quitado');
		redirect(site_url('usuarios/grupos_rel/' . $users_id));
	}

}

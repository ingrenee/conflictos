<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Estados extends CI_Controller {
	var $id;
	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Estados_model', 'estados_modelo');
		// $this->load->model('Concejos_regionales_model', 'modelo_concejos_regionales');
	}
	function change_status($id, $status) {

		if ($status == 1):
			$up['status'] = 0;
		else:
			$up['status'] = 1;
		endif;

		$this->estados_modelo->update($up, $id);

		redirect(site_url('estados/listar'), 'refresh');

	}
	public function index() {
		$data = array();
		$data['tab'] = 1;
		$data['menu'] = 2;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre', 'Nombre', 'required|callback_verifica_nombre', array('verifica_nombre' => 'El estado ya existe.'));

		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['nombre'] = $this->input->post('nombre');
			$p['descripcion'] = ($this->input->post('descripcion'));

			$temp = $this->estados_modelo->save($p);

			// $temp = _help_get_message($temp);
			if ($temp > 0):
				$this->session->set_flashdata('mensaje', ' Se agrego a un nuevo estado:');
			else:
				$this->session->set_flashdata('mensaje', 'Se agrego un nuevo estado');
			endif;
			redirect(site_url('estados/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('estados/agregar', $data, true);
		$data['content'] = $this->load->view('estados/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	public function estado($estado = 1, $id = false) {
		$estado = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$this->modelo_usuarios->set_usuarios_estado($estado, $id);
		redirect(site_url('usuarios/listar'));
	}
	public function editar($id) {
		$data = array();
		$data['tab'] = 2;
		$id = (int) $this->uri->segment(3);
		$this->id = $id;
		$data['id'] = $id;
		if ($id <= 0) {
			redirect(site_url('estados/index'));
		}

		$p = array();
		$p[] = $id;
		$data['row'] = $this->estados_modelo->get($id);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('nombre', 'Nombre', 'required|callback_verifica_nombre', array('verifica_nombre' => 'El estado ya  existe.'));
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['id'] = $this->input->post('id');
			$p['nombre'] = ($this->input->post('nombre'));
			$p['descripcion'] = $this->input->post('descripcion');

			$temp = $this->estados_modelo->update($p, $id);

			if ($temp == 1):
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente.');
			else:
				$this->session->set_flashdata('mensaje', 'No existe el grupo');
			endif;
			redirect(site_url('estados/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('estados/editar', $data, true);
		$data['content'] = $this->load->view('estados/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	function verifica_nombre($str) {

		if ($this->estados_modelo->get_verifica_nombre($str, $this->id) > 0) {
			return false;
		} else {
			return true;
		}
	}

	function listar() {
		$data = array();
		$data['menu'] = 1;
		$data['tab'] = 3;

		//usp_authbear_users_list
		$data['rows'] = $this->estados_modelo->get_list()->result_array();

		$data['subcontent'] = $this->load->view('estados/listar', $data, true);

		$data['content'] = $this->load->view('estados/grupos-index', $data, true);
		$this->load->view('template', $data);
	}

	function quitar_grupo() {
		$id = (int) $this->uri->segment(3);
		$users_id = (int) $this->uri->segment(4);
		$p['id'] = $id;
		$temp = $this->modelo_usuarios->deleting_users_groups($id);

		$this->session->set_flashdata('mensaje', 'El grupo quitado');
		redirect(site_url('usuarios/grupos_rel/' . $users_id));
	}

}

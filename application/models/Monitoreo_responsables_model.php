<?php
class Monitoreo_responsables_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_for_id($id) {

		$w['md5(id::TEXT)'] = $id;

		return $this->db->select('*')->where($w)->get('conflictos.monitoreo_responsables')->row_array();
	}

	public function delete_by_id($id) {
		$w['md5(id::TEXT)'] = $id;

		$this->db->where($w)->limit(1)->delete('conflictos.monitoreo_responsables');

	}
	public function get_all_parent($monitoreo_id, $md5 = true) {
		if ($md5):
			$w['md5(monitoreo_id::TEXT)'] = $monitoreo_id;
		else:
			$w['monitoreo_id'] = $monitoreo_id;
		endif;
		return $this->db->select('*')->where($w)->get('conflictos.monitoreo_responsables')->result_array();

	}

}

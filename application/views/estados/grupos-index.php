<?php
if (!isset($menu)):
	$menu = -1;
endif;
?>


<h1 class="titular"> Estados  </h1>


<div class="btn-group">
	<a href="<?php echo site_url('estados/listar'); ?>" type="button" class=" <?php echo _helper_active($menu, 1, 'active'); ?> btn btn-sm btn-default">
	<?php echo _helper_icono('list') ?> Listar estados</a>
	<a href="<?php echo site_url('estados'); ?>" type="button" class=" <?php echo _helper_active($menu, 2, 'active'); ?> btn btn-sm btn-default">
 <?php echo _helper_icono('plus') ?>
	Agregar estados</a>

</div>

<hr>

<?php echo $subcontent; ?>


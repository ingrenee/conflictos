<?php

$file = "demo.xls";

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");

?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Conflictos por tipo</h3>
	</div>
	<div class="panel-body">
		<table border=1 class="table table-bordered">
			<thead>
				<tr>
				<th>#</th><th>Tipo</th>
				<th>Nombre del conflicto</th>
				<th>Actor reporta Nombre</th>
				<th>Actor reporta DNI</th>
				<th>Actor reporta tipo</th>
				<th>Actor Reporta Observacion</th>
				<th>Código</th>
				<th>Fecha</th>
				<th>Departamento</th>
				<th>Provincia</th>
				<th>Distrito</th>
				<th>Comunidad</th>
			</tr>
		</thead>
		<?php foreach ($rows as $k => $v): ?>
		<tr>
		<td><?php echo $k + 1; ?></td>
		<td><?php echo utf8_decode($tipo['nombre']); ?></td>
		<td><?php echo $v['nombre']; ?></td>
		<td><?php echo $v['actor_reporta_nombre']; ?></td>
		<td><?php echo $v['actor_reporta_dni']; ?></td>
		<td><?php echo $v['actor_reporta_tipo']; ?></td>
		<td><?php echo $v['actor_reporta_observacion']; ?></td>
		<td><?php echo $v['codigo']; ?></td>
		<td><?php echo $v['fecha']; ?></td>

		<td><?php echo @$departamentos[$v['departamento_cod']]; ?></td>
		<td><?php echo @$provincias[$v['provincia_cod']]; ?></td>
		<td><?php echo @$distritos[$v['distrito_cod']]; ?></td>
		<td><?php echo $v['comunidad']; ?></td>

	</tr>
	<?php endforeach?>
</table>
</div>
</div>
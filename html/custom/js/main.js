/**/
jQuery(function() {
    var ubigeo = {


        departamento: function(origin, target) {
            jQuery(origin).change(function(e) {
                e.preventDefault();
                id = jQuery(origin).val();
                $.ajax({
                        url: _helper_site_url('ubigeo/provincias'),
                        type: 'post',
                        dataType: 'html',
                        data: {
                            id: id
                        },
                    })
                    .done(function(data) {

                        jQuery(target).html(data);
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
            });
        },
        provincia: function(origin, target) {
            jQuery(origin).change(function(e) {
                e.preventDefault();
                id = jQuery(origin).val();
                $.ajax({
                        url: _helper_site_url('ubigeo/distritos'),
                        type: 'post',
                        dataType: 'html',
                        data: {
                            id: id
                        },
                    })
                    .done(function(data) {

                        jQuery(target).html(data);
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
            });
        }
    }
    ubigeo.departamento('#departamento', '#provincia');
    ubigeo.provincia('#provincia', '#distrito');



    var form_ajax = {
        init: function() {


        },
        init_ajax: function(clase) {

            jQuery(clase).find('button[type=submit]').click(function(e) {
                e.preventDefault();

                target = jQuery(clase).attr('data-target');
                action = jQuery(clase).attr('action');

                var data = $(clase).serialize();
                $.ajax({
                    url: action,
                    type: 'post',
                    data: data,
                    success: function(data) {
                        jQuery(target).html(data);
                        jQuery('body').trigger('custom_form_ajax_complete');
                    }
                });

            });

        }
    };

    form_ajax.init_ajax('#form_ajax');


    var clon = {


        clonar: function(origin, target) {
            c = jQuery(origin).clone();

            len = jQuery(target).find(origin).length;

            jQuery(c).find('input').val('');
            jQuery(c).find('select').val('');
            jQuery(c).find('textarea').val('');

            jQuery(target).append(c);

        },
        delete_object: function(origin, target) {
            len = jQuery(target).find(origin).length;
            if (len > 1) {
                jQuery(target).find(origin).eq(len - 1).remove();
            }

        }



    };


    jQuery('#boton_clonar_responsables').click(function(e) {
        e.preventDefault();


        clon.clonar('#otros_responsables', '#otros_responsables_content');

    });

    jQuery('#boton_clonar').click(function(e) {
        e.preventDefault();


        clon.clonar('#otras_fuentes', '#otras_fuentes_content');

    });

    jQuery('#boton_clonar_hechos').click(function(e) {
        e.preventDefault();


        clon.clonar('#hechos_clonar', '#hechos_clonar_content');

    });

    jQuery('#boton_clonar_primario').click(function(e) {
        e.preventDefault();


        clon.clonar('#primario_clon', '#primario_content');

    });


    jQuery('body').on('click', '.boton_quitar', function(e) {
        e.preventDefault();
        len = jQuery('.otras_fuentes').length;
        if (len > 1) {
            jQuery(this).closest('.otras_fuentes').remove();
        }
    });


    jQuery('body').on('click', '.boton_quitar_responsables', function(e) {
        e.preventDefault();
        len = jQuery('.otros_responsables').length;
        if (len > 1) {
            jQuery(this).closest('.otros_responsables').remove();
        }
    });

    jQuery('body').on('click', '.boton_quitar_hechos', function(e) {
        e.preventDefault();
        len = jQuery('.hechos_clon').length;
        if (len > 1) {
            jQuery(this).closest('.hechos_clon').remove();
        }
    });

    jQuery('body').on('click', '.boton_quitar_primario', function(e) {
        e.preventDefault();
        len = jQuery('.primario_clon').length;

        if (len > 1) {
            jQuery(this).closest('.primario_clon').remove();
        }
    });


    jQuery('body').on('focus', ".datepicker_recurring_start", function() {
        $(this).datepicker({
            format: "dd/mm/yyyy"
        });
    })









    jQuery('body').on('click', '.boton_delete_responsables', function(e) {
        e.preventDefault();
        len = jQuery('.otros_responsables').length;
        if (len > 1) {

            if (confirm("Desea eliminar este registro?")) {

                id = jQuery(this).attr('data-id');
                $.ajax({
                        url: _helper_site_url('monitoreo/delete_responsables/' + $.md5(id)),
                        type: 'post',
                        dataType: 'json',
                        data: {
                            id: id
                        },
                    })
                    .done(function(data) {



                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });

                jQuery(this).closest('.otros_responsables').remove();
            }



        }
    });




    jQuery('body').on('click', '.boton_delete', function(e) {
        e.preventDefault();
        len = jQuery('.otras_fuentes').length;
        if (len > 1) {

            if (confirm("Desea eliminar esta fuente?")) {

                id = jQuery(this).attr('data-id');
                $.ajax({
                        url: _helper_site_url('conflictos/delete/' + id),
                        type: 'post',
                        dataType: 'json',
                        data: {
                            id: id
                        },
                    })
                    .done(function(data) {



                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });

                jQuery(this).closest('.otras_fuentes').remove();
            }



        }
    });




    jQuery('body').on('click', '.boton_delete_hechos', function(e) {
        e.preventDefault();
        len = jQuery('.hechos_clon').length;
        if (len > 1) {

            if (confirm("Desea eliminar este hecho?")) {

                id = jQuery(this).attr('data-id');
                $.ajax({
                        url: _helper_site_url('conflictos/delete_hecho/' + id),
                        type: 'post',
                        dataType: 'json',
                        data: {
                            id: id
                        },
                    })
                    .done(function(data) {



                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });

                jQuery(this).closest('.hechos_clon').remove();
            }



        }
    });





    jQuery('body').on('click', '.boton_delete_primario', function(e) {
        e.preventDefault();
        len = jQuery('.primario_clon').length;
        if (len > 1) {

            if (confirm("Desea eliminar este registro?")) {

                id = jQuery(this).attr('data-id');
                $.ajax({
                        url: _helper_site_url('conflictos/delete_primario/' + id),
                        type: 'post',
                        dataType: 'json',
                        data: {
                            id: id
                        },
                    })
                    .done(function(data) {



                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });

                jQuery(this).closest('.primario_clon').remove();
            }



        }
    });




});

function _helper_site_url(path) {
    return _site_url + '/' + path;
}

    function mostrar_grafico() {
        $('table.highchart').highchartTable({


            yaxis: [{
                min: 0,
                max: 6,
                tickInterval: 1,

                lineColor: '#FF0000',
                lineWidth: 1,
            }]

        });
    }
function _helper_link(text, href, icon, options) {
    var options = typeof options !== 'undefined' ? options : false;
    var str = '';
    if (!options) {
        $.each(options, function(i, item) {
            str += i + '=' + item + '   ';
        });
    }
    link = " <a href='" + href + "' " + str + " > <span class=\"glyphicon glyphicon-" + icon + "\" aria-hidden=\"true\"></span> " + text + " </a>"
    link = '<div style="" class="custom-action">' + link + '</div>';
    return link;
}

var main = {

    init: function() {
        _self_ = this;

        _self_.init_sumatorias();

    },
    init_sumatorias: function() {


        jQuery('.js_sumar').each(function(index, el) {


            target = jQuery(this).attr('data-target');
            suma = 0;
            jQuery('.' + target).each(function(index2, el2) {

                suma = suma + parseFloat(jQuery(this).text());
            });


            jQuery(this).text(suma);

        });
    }
};

jQuery(function() {
    main.init();

    jQuery('body').on('custom_form_ajax_complete', function () {
        main.init_sumatorias();
    });

});
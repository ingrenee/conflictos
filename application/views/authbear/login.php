<div class="section">
  <div class="row">
    <div class="col-sm-12 text-center">
      <h2> MODULO DE CONFLICTOS – SIAL Villa Rica </h2>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4> Ingresar al sistema</h4>
        </div>
        <div class="panel-body">
          <form action="" method="POST" class="form-horizontal" role="form">
            <div class="form-group">
              <label for="ejemplo_email_3" class="col-lg-2 control-label">Email</label>
              <div class="col-lg-10">
                <input type="email" name="username" class="form-control" id="ejemplo_email_3"
                placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label for="ejemplo_password_3" class="col-lg-2 control-label">Contraseña</label>
              <div class="col-lg-10">
                <input type="password" name="password" class="form-control" id="ejemplo_password_3"
                placeholder="Contraseña">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-10 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">Ingresar</button>
              </div>
            </div>
            <div style="margin-bottom: 25px" class="input-group error-login">
              <?PHP echo $_SESSION['error_login']; ?>
              <?php echo form_error('username'); ?>
              <?php echo form_error('password'); ?>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
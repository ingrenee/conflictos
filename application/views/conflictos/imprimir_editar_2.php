<h4>Analisis del conflicto/denuncia</h4>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Codigo:</label>
	<div class="col-sm-4">
		<p><?php _vi($row, 'codigo');?></p>
	</div>
	<label class="control-label col-sm-2" for="email">Estado:</label>
	<div class="col-sm-3">
	<p>
			<?php foreach ($estados as $key => $value):
	if ($row['estado'] == $value['id']):
		echo $value['nombre'];
	endif;

endforeach;?>
	</p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Nombre </label>
	<div class="col-sm-9">
		<p><?php _vi($row, 'nombre');?></p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Departamento:</label>
	<div class="col-sm-3">
		<?php foreach ($departamentos as $k => $v) {

	if ($row['departamento_cod'] == $k):
		echo $v;
	endif;

}?>
	</div>
	<label class="control-label col-sm-1 col-xs-12" for="email">Provincia:</label>
	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		<?php foreach ($provincia as $k => $v) {
	?>
		<?php
if ($row['provincia_cod'] == $k):
		echo $v;
	endif;
	?>
		<?PHP
}?>
	</div>
	<label class="control-label col-sm-1 col-xs-12" for="email">Distrito: 	</label>
	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		<?php foreach ($distrito as $k => $v) {
	?>
		<?php
if ($row['distrito_cod'] == $k):
		echo $v;
	endif;
	?>
		<?PHP
}?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Comunidad:</label>
	<div class="col-sm-4">
	 <p><?php _vi($row, 'comunidad');?></p>
	</div>
	<label class="control-label col-sm-2" for="email">Fecha:</label>
	<div class="col-sm-3">
	<p><?php
$row['fecha'] = date('d/m/Y', strtotime($row['fecha']));
_vi($row, 'fecha');?></p>
	</div>
</div>
<div class="clearfix"></div>
 <div class="form-group">
<h4>Perfil del conflicto </h4>
 </div>
  <div class="clearfix"></div>



<div class="form-group	">
	<label class="control-label col-sm-3" for="email">Antecedentes:</label>
	<div class="col-sm-9">
		 <p><?php _vi($row, 'antecedentes');?></p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Situacion Actual:</label>
	<div class="col-sm-9">
	 <p><?php _vi($row, 'situacion_actual');?></p>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-sm-3" for="email">Hechos:</label>
	<div class="col-sm-9">
		<ol id="hechos_clonar_content">
			<?php if (count($hechos) > 0): ?>
			<?php foreach ($hechos as $k => $v): ?>
			<?php
$v['hechos_fecha'] = date('d/m/Y', strtotime($v['hechos_fecha']));
?>
			<li id="hechos_clonar" class="hechos_clon">
				<div class="row">
					<input name="hechos_id[]" type="hidden" value="<?php echo $v['id']; ?>">

					<div class="col-sm-8">
						<p>(Descripción)<?php echo $v['hechos_descripcion']; ?></p>
					</div>
					<div class="col-sm-3">
					<p>(Fecha)<?php echo $v['hechos_fecha']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</li>
			<?php endforeach?>
			<?php else: ?>

			<?php endif?>
		</ol>

	</div>
</div>
<hr>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Tipo:</label>
	<div class="col-sm-9">
		<?php foreach ($tipos as $k => $v): ?>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="tipos[]" <?php
if (in_array($v['id'], $mis_tipos)):
	echo "checked";
endif;
?> value="<?php echo $v['id']; ?>">
				<?php echo $v['nombre']; ?>
			</label>
		</div>
		<?php endforeach?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Problema:</label>
	<div class="col-sm-9">
		 <p><?php _vi($row, 'problema');?></p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Poblacion afectada:</label>
	<div class="col-sm-4">
	 <p><?php _vi($row, 'poblacion_afectada');?></p>
	</div>
	<label class="control-label col-sm-2" for="email">Demanda:</label>
	<div class="col-sm-3">
	<p><?php _vi($row, 'demanda');?></p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Causas:</label>
	<div class="col-sm-9">
<p> <?php _vi($row, 'causas');?></p>
	</div>
</div>

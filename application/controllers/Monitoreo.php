<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoreo extends CI_Controller {

	var $schema = 'conflictos.';

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Conflictos_model', 'conflictos_model');
		$this->load->model('Monitoreo_model', 'monitoreo_model');
		$this->load->model('Monitoreo_responsables_model', 'responsables_model');
		$this->load->model('Monitoreo_documentos_model', 'monitoreo_documentos_model');
	}

	function delete_responsables($responsables_id) {

		$this->responsables_model->delete_by_id($responsables_id);

	}

	function monitoreo_ver($evaluaciones_id) {
		$data['conflicto'] = $this->conflictos_model->get_by_id($evaluaciones_id);
		$row = $this->monitoreo_model->get_by_id($evaluaciones_id);

		$data['row'] = $row;

		$responsables = $this->responsables_model->get_all_parent(md5($row['id']));
		$data['responsables'] = $responsables;

		$data['content'] = $this->load->view('monitoreo/monitoreo_ver', $data, true);

		$this->load->view('template', $data, FALSE);
	}

	public function agregar($conflictos_id = false) {
		$data = array();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$conflicto = $this->db->where('md5(id::TEXT)', $conflictos_id, true)->get($this->schema . 'conflictos')->row_array();

		$this->form_validation->set_rules('fase', 'Fase', 'required');

		if ($this->form_validation->run() == FALSE) {
			$tipos = $this->db->select('*')->get($this->schema . 'tipos')->result_array();
			$data['tipos'] = $tipos;

			$estados = $this->db->select('*')->get($this->schema . 'estados')->result_array();
			$data['estados'] = $estados;

			$data['content'] = $this->load->view('monitoreo/index', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {

			/* section 1 */
			$data['fase'] = $this->input->post('fase');
			$data['fecha'] = $this->input->post('fecha');
			$data['desencadenante'] = $this->input->post('desencadenante');
			$data['mecanismo_1'] = $this->input->post('mecanismo_1');
			$data['mecanismo_2'] = $this->input->post('mecanismo_2');
			$responsable = $this->input->post('responsable');
			$observaciones = $this->input->post('observaciones');
			$data['acciones'] = $this->input->post('acciones');
			$data['fecha_2'] = $this->input->post('fecha_2');
			$data['efectos'] = $this->input->post('efectos');
			$data['acuerdos'] = $this->input->post('acuerdos');
			$data['responsable_2'] = $this->input->post('responsable_2');
			$data['conflictos_id'] = $conflicto['id'];
			$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

			//echo "<pre>";
			//print_r($data);
			$this->db->insert($this->schema . 'monitoreo', $data);

			$monitoreo_id = $this->db->insert_id();

			$data = array();

			if (is_array($responsable) && count($responsable) > 0):
				foreach ($responsable as $k => $v) {

					$data['responsable'] = $v;
					$data['monitoreo_id'] = $monitoreo_id;

					$data['observaciones'] = $observaciones[$k];

					//$data['create_date'] = date('Y-m-d H:i:s');
					//$data['modify_date'] = date('Y-m-d H:i:s');
					$this->db->insert($this->schema . 'monitoreo_responsables', $data);

				}
			endif;

			redirect(site_url('monitoreo/finalizado/' . $conflictos_id . '/' . md5($monitoreo_id)));
		}

	}

	public function monitoreo_editar($monitoreo_id) {

		$data = array();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$monitoreo = $row = $this->monitoreo_model->get_for_id($monitoreo_id);

		$conflicto = $this->conflictos_model->get_for_id(md5($monitoreo['conflictos_id']));

		$data['row'] = $row;
		$this->form_validation->set_rules('fase', 'Fase', 'required');

		if ($this->form_validation->run() == FALSE) {
			$responsables = $this->responsables_model->get_all_parent($monitoreo_id);
			$data['responsables'] = $responsables;

			$data['content'] = $this->load->view('monitoreo/editar_index', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {

			$data = array();
			/* section 1 */
			$data['fase'] = $this->input->post('fase');
			$data['fecha'] = $this->input->post('fecha');
			$data['desencadenante'] = $this->input->post('desencadenante');
			$data['mecanismo_1'] = $this->input->post('mecanismo_1');
			$data['mecanismo_2'] = $this->input->post('mecanismo_2');

			$responsable = $this->input->post('responsable');
			$observaciones = $this->input->post('observaciones');
			$responsable_id = $this->input->post('responsable_id');

			$data['acciones'] = $this->input->post('acciones');
			$data['fecha_2'] = $this->input->post('fecha_2');
			$data['efectos'] = $this->input->post('efectos');
			$data['acuerdos'] = $this->input->post('acuerdos');
			$data['responsable_2'] = $this->input->post('responsable_2');
			$data['conflictos_id'] = $conflicto['id'];
			//	$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

			//echo "<pre>";
			//print_r($data);
			$w = array();
			$w['id'] = $monitoreo['id'];
			$this->db->where($w)->update($this->schema . 'monitoreo', $data);

			$monitoreo_id = $monitoreo['id'];

			$data = array();

			echo "<pre>";
			print_r($responsable);

			if (is_array($responsable) && count($responsable) > 0):
				foreach ($responsable as $k => $v) {

					$data['responsable'] = $v;
					$data['monitoreo_id'] = $monitoreo_id;

					$data['observaciones'] = $observaciones[$k];

					if (isset($responsable_id[$k])):
						$id = $responsable_id[$k];
						$this->db->where('id', $id)->update($this->schema . 'monitoreo_responsables', $data);
					else:

						$this->db->insert($this->schema . 'monitoreo_responsables', $data);
					endif;

					//$data['create_date'] = date('Y-m-d H:i:s');
					//$data['modify_date'] = date('Y-m-d H:i:s');

				}
			endif;

			redirect(site_url('monitoreo/finalizado_editado/' . md5($conflicto['id']) . '/' . md5($monitoreo_id)));
		}

	}


 public function  borrar_documento($id)
 {

  $row=$this->monitoreo_documentos_model->get_for_id($id);
  
     $file=('uploads/monitoreo/'.$row['file']);
 
  if (file_exists($file))
  {
  	  
  	  unlink($file);
 
  	 
 
  }
 
		  $this->monitoreo_documentos_model->delete_by_id($id);

		  	redirect(site_url('monitoreo/agregar_documentos/' . md5($row['monitoreo_id'])));// . '/' . md5($monitoreo_id)));
 }
	public function agregar_documentos($monitoreo_id) {

		$data = array();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$monitoreo = $row = $this->monitoreo_model->get_for_id($monitoreo_id);

		$conflicto = $this->conflictos_model->get_for_id(md5($monitoreo['conflictos_id']));

		$data['row'] = $row;

 

	

		if (empty($_FILES['file']['name'])) {
			$documentos = $this->monitoreo_documentos_model->get_all_parent($monitoreo_id);
			$data['documentos'] = $documentos;

			$data['content'] = $this->load->view('monitoreo/agregar_documentos', $data, true);

			$this->load->view('template', $data, FALSE);
		} else {
 
			$data = array();
			/* section 1 */
			$data['fase'] = $this->input->post('fase');
		
			$data['conflictos_id'] = $conflicto['id'];
			//	$data['create_date'] = date('Y-m-d H:i:s');
			$data['modify_date'] = date('Y-m-d H:i:s');

 $filesu=$this->subir();

 
			//echo "<pre>";
			//print_r($data);
			$w = array();
			$w['id'] = $monitoreo['id'];

			 foreach ($filesu as $k => $v) {
			 	 $in=array();
			 	$in['monitoreo_id']= $w['id'];
			 	$in['file']=$v;

			 	$this->db->insert($this->schema . 'monitoreo_documentos', $in);
			 	# code...
			 }
			

			$monitoreo_id = $monitoreo['id'];

		

		

		

			redirect(site_url('monitoreo/agregar_documentos/' .  md5($monitoreo_id)));
		}

	}

		function subir() {
		$imagenes = array();

		if (isset($_FILES['file']) && count($_FILES['file']) > 0 && (isset($_FILES['file']['name'][0]))):
			for ($i = 0; $i < count($_FILES['file']['name']); $i++):
				$target_path = "uploads/monitoreo/";
				$ext = explode('.', basename($_FILES['file']['name'][$i]));
				$file_name = md5(uniqid()) . '_' . basename($_FILES['file']['name'][$i]) . "." . $ext[count($ext) - 1];
				$target_path = $target_path . $file_name;

				if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path)) {

					//$in = array();

					$imagenes[$i] = $file_name;

					//$files_attach[] = base_url($target_path);

				} else {

				}
			endfor;
		endif;

		return $imagenes;

	}

	public function listar_editar() {
		$data = array();
		$data['content'] = $this->load->view('monitoreo/listar_editar', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	public function listar($conflictos_id) {
		$data = array();
		$data['conflictos_id'] = $conflictos_id;
		$data['conflicto'] = $this->conflictos_model->get_for_id($conflictos_id);
		$data['content'] = $this->load->view('monitoreo/listar', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function finalizado_editado($conflictos_id, $monitoreo_id) {

		$data = array();

		$data['content'] = $this->load->view('monitoreo/finalizado_editado', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function finalizado($conflictos_id, $monitoreo_id) {

		$data = array();

		$data['content'] = $this->load->view('monitoreo/finalizado', $data, true);

		$this->load->view('template', $data, FALSE);
	}
	function listado_monitoreo($conflictos_id) {

		$order = $this->input->get('order');
		$sort = $this->input->get('sort');
		$filter = $this->input->get('filter');

		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$order = ($sort == '') ? 'desc' : $order;
		$sort = ($sort == '') ? 'id' : $sort;

		$filter = json_decode($filter, true);
		$flag = true;
		if (count($filter) > 0):
			foreach ($filter as $k => $v) {
				if ($flag):
					$this->db->like($k, $v);
					$flag = false;
				else:
					$this->db->or_like($k, $v);
				endif;
			}
		endif;
		/*

			order:asc
			offset:10
			limit:10

		*/
		$w['md5(conflictos_id::TEXT)'] = $conflictos_id;
		$r = $this->db->select('*')->where($w)->order_by($sort, $order)->limit($limit, $offset)->get($this->schema . 'monitoreo')->result_array();

		$total = $this->db->select('id')->where($w)->get($this->schema . 'monitoreo')->num_rows();

		$final['total'] = $total;
		$final['rows'] = $r;
		echo json_encode($final);
	}
	function listar_para_editar() {

		$order = $this->input->get('order');
		$sort = $this->input->get('sort');
		$filter = $this->input->get('filter');

		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$order = ($sort == '') ? 'desc' : $order;
		$sort = ($sort == '') ? 'id' : $sort;

		$filter = json_decode($filter, true);
		$flag = true;
		if (count($filter) > 0):
			foreach ($filter as $k => $v) {
				if ($flag):
					$this->db->like($k, $v);
					$flag = false;
				else:
					$this->db->or_like($k, $v);
				endif;
			}
		endif;
		/*

			order:asc
			offset:10
			limit:10

		*/
		$r = $this->db->select('*')->order_by($sort, $order)->limit($limit, $offset)->get($this->schema . 'conflictos')->result_array();

		$total = $this->db->select('id')->get($this->schema . 'conflictos')->num_rows();

		$final['total'] = $total;
		$final['rows'] = $r;
		echo json_encode($final);
	}
}

var _site_url_ajax_ubigeo = sgpteam_url_data + '/fichas/json_ubigeo';
//var _site_url_ajax_productores = sgpteam_url_data + '/fichas/json_productores';
var _site_url_ajax_productores = sgpteam_url_data + '/fichas/json_conflictos';
var _site_url_iframe = sgpteam_url_data + "/m/s_index?";
var _url_tabs_parcelas = sgpteam_url_data + '/fichas/json_productor_parcelas';
var _url_modal_productor_parcela = sgpteam_url_data + '/fichas/json_productor_parcelas_info';
var departamentos_id = sgpteam_id_departamento;
var pagina = 1;

function modal_init() {
    jQuery('#modal-parcelas').fadeOut('300', function() {});
}

function loadIframe(t) {
    jQuery('#sgp-iframe').attr('src', _site_url_iframe + 'points=' + t);
}

function mas_informacion() {
    jQuery('.sgp .mas-informacion').click(function(event) {
        /* obtenemos datos de la parcela seleccionada actualmente. */
        var obj = jQuery('.sgp #tabs-content ul li.active a');
        console.log(obj);
        var dni = obj.attr('data-dni');
        var parcelas_id = obj.attr('data-parcela-id');
        var periodos_id = obj.attr('data-periodos-id');
        if (typeof dni === 'undefined') {
            alert("El productor no ha registrado parcelas.");
            return 0;
        }
        console.log("dni" + dni);
        console.log("parcelas_id" + parcelas_id);
        console.log("periodos_id=" + periodos_id);
        jQuery.ajax({
            url: _url_modal_productor_parcela,
            type: 'GET',
            dataType: 'jsonp',
            data: {
                dni: dni,
                parcelas_id: parcelas_id,
                periodos_id: periodos_id
            },
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template-parcelas-modal").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx = data;
            html = tmpl(ctx);
            console.log(data);
            console.log(html);
            jQuery('.sgp #modal-parcelas').html(html);
            jQuery('#modal-parcelas').fadeIn(200, function() {});
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    });
}

function loadParcelas(dni) {
    $.ajax({
        url: _url_tabs_parcelas,
        type: 'GET',
        dataType: 'jsonp',
        data: {
            dni: dni
        },
    }).done(function(data, textStatus, jqXHR) {
        var stemplate = jQuery("#template-tabs-parcelas").html();
        var tmpl = Handlebars.compile(stemplate);
        var ctx = {};
        ctx.rows = data;
        html = tmpl(ctx);
        console.log(data);
        console.log(html);
        jQuery('.sgp #tabs #tabs-content').html(html);
        jQuery('.sgp #tabs #tabs-content').find('li:first').addClass('active');
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}
jQuery(function() {
    if (typeof jQuery.easing[jQuery.easing.def] == 'function') {
        // return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    }
    modal_init();
    mas_informacion();
    /****************************************************************************/
    sgpteam_nav();
    /****************************************************************************/
    /* Cada ves que  se hace click sobre un productor*/
    sgpteam_productor();
    /****************************************************************************/
    /* Cada vez que se hace click sobre  una parcela */
    sgpteam_parcela();
    /****************************************************************************/
    jQuery('body').on('click', '#modal-info-parcelas-content .cerrar', function() {
        jQuery('#modal-parcelas').fadeOut('300', function() {});
    });
    sgpteam_provincias_cambiar();
    sgpteam_distritos_cambiar();
    sgpteam_buscar();
    sgpteam_ubigeo_load_init();
    sgpteam_productores_load_init();
    sgpteam_paginacion();
    jQuery('#sgp-iframe').attr('src', _site_url_iframe);
});

function sgpteam_ubigeo_load_init() {
    jQuery.ajax({
        data: {
            tipo: 2,
            a: departamentos_id,
            b: 0,
            c: 0
        },
        type: "post",
        dataType: "jsonp",
        url: _site_url_ajax_ubigeo,
    }).done(function(data, textStatus, jqXHR) {
        var stemplate = jQuery("#template").html();
        var tmpl = Handlebars.compile(stemplate);
        var ctx = {};
        ctx.rows = data;
        html = tmpl(ctx);
        jQuery("#form_provincias_id").html(html);
        jQuery("#form_distritos_id").html('<option value=""> Seleccione </option>');
        if (console && console.log) {
            console.log("La solicitud se ha completado correctamente.");
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}
/*
 
 */
function sgpteam_productores_load_init() {
    jQuery("#sgp-listado-productores").html('cargando...');
    jQuery("#sgp-buscar").trigger('click');
    /*
        jQuery.ajax({
            data: {
                departamentos_id: departamentos_id,
                com: ''
            },
            type: "post",
            dataType: "jsonp",
            url: _site_url_ajax_productores,
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template2").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            html = tmpl(ctx);
            jQuery("#sgp-listado-productores").html(html);
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });

        */
}

function sgpteam_parcela() {
    jQuery('body').on('click', '#tabs-content li a', function() {
        jQuery(this).closest('ul').find('li').removeClass('active');
        jQuery(this).closest('li').addClass('active');
        t = jQuery(this).attr('data-puntos');
        jQuery('#sgp-iframe').attr('src', _site_url_iframe + 'points=' + t);
    });
}

function sgpteam_productor() {
    jQuery('body').on('click', '.sgp-productor', function() {
        jQuery('.sgp-productor').removeClass('sgp-active')
        jQuery(this).toggleClass('sgp-active');
        t = jQuery(this).attr('data-puntos');
        dni = jQuery(this).attr('data-dni');
        jQuery('#sgp-iframe').attr('src', _site_url_iframe + 'points=' + t);
        loadParcelas(dni);
    });
}

function sgpteam_nav() {
    jQuery('.sgp-nav ul li a').click(function(event) {
        event.preventDefault();
        jQuery('.sgp-nav ul li a').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.sgp-tab').hide();
        jQuery(jQuery(this).attr('href')).show();
    });
}

function sgpteam_provincias_cambiar() {
    jQuery('#form_provincias_id').change(function(event) {
        _valor_departamento_id = departamentos_id;
        _valor = jQuery(this).val();
        jQuery("#form_distritos_id").html('<option>Cargando...</option>');
        jQuery.ajax({
            data: {
                tipo: 3,
                a: _valor_departamento_id,
                b: _valor,
                c: 0
            },
            type: "post",
            dataType: "jsonp",
            url: _site_url_ajax_ubigeo,
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            html = tmpl(ctx);
            jQuery("#form_distritos_id").html(html);
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
        /*
         *************************************************************************************
         */
    });
}

function sgpteam_distritos_cambiar() {
    jQuery('#form_distritos_id').change(function(event) {
        _valor_departamento_id = departamentos_id;
        _valor_provincia_id = jQuery('#form_provincias_id').val();
        _valor_distrito_id = jQuery(this).val();
        jQuery("#form_comunidades_id").html('<option>Cargando...</option>');
        jQuery.ajax({
            data: {
                tipo: 4,
                a: _valor_departamento_id,
                b: _valor_provincia_id,
                c: _valor_distrito_id
            },
            type: "post",
            dataType: "jsonp",
            url: _site_url_ajax_ubigeo,
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            html = tmpl(ctx);
            jQuery("#form_comunidades_id").html(html);
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
        /*
         *************************************************************************************
         */
    });
}

function sgpteam_buscar() {
    /*
     *************************************************************************************
     */
    jQuery('#sgp-buscar').click(function(event) {
        event.preventDefault();
        _dep = departamentos_id;
        _pro = jQuery('#form_provincias_id').val();
        _dis = jQuery('#form_distritos_id').val();
        _com = jQuery("#form_comunidades_id").val();
        _pagina = pagina;
        _por_pagina = por_pagina;
        /*
         jQuery.post(_site_url_ajax_productores, { com:_com}, function(data){
                    jQuery("#sgp-listado-productores").html(data);
                  });     
        */
        /*
         *************************************************************************************
         */
        jQuery.ajax({
            data: {
                departamentos_id: _dep,
                provincias_id: _pro,
                distritos_id: _dis,
                com: _com,
                pagina: _pagina,
                por_pagina: _por_pagina
            },
            type: "post",
            dataType: "jsonp",
            url: _site_url_ajax_productores,
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template2").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            if (data.length > 0) {
                ctx.paginador_pagina = pagina;
                ctx.paginador = true;
            } else {
                ctx.paginador_pagina = pagina;
                ctx.paginador = false;
            }
            html = tmpl(ctx);
            if (html.length > 4) {
                jQuery("#sgp-listado-productores").html(html);
            } else {
                jQuery("#sgp-listado-productores").html('<p> No hay productores  registrados en esta comunidad </p>');
            }
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
        /*
         *************************************************************************************
         */
    });
}

function sgpteam_paginacion() {
    jQuery('body').on('click', '.paginacion .siguiente', function() {
        pagina++;
        jQuery('#sgp-buscar').trigger('click');
    });
    jQuery('body').on('click', '.paginacion .anterior', function() {
        pagina--;
        if (pagina <= 0) {
            pagina = 0;
        }
        jQuery('#sgp-buscar').trigger('click');
    });
}

<div class="form-group">
	<label for="input-id" class="col-sm-3 control-label">
		Acciones GT
	</label>
	<div class="col-sm-3">
		<select name="acciones" id="acciones" class="control-form">
			<option value="no-acciones">0: no acciones /intervenciones</option>
			<option value="tomado-conocimiento">1: Tomado conocimiento</option>
			<option value="revisado">2: Revisado</option>
			<option value="denuncia-hecho">3: Denuncia hecho</option>
			<option value="acciones-frecuentes">4: Acciones frecuentes</option>
		</select>
	</div>
	<div class="form-group">
		<label for="input" class="col-sm-3 control-label">Fecha:</label>
		<div class="col-sm-2">
			<input type="text" name="fecha_2" id="fecha_2" class="form-control fecha" value="<?php
$row['fecha_2'] = date('d/m/Y', strtotime($row['fecha_2']));
_vi($row, 'fecha_2');?>" required="required" pattern="" title="">
		</div>
	</div>
</div>


<div class="form-group">
	 <label for="" class="control-label col-sm-3">Efecto de acciones</label>
	  <div class="col-sm-9"><input type="text" name="efectos" id="efectos" class="form-control" value="" required="required" pattern="" title=""></div>
</div>

<div class="form-group">
	 <label for="" class="control-label col-sm-3">Acuerdos</label>
	  <div class="col-sm-9"><input type="text" name="acuerdos" id="acuerdos" class="form-control" value="" required="required" pattern="" title=""></div>
</div>

<div class="form-group">
	 <label for="" class="control-label col-sm-3">Responsable</label>
	  <div class="col-sm-9"><input type="text" name="responsable_2"
	  id="responsable_2" class="form-control" value="" required="required" pattern="" title=""></div>
</div>


<div class="form-group">
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-default" id="boton_form_2_back">Volver</button>
	</div>
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_3">Guardar</button>
	</div>
</div>

<script type="text/javascript">
jQuery(function(){
jQuery('#acciones').val('<?php echo $row['acciones']; ?>');
jQuery('#efectos').val('<?php echo $row['efectos']; ?>');
jQuery('#acuerdos').val('<?php echo $row['acuerdos']; ?>');
jQuery('#responsable_2').val('<?php echo $row['responsable_2']; ?>');
});
</script>
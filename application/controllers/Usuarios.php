<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Usuarios extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *      http://example.com/index.php/welcome
	 *  - or -
	 *      http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();

		$this->load->model('Usuarios_model', 'modelo_usuarios');
		//	$this->load->model('Groups_model', 'modelo_groups');
		// $this->load->model('Concejos_regionales_model', 'modelo_concejos_regionales');
	}
	public function index() {
		$data = array();
		$data['tab'] = 1;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('dni', 'DNI', 'required|callback_verifica_dni', array('verifica_dni' => 'El dni ya es usado por otro usuario'));

		$this->form_validation->set_rules('email', 'Email', 'required|callback_verifica_email', array('verifica_email' => 'El email ya es usado por otro usuario'));

		$this->form_validation->set_rules('nombres', 'nombres', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('active', 'Estado del  usuario', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['username'] = $this->input->post('username');
			$p['password'] = md5($this->input->post('password'));
			$p['active'] = ($this->input->post('active'));
			$p['cargo'] = $this->input->post('cargo');
			$p['institucion'] = $this->input->post('institucion');
			$p['nombres'] = ($this->input->post('nombres'));
			$p['email'] = ($this->input->post('email'));
			$p['dni'] = $p['username'] = ($this->input->post('dni'));

			$temp = $this->modelo_usuarios->insert_usuarios($p);

			// $temp = _help_get_message($temp);
			if ($temp > 1):
				$this->session->set_flashdata('mensaje', ' Se agrego a un nuevo usuario:' . $email);
			else:
				$this->session->set_flashdata('mensaje', 'Se agrego un nuevo usuario');
			endif;
			redirect(site_url('usuarios/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('usuarios/agregar', $data, true);
		$data['content'] = $this->load->view('usuarios/usuarios-index', $data, true);
		$this->load->view('template', $data);
	}

	public function estado($estado = 1, $id = false) {
		$estado = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$this->modelo_usuarios->set_usuarios_estado($estado, $id);
		redirect(site_url('usuarios/listar'));
	}
	public function editar() {
		$data = array();
		$data['tab'] = 2;
		$id = (int) $this->uri->segment(3);
		$data['id'] = $id;
		if ($id <= 0) {
			redirect(site_url('usuarios/index'));
		}

		$p = array();
		$p[] = $id;
		$data['usuario'] = $this->modelo_usuarios->get_usuarios_por_id($id);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'Nombre de usuario', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('dni', 'DNI', 'required|callback_verifica_dni', array('verifica_dni' => 'El dni ya es usado por otro usuario'));
		$this->form_validation->set_rules('nombres', 'nombres', 'required');
		// $this->form_validation->set_rules('username', 'Nombre de usuario', 'required|callback_verifica_username', array('verifica_username' => 'El nombre de usuario ya esta siendo usado por otro usuario.'));

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p['id'] = $this->input->post('id');
			$p['password'] = md5($this->input->post('password'));
			$p['nombres'] = $this->input->post('nombres');
			$p['dni'] = $this->input->post('dni');

			$p['cargo'] = $this->input->post('cargo');
			$p['institucion'] = $this->input->post('institucion');

			$p['username'] = $p['dni']; //$this->input->post('username');
			$temp = $this->modelo_usuarios->update_usuarios($p, $id);

			if ($temp == 1):
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente.');
			else:
				$this->session->set_flashdata('mensaje', 'No existe el usuario');
			endif;
			redirect(site_url('usuarios/listar'));
		}

		//$data['groups_id']=$this->groups_list();
		$data['subcontent'] = $this->load->view('usuarios/editar', $data, true);
		$data['content'] = $this->load->view('usuarios/usuarios-index', $data, true);
		$this->load->view('template', $data);
	}

	function verifica_dni($str) {

		$id = $this->uri->segment(3);
		if ($this->modelo_usuarios->get_verifica_dni($str, $id) > 0) {
			return false;
		} else {
			return true;
		}
	}

	function verifica_email($str) {

		$id = $this->uri->segment(3);
		if ($this->modelo_usuarios->get_verifica_email($str, $id) > 0) {
			return false;
		} else {
			return true;
		}
	}

	function verifica_username($str) {

		$id = $this->uri->segment(3);
		if ($this->modelo_usuarios->get_verifica_username($str, $id) > 0) {
			return false;
		} else {
			return true;
		}
	}
	function listar() {
		$data = array();
		$data['tab'] = 3;

		//usp_authbear_users_list
		$data['rows'] = $this->modelo_usuarios->listing()->result_array();
		//$data['content'] = $this->load->view('usuarios/listar', $data, true);

		$data['subcontent'] = $this->load->view('usuarios/listar', $data, true);
		$data['content'] = $this->load->view('usuarios/usuarios-index', $data, true);
		$this->load->view('template', $data);

	}
	function groups_list() {

		$grupos = $this->modelo_usuarios->listing_groups()->result_array();
		$grupos_temp = array();
		foreach ($grupos as $key => $value) {
			$grupos_temp[$value['id']] = $value['name'];
		}
		return $grupos_temp;
	}

	function rol_list() {

		$grupos = $this->modelo_usuarios->listing_rol()->result_array();
		$grupos_temp = array();
		foreach ($grupos as $key => $value) {
			$grupos_temp[$value['id']] = $value['name'];
		}
		return $grupos_temp;
	}

	public function rol_rel() {
		$data = array();
		$data['tab'] = 2;
		$id = (int) $this->uri->segment(3);
		$data['id'] = $id;
		if ($id <= 0) {
			redirect(site_url('usuarios/index'));
		}

		$p = array();
		$p[] = $id;
		$data['usuario'] = $this->modelo_usuarios->get_usuarios_por_id($id);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('users_id', 'Nombre de usuario', 'required');
		$this->form_validation->set_rules('rol_id', 'Rol', 'required');

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p[] = $this->input->post('users_id');
			$p[] = ($this->input->post('rol_id'));
			$temp = $this->modelo_usuarios->adding_users_rol($p[0], $p[1]);

			$this->session->set_flashdata('mensaje', 'Se agrego un nuevo rol.');

			redirect(site_url('usuarios/rol_rel/' . $id));
		}
		$data['rol_id'] = $this->rol_list();
		$data['rol_rel'] = $this->modelo_usuarios->listing_users_rol($id)->result_array();
		$data['subcontent'] = $this->load->view('usuarios/rol_rel', $data, true);
		$data['content'] = $this->load->view('usuarios/usuarios-index', $data, true);
		$this->load->view('template', $data);
	}
	public function grupos_rel() {
		$data = array();
		$data['tab'] = 2;
		$id = (int) $this->uri->segment(3);
		$data['id'] = $id;
		if ($id <= 0) {
			redirect(site_url('usuarios/index'));
		}

		$p = array();
		$p[] = $id;
		$data['usuario'] = $this->modelo_usuarios->get_usuarios_por_id($id);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('users_id', 'Nombre de usuario', 'required');
		$this->form_validation->set_rules('groups_id', 'Grupo', 'required');

		if ($this->form_validation->run() == FALSE) {
		} else {
			$p = array();
			$p[] = $this->input->post('users_id');
			$p[] = ($this->input->post('groups_id'));
			$temp = $this->modelo_usuarios->adding_users_groups($p[0], $p[1]);

			$this->session->set_flashdata('mensaje', 'Se agrego un nuevo grupo.');

			redirect(site_url('usuarios/grupos_rel/' . $id));
		}
		$data['groups_id'] = $this->groups_list();
		$data['grupos_rel'] = $this->modelo_usuarios->listing_users_groups($id)->result_array();
		$data['subcontent'] = $this->load->view('usuarios/grupos_rel', $data, true);
		$data['content'] = $this->load->view('usuarios/usuarios-index', $data, true);
		$this->load->view('template', $data);
	}
	function quitar_grupo() {
		$id = (int) $this->uri->segment(3);
		$users_id = (int) $this->uri->segment(4);
		$p['id'] = $id;
		$temp = $this->modelo_usuarios->deleting_users_groups($id);

		$this->session->set_flashdata('mensaje', 'El grupo quitado');
		redirect(site_url('usuarios/grupos_rel/' . $users_id));
	}

	function quitar_rol() {
		$id = (int) $this->uri->segment(3);
		$users_id = (int) $this->uri->segment(4);
		$p['id'] = $id;
		$temp = $this->modelo_usuarios->deleting_users_rol($id);

		$this->session->set_flashdata('mensaje', 'El rol fue quitado');
		redirect(site_url('usuarios/rol_rel/' . $users_id));
	}

}

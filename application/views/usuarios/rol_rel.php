<div class="panel panel-default">
	<div class="panel-heading">
		Asignar rol al usuario:  <label> <?php echo $usuario['username']; ?> </label>
	</div>
	<div class="panel-body">
		<?php _help_mensajes();?>
		<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
		<form method="post" class="form-horizontal">
			<input type="hidden" class="form-control" name="users_id" value="<?php echo $id; ?>">
			<div class="row">
				<div class="col-sm-2"> Rol </div>
				<div class="col-sm-2">
					<?php echo form_dropdown('rol_id', $rol_id); ?>
				</div>
				<div class="col-sm-3">   <button type="submit"  class="btn btn-primary btn-xs"> Agregar  Rol</button> </div>
			</div>
		</form>
		<br>
		<div class=" ">
			<table class=" table table-hove">
				<caption>	Roles actuales:</caption>
				<thead>
					<tr>
						<td>  Rol </td>
						<td>  Acción </td>
					</tr>
				</thead>
				<?php foreach ($rol_rel as $key => $value) {
	?>
				<tr>
					<td> <?php echo $rol_id[$value['authbear_rol_id']] ?>  </td>
					<td><a class="btn btn-xs btn-primary" href="<?php echo site_url('usuarios/quitar_rol/' . $value['id'] . '/' . $id); ?>"> Quitar </a></td>
				</tr>
				<?PHP
}?>
			</table>
		</div>
	</div>
</div>
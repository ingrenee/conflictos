<?php
class Tipos_model extends CI_Model {

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_active() {
		$w['status'] = 1;
		return $this->db->select('*')->where($w)->order_by('nombre', 'asc')->get('conflictos.tipos')->result_array();
	}
	public function get_list() {

		return $this->db->select('*')->order_by('nombre', 'asc')->get('conflictos.tipos');
	}

	function save($p = array()) {

		$this->db->insert('conflictos.tipos', $p);
		return $this->db->insert_id();

	}

	function get($id) {
		$rows = $this->db->select('*')->where('id', $id)->get('conflictos.tipos')->row_array();

		return $rows;
	}
	public function get_verifica_nombre($str, $id) {
		$w['id !='] = $id;
		$rows = $this->db->select('nombre')->where($w)->where('nombre', $str)->get('conflictos.tipos')->num_rows();

		return $rows;
	}
	public function update($p = array(), $id) {

		$this->db->where('id', $id)->update('conflictos.tipos', $p);
		return 1;
	}

}

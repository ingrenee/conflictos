<div id="imprimir" class="c">

	<div class="well well-sm">
		<div class="row">
			<div class="col-sm-2"> <label for="">Nombre del conflicto:</label> </div>
			<div class="col-sm-10">
				<p>
					<?php echo $conflicto['nombre']; ?>
				</p>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="input" class="col-sm-3 control-label">Aprendizaje:</label>
		<div class="col-sm-9">
			<p> <?php echo $row['aprendizaje']; ?></p>
		</div>
	</div>
	<div class="form-group">
		<label for="" class="control-label col-sm-3">Propuesta:</label>
		<div class="col-sm-9">
			<p><?php echo $row['propuesta']; ?></p>
		</div>
	</div>
	<div class="form-group">
		<label for="" class="control-label col-sm-3">Responsable:</label>
		<div class="col-sm-9">
			<p><?php echo $row['responsable']; ?></p>
		</div>
	</div>
	<div class="form-group">
		<label for="" class="control-label col-sm-3">Fecha:</label>
		<div class="col-sm-9">
			<p><?php echo $row['fecha']; ?></p>
		</div>
	</div>
</div>
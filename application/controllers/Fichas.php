<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Fichas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *      http://example.com/index.php/welcome
	 *  - or -
	 *      http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
		$this->load->model('procedimientos_model', 'procedimientos_model');
	//	$this->load->model('Concejos_regionales_model', 'modelo_concejos_regionales');
	//	$this->load->model('Nucleos_locales_model', 'modelo_nucleos_locales');
	//	$this->load->model('Productores_model', 'modelo_productores');
	}
	public function basica_productor_editar() {
		$data = array();

		$data['tab'] = 2;
		$data['subtab'] = 5;
		$data['html'] = ' class="form-control" ';

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		// $temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;

		$data['productores'] = $productor = $temp; //->row_array();
		$temp = NULL;
		$data['productor_dni'] = $temp_dni;

		//  listado de nucleo local
		//  $temp = $this->procedimientos_model->sp_select('usp_nucleos_listado', array());

		$w = array();
		$w['concejos_regionales_id'] = $productor['consejos_regionales_id'];

		$temp = $this->modelo_nucleos_locales->get_listado($w);

		$form_nucleos = $temp;

		//->result_array();

		$data['form_nucleos'] = rows_array($form_nucleos, array('id', 'nombre'));

		//  listado de nucleo local
		/*
	                $temp = $this->procedimientos_model->sp_select('usp_consejos_regionales_listado', array());

	                $form_consejos_regionales = $temp->result_array();

	                $data['form_consejos_regionales'] = rows_array($form_consejos_regionales, array('id', 'nombre'));
*/

		/**
		 * Guardamos   los concejos regionales asignados al usuario.
		 */
		$usuario_concejos_regionales = array_keys(_helper_usuario_concejos_regionales());

		/**
		 * obtenemos informacion de los concejos regionales desde la base de datos
		 */
		$temp = $this->modelo_concejos_regionales->get_concejos_regionales_id($usuario_concejos_regionales);

		$form_consejos_regionales = ($temp);

		/**
		 * Generamos un array "ID:NOMBRE" de los concejos regionales
		 * para crear la lista desplegable
		 */
		$data['form_consejos_regionales'] = rows_array($form_consejos_regionales, array('id', 'nombre'));

		/***************************************/

		$this->load->library('form_validation');

		$this->form_validation->set_rules('form_dni', 'DNI', 'required');

		if ($this->form_validation->run() == FALSE) {
		} else {

			/* guardamos la informacion */

			// recepcionamos los datos del  formulario :)
			//select   sgpteam.usp_productores_ins_upd
			//(0,0,'codigo','42864625','Renee','Morales','calhua','2015-06-07')
			$parametros['id'] = (int) $this->input->post('id');
			$parametros['consejos_regionales_id'] = (int) $this->input->post('form_consejos_regionales_id');
			$parametros['nucleos_id'] = (int) $this->input->post('form_nucleos_id');
			$parametros['codigo'] = $this->input->post('form_codigo');
			$temp_dni = $parametros['dni'] = $this->input->post('form_dni');
			$parametros['nombres'] = strtoupper($this->input->post('form_nombres'));
			$parametros['paterno'] = strtoupper($this->input->post('form_paterno'));
			$parametros['materno'] = strtoupper($this->input->post('form_materno'));
			$parametros['fnacimiento'] = $this->input->post('form_fnacimiento');
			$parametros['departamentos_id'] = (int) $this->input->post('form_departamentos_id');
			$parametros['provincias_id'] = (int) $this->input->post('form_provincias_id');
			$parametros['distritos_id'] = (int) $this->input->post('form_distritos_id');
			$parametros['comunidades_id'] = (int) $this->input->post('form_comunidades_id');
			$parametros['celular'] = $this->input->post('form_celular');
			$parametros['telefono'] = $this->input->post('form_telefono');
			$parametros['email'] = $this->input->post('form_email');
			$parametros['direccion'] = $this->input->post('form_direccion');

			if (empty($parametros['fnacimiento'])):
				$parametros['fnacimiento'] = NULL;
			endif;

			//print_r($parametros);
			//$temp = $this->procedimientos_model->sp_ins_upd('usp_productores_ins_upd', $parametros);
			$parametros['modificado'] = date('Y-m-d H:i:s');

			//$parametros['usuario_crea']=_helper_usuario_dni();
			$parametros['usuario_modifica'] = _helper_usuario_dni();

			$temp = $this->modelo_productores->updateProductor($parametros);

			//print_r($parametros);
			// $temp = $this->procedimientos_model->sp_ins_upd('usp_productores_ins_upd', $parametros);
			//$temp = _help_get_message($temp);

			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo el productor de dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'Se registro el productor de dni:' . $temp_dni);

				// insert

			endif;

			redirect(site_url('fichas/basica_productor_seccion_1/' . $temp_dni));
		}

		/*
			                                                        extraemos informacion de los departamen tos
		*/

		//usp_ubigeo_get
		/*
	                                                        extraemos informacion de los prpovincias
*/

		/*
			                                                        extraemos informacion de los ditritos
		*/

		/* buscamos el departamento amarrado al concejo regional*/

		$cr = $this->modelo_concejos_regionales->get_concejos_regionales_id(array('id' => $productor['consejos_regionales_id']));

		//$data['departamentos'] = $this->ubigeo();
		$dep_temp = $this->ubigeo();

		$dep_temp_final = array();
		$dep_temp_final[''] = 'seleccione';
		$dep_temp_final[$cr['departamentos_id']] = $cr['departamentos_nombre'];

		foreach ($dep_temp as $k => $v) {
			if ($k == $productor['departamentos_id']):
				$dep_temp_final[$k] = $v;
			endif;
		}

		$data['departamentos'] = $dep_temp_final;

		$data['provincias'] = $this->ubigeo(2, $productor['departamentos_id']);
		$data['distritos'] = $this->ubigeo(3, $productor['departamentos_id'], $productor['provincias_id']);

		$codigo_comunidades = $this->completaNumero($this->completaNumero($productor['distritos_id']));
		$data['comunidades'] = $this->ubigeo_comunidades($codigo_comunidades);
		$data['seccion'] = $this->load->view('fichas/basica_productor_ficha', $data, true);
		$data['subcontent'] = $this->load->view('fichas/basica_productor_ficha_secciones', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}

	public function pdf() {

		$tipo = $this->uri->segment(4);
		if ($tipo == 'word'):
			$data['formato'] = 'word';
		else:
			$data['formato'] = 'pdf';
		endif;

		$html = '';

		$html .= $this->basica_productor_editar_pdf($data['formato']);
		$html .= $this->basica_productor_seccion_1_pdf($data['formato']);

		$html .= $this->basica_productor_seccion_2_pdf($data['formato']);
		$html .= $this->basica_productor_seccion_3_pdf($data['formato']);

		$html .= $this->basica_productor_seccion_4_pdf($data['formato']);

		$html = utf8_decode($html);

		/*
			                                                                    echo $html;
			                                                                    exit(0);
		*/

		$data['contenido'] = $html;

		if ($tipo == 'word'):

			$html = $this->load->view('fichas/template_word', $data, true);
			echo $html;
		else:

			$html = $this->load->view('fichas/template_pdf', $data, true);

			define("DOMPDF_ENABLE_REMOTE", true);
			require_once FCPATH . "/pdf/dompdf_config.inc.php";
			$dompdf = new DOMPDF();
			$dompdf->load_html(stripslashes($html));
			$dompdf->set_paper('a4', 'portrait');
			$dompdf->render();
			$time = date('Y_m_d_H_i_s');
			$dompdf->stream("fichas_$time.pdf", array("Attachment" => true));
			exit(0);
		endif;
	}

	public function basica_productor_editar_pdf($formato) {
		$data = array();
		$data['formato'] = $formato;
		$data['tab'] = 2;
		$data['subtab'] = 5;
		$data['html'] = ' class="form-control" ';

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		//$temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;

		$data['productores'] = $productor = $temp; //->row_array();
		$temp = NULL;
		$data['productor_dni'] = $temp_dni;

		//  listado de nucleo local
		$temp = $this->procedimientos_model->sp_select('usp_nucleos_listado', array());

		$form_nucleos = $temp->result_array();

		$data['form_nucleos'] = rows_array($form_nucleos, array('id', 'nombre'));

		//  listado de nucleo local
		//$temp = $this->procedimientos_model->sp_select('usp_consejos_regionales_listado', array());
		$temp = $this->modelo_concejos_regionales->get_concejos_regionales_listado();
		$form_consejos_regionales = $temp;

		$data['form_consejos_regionales'] = rows_array($form_consejos_regionales, array('id', 'nombre'));

		/*
			                                                        extraemos informacion de los departamen tos
		*/

		//usp_ubigeo_get
		/*
	                                                        extraemos informacion de los prpovincias
*/

		/*
			                                                        extraemos informacion de los ditritos
		*/

		$data['departamentos'] = $this->ubigeo();

		$data['provincias'] = $this->ubigeo(2, $productor['departamentos_id']);
		$data['distritos'] = $this->ubigeo(3, $productor['departamentos_id'], $productor['provincias_id']);
		$codigo_comunidades = $this->completaNumero($productor['distritos_id']);
		$data['comunidades'] = $this->ubigeo_comunidades($codigo_comunidades);
		return $data['seccion'] = $this->load->view('fichas/basica_productor_ficha_pdf', $data, true);
	}
	public function basica_productor_seccion_1_pdf($formato) {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 1;
		$data['formato'] = $formato;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		// $temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$temp = $this->procedimientos_model->sp_select('usp_seccion1_por_dni', $param);
		$data['row'] = $temp->row_array();
		return $data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_1_pdf', $data, true);
	}
	public function basica_productor_seccion_2_pdf($formato) {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 2;
		$data['formato'] = $formato;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		//$temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$temp = $this->procedimientos_model->sp_select('usp_seccion2_por_dni', $param);
		$data['row'] = $temp->row_array();

		return $data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_2_pdf', $data, true);
	}

	public function basica_productor_seccion_3_pdf($formato) {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 3;
		$data['formato'] = $formato;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		//$temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$data['row'] = array();

		/**
		 * extraemos informacion de la seccion 3 y lo pasamos ala  vista.
		 */
		$param = array();
		$param[] = $temp_dni;
		$temp = $this->procedimientos_model->sp_select('usp_seccion3_get', $param)->row_array();
		$data['seccion3'] = $temp;

		//  recuperando las parcelas
		$param = array();
		$param[] = $temp_dni;
		$temp = $this->procedimientos_model->sp_select('usp_parcelas_por_dni', $param);
		$form_name = $this->input->post('js_form_name');
		if ($form_name == '_agregar_parcela_1'):
			$this->basica_productor_seccion_3_parcela_1();
		elseif ($form_name == '_agregar_parcela_2'):
			if (!isset($_POST['borrar_parcela_1'])):
				$this->basica_productor_seccion_3_parcela_2();
			else:
				$this->basica_productor_seccion_3_parcela_3();
			endif;
		elseif ($form_name == '_agregar_parcela_4'):
			$this->basica_productor_seccion_3_parcela_4();
		elseif ($form_name == '_accion_guardar_seccion_3'):
			$this->basica_productor_seccion_3_ins_upd();
		endif;
		$data['parcelas'] = $temp->result_array();
		return $data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_3_pdf', $data, true);
	}

	public function basica_productor_seccion_4_pdf($formato) {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 4;
		$data['formato'] = $formato;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		// $temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$param = array();
		$param[] = $temp_dni;
		$data['row'] = $this->procedimientos_model->sp_select('usp_seccion4_get', $param)->row_array();
		$param = array();
		$param[] = $temp_dni;
		$data['cultivos'] = $this->procedimientos_model->sp_select('usp_cultivos_get', $param)->result_array();
		$param = array();
		$param[] = $temp_dni;
		$data['animales'] = $this->procedimientos_model->sp_select('usp_animales_get', $param)->result_array();
		$form_name = $this->input->post('js_form_name');

		$param = array();
		$param[] = $temp_dni;
		$temp = $this->procedimientos_model->sp_select('usp_parcelas_por_dni', $param);

		$data['parcelas'] = $temp->result_array();

		return $data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_4_pdf', $data, true);
	}

	function help() {
		$q = 'select * from "sgpteam"."ubigeo2006" ';

		$row = $this->db->query($q)->result_array();

		foreach ($row as $key => $value) {
			$num = $value['coddpto'] . $value['codprov'] . $value['coddist'];
			$id = $value['id'];
			$q2 = ' update  "sgpteam"."ubigeo2006"  set  coddist_id=' . $num . ' where coddpto=' . $value['coddpto'] . ' and codprov=' . $value['codprov'] . ' and coddist=' . $value['coddist'];

			$this->db->query($q2);
		}
	}

	function help2() {
		$q = 'select * from "sgpteam"."ubigeo2006" ';

		$row = $this->db->query($q)->result_array();

		foreach ($row as $key => $value) {
			$num = $value['coddpto'] . $value['codprov'] . '0';
			$id = $value['id'];
			$q2 = ' update  "sgpteam"."ubigeo2006"  set  codprov_id=' . $num . ',id=' . $num . ' where coddpto=' . $value['coddpto'] . ' and codprov=' . $value['codprov'] . ' and coddist=' . $value['coddist'];

			$this->db->query($q2);
		}
	}
	function help3() {
		$q = 'select * from "sgpteam"."ubigeo2006" ';

		$row = $this->db->query($q)->result_array();

		foreach ($row as $key => $value) {
			$num = $value['coddpto'] . '00';
			$id = $value['id'];
			$q2 = ' update  "sgpteam"."ubigeo2006"  set  coddpto_id=' . $num . ' where coddpto=' . $value['coddpto'] . ' and codprov=' . $value['codprov'] . ' and coddist=' . $value['coddist'];

			$this->db->query($q2);
		}
	}
	function help4() {
		$q = 'select * from "sgpteam"."comunidades" ';

		$row = $this->db->query($q)->result_array();

		foreach ($row as $key => $value) {
			$num = str_split($value['codigo']);
			if (count($num) == 6):
				$a = (int) ($num[0] . $num[1]);
				$b = (int) ($num[2] . $num[3]);
				$c = (int) ($num[4] . $num[5]);

				$id = (int) $value['id'];
				$q2 = ' update  "sgpteam"."comunidades"  set  coddist_id=' . $a . $b . $c . ' where id=' . $id;

				$this->db->query($q2);
			endif;
		}
	}
	function ajax_ubigeo() {
		$p = array();
		$tipo = $p[] = $this->input->post('tipo');
		$p[] = $this->input->post('a');
		$p[] = $this->input->post('b');
		$p[] = $this->input->post('c');
		if ($tipo == 4) {
			$codigo_comunidades = $this->completaNumero($p[3]);

			//$this->completaNumero($p[1]) . $this->completaNumero($p[2]) . $this->completaNumero($p[3]);

			$temp = $this->ubigeo_comunidades($codigo_comunidades);
		} else {
			$temp = $this->ubigeo($p[0], $p[1], $p[2], $p[3]);
		}
		echo "<option value=''>Seleccione</option>";
		foreach ($temp as $key => $value) {
			echo "<option value='" . $key . "'>" . $value . "</option>";
		}
	}
	function json_ubigeo() {
		$p = array();

		/*
			                                                        $tipo=$p[]=$this->input->post('tipo');
			                                                        $p[]=$this->input->post('a');
			                                                        $p[]=$this->input->post('b');
			                                                        $p[]=$this->input->post('c');
		*/
		$tipo = $p[] = $_REQUEST['tipo'];
		$p[] = $_REQUEST['a'];
		$p[] = $_REQUEST['b'];
		$p[] = $_REQUEST['c'];
		if ($tipo == 4) {
			$codigo_comunidades = $this->completaNumero($p[3]);

			//$this->completaNumero($p[1]) . $this->completaNumero($p[2]) . $this->completaNumero($p[3]);

			$temp = $this->ubigeo_comunidades($codigo_comunidades);

		} else {

			$temp = $this->ubigeo($p[0], $p[1], $p[2], $p[3]);
			
		}

		//echo "<option value=''>seleccione</option>";
		$c['id'] = '';
		$c['nombre'] = 'Seleccione';
		$r[] = $c;
		foreach ($temp as $key => $value) {

			// echo "<option value='".$key."'>".$value."</option>";
			$c['id'] = $key;
			$c['nombre'] = $value;
			$r[] = $c;
		}
		if (isset($_GET['callback'])) {

			// Si es una petición cross-domain
			echo $_GET['callback'] . '(' . json_encode($r) . ')';
		}
	}
	function json_parcelas() {
		$dni = isset($_REQUEST['dni']) ? $_REQUEST['dni'] : '';
		$periodos_id = isset($_REQUEST['periodos_id']) ? $_REQUEST['periodos_id'] : '';

		$temp_rows = array();

		$rows = $this->db->select('W.cultivo_nombre,W.id,Z.puntos, Z.nombre,Y.fecha_siembra, Y.fecha_cosecha')->where('X.productores_dni', $dni)->where('X.periodos_id', $periodos_id)->where('X.id', 'Y.plan_produccion_id')->where('Y.parcelas_id', 'Z.id')->where('Z.cultivos_id', 'W.id')->get('sgpteam.plan_produccion X, sgpteam.plan_produccion_agricola Y, parcelas Z,cultivos W')->result_array();
		foreach ($rows as $key => $value) {

			//$value['ruta']=_help_imagen_productor($value['dni']);
			//$value['periodos_id']=$periodos_id['id'];
			$temp_rows[] = $value;
		}

		/*
			                                                        $parcelas=$this->db->select('puntos')->where_in('productores_dni',$dnis)->get('sgpteam.parcelas')->result_array();
			                                                        $temp_parcelas=array();
			                                                        foreach ($parcelas as $key => $value) {
			                                                        $temp=array();
			                                                        $temp['puntos']=$value['puntos'];
			                                                        //$temp['puntos']=$value['nombre'];
			                                                        }
		*/

		//echo json_encode($temp_rows);
		if (isset($_GET['callback'])) {

			// Si es una petición cross-domain
			echo $_GET['callback'] . '(' . json_encode($temp_rows) . ')';
		}
	}
	function json_conflictos() {

		$comunidades_id = (isset($_REQUEST['com']) && (int) $_REQUEST['com'] > 0) ? $_REQUEST['com'] : '';
		$distritos_id = (isset($_REQUEST['distritos_id']) && (int) $_REQUEST['distritos_id'] > 0) ? $_REQUEST['distritos_id'] : '';
		$provincias_id = (isset($_REQUEST['provincias_id']) && (int) $_REQUEST['provincias_id'] > 0) ? $_REQUEST['provincias_id'] : '';
		$departamentos_id = (isset($_REQUEST['departamentos_id']) && (int) $_REQUEST['departamentos_id'] > 0) ? $_REQUEST['departamentos_id'] : '';
		$pagina = (int) $_REQUEST['pagina'];
		$excel=0;
		if(isset($_REQUEST['excel'])):
		$excel = (int) $_REQUEST['excel'];
		endif;
		$por_pagina = (int) $_REQUEST['por_pagina'];
		if ($por_pagina <= 0):
			$por_pagina = 10;
		endif;

		if ($pagina <= 0):
			$pagina = 1;
		endif;

		$offset = ($pagina - 1) * $por_pagina + 0;

		$where = ' where departamento_cod=' . $departamentos_id;

		if ($provincias_id != ''):
			$where .= ' and provincia_cod=' . $provincias_id;
			if ($distritos_id != ''):
				$where .= '  and  distrito_cod=' . $distritos_id;
				if ($comunidades_id != ''):
					$where .= '  and  comunidad=' . $comunidades_id;
				endif;
			endif;
		endif;
		if($excel==0):
		$where .= ' limit ' . $por_pagina . ' offset ' . $offset;
	 	endif;
		//select_productores_list

		 $SQL='select * from  conflictos.conflictos  '.$where;

		 $rows= $this->db->query($SQL)->result_array();
		//$rows = $this->procedimientos_model->query_select('select_conflictos_list', $where)->result_array();

		//$this->load->view('fichas/ajax_productores', $data);
		//_help_imagen_productor($value['dni']);
		$temp_rows = array();
		//$temp_rows= $rows;
	//	$dnis = array();
		//$periodos_id = $this->db->select('id')->where('flag_principal', 1)->limit(1)->get('sgpteam.periodos')->row_array();

		 $estado_texto['']='';
		 $estado_texto['1']='  1. Prevención ';
		 $estado_texto['2']='  2. Tratamiento ';
		 $estado_texto['3']='  3.Seguimiento ';
$this->load->model('Tipos_model', 'tipos_modelo');
	$tipos = $this->tipos_modelo->get_active();
			
		 
$tipos_text=array();
 foreach ($tipos as $kk => $vv) {
 	$tipos_text[$vv['id']]= $vv['nombre'];
 }
  $tipos_text['']='';
			//$estados = $this->db->select('*')->get($this->schema . 'estados')->result_array();
	 

			/****************/
			


  

		foreach ($rows as $key => $value) {
	    //  $value['ruta'] = _help_imagen_productor($value['dni']);
		//	$value['periodos_id'] = $periodos_id['id'];
		
		$value['link_pdf']= site_url('conflictos/exportar_pdf/'.$value['id']);
		
		 $value['estado_texto']= $estado_texto[$value['estado']];



$mis_tipos = $this->db->select('tipos_id')->where('conflictos_id', $value['id'])->get( 'conflictos.tipos_has_conflictos')->result_array();
			$mis_tipos_array = '';
			 if(count($mis_tipos)>0):
			foreach ($mis_tipos as $k => $v) {
				$mis_tipos_array .= $tipos_text[$v['tipos_id']].'/';
			}
			 	endif;





			$value['tipos_texto']= $mis_tipos_array;





		$temp_rows[] = $value;
		
		//$dnis[] = $value['dni'];

		}

		/*
			                                                        $parcelas=$this->db->select('puntos')->where_in('productores_dni',$dnis)->get('sgpteam.parcelas')->result_array();
			                                                        $temp_parcelas=array();
			                                                        foreach ($parcelas as $key => $value) {
			                                                        $temp=array();
			                                                        $temp['puntos']=$value['puntos'];
			                                                        //$temp['puntos']=$value['nombre'];
			                                                        }
		*/

 if($excel==1):
 	$data=array();
 $data['rows']=$temp_rows;

echo $data['content'] = $this->load->view('fichas/fichas_excel', $data, true);
 
 	return 0;
 	endif;
		//echo json_encode($temp_rows);
		if (isset($_GET['callback'])) {

			// Si es una petición cross-domain
			echo $_GET['callback'] . '(' . json_encode($temp_rows) . ')';
		}
	}
	function json_productor_parcelas() {

		$dni = isset($_REQUEST['dni']) ? $_REQUEST['dni'] : '';

		//select_productores_list
		//$this->load->view('fichas/ajax_productores', $data);
		//_help_imagen_productor($value['dni']);
		$temp_rows = array();
		$dnis = array();
		$periodos_id = $this->db->select('id')->where('flag_principal', 1)->limit(1)->get('sgpteam.periodos')->row_array();
		$rows = $this->procedimientos_model->query_select('select_productor_parcelas', " where X.productores_dni='" . $dni . "'")->result_array();
		$data['rows'] = $rows;

		//$contenido=$this->load->view('fichas/json_propductor_parcelas',$data,true);
		/*
	                                                        foreach ($rows as $key => $value) {
	                                                        $value['ruta']=_help_imagen_productor($value['dni']);
	                                                        $value['periodos_id']=$periodos_id['id'];
	                                                        $temp_rows[]=$value;
	                                                        $dnis[]=$value['dni'];
	                                                        }
*/

		/*
			                                                        $parcelas=$this->db->select('puntos')->where_in('productores_dni',$dnis)->get('sgpteam.parcelas')->result_array();
		*/
		$temp_parcelas = array();
		foreach ($rows as $key => $value) {
			$temp = array();
			$temp['puntos'] = $value['puntos'];
			$temp['nombre'] = $value['nombre'];
			$temp['dni'] = $value['dni'];
			$temp['id'] = $value['id'];
			$temp['periodos_id'] = $periodos_id['id'];
			$temp_parcelas[] = $temp;
		}

		//echo json_encode($temp_rows);
		if (isset($_GET['callback'])) {

			// Si es una petición cross-domain
			echo $_GET['callback'] . '(' . json_encode($temp_parcelas) . ')';
		}
	}
	function json_productor_parcelas_info() {

		$dni = isset($_REQUEST['dni']) ? $_REQUEST['dni'] : '';
		$parcelas_id = isset($_REQUEST['parcelas_id']) ? $_REQUEST['parcelas_id'] : '';
		$periodos_id = isset($_REQUEST['periodos_id']) ? $_REQUEST['periodos_id'] : '';

		$rows = $this->procedimientos_model->json_productor_parcelas($dni, $periodos_id, $parcelas_id);
		$rows['dni'] = $dni;
		$rows['parcelas_id'] = $parcelas_id;
		$rows['periodos_id'] = $periodos_id;
		$url = site_url('fichas/get_productor_parcelas_info/?dni=' . $dni . '&parcelas_id=' . $parcelas_id . '&periodos_id=' . $periodos_id);
		$rows['url'] = $url;

		//echo json_encode($temp_rows);
		if (isset($_GET['callback'])) {

			// Si es una petición cross-domain
			echo $_GET['callback'] . '(' . json_encode($rows) . ')';
		}
	}

	function get_productor_parcelas_info() {

		$dni = isset($_REQUEST['dni']) ? $_REQUEST['dni'] : '';
		$parcelas_id = isset($_REQUEST['parcelas_id']) ? $_REQUEST['parcelas_id'] : '';
		$periodos_id = isset($_REQUEST['periodos_id']) ? $_REQUEST['periodos_id'] : '';

		$rows = $this->procedimientos_model->json_productor_parcelas($dni, $periodos_id, $parcelas_id);

		$data['rows'] = $rows;

		//echo json_encode($temp_rows);
		$html = $this->load->view('fichas/get_productor_parcelas_info', $data, true);

		$html = utf8_decode($html);
		$data['contenido'] = $html;

		$html = $this->load->view('fichas/template_pdf', $data, true);

		define("DOMPDF_ENABLE_REMOTE", true);
		require_once FCPATH . "/pdf/dompdf_config.inc.php";
		$dompdf = new DOMPDF();
		$dompdf->load_html(stripslashes($html));
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
		$time = date('Y_m_d_H_i_s');
		$dompdf->stream("fichas_productor_resumen_$time.pdf", array("Attachment" => true));
		exit(0);
	}

	function ajax_nucleos_locales_filtrado() {
		$valor = $this->input->post('valor');

		$w['concejos_regionales_id'] = $valor;
		$r = $this->modelo_nucleos_locales->get_listado($w);

		$temp[''] = 'seleccione';

		foreach ($r as $k => $v) {
			$temp[$v['id']] = $v['nombre'];
		}

		echo form_dropdown('form_nucleos_id', $temp, '');
	}
	function ajax_departamentos_filtrado() {

		$valor = $this->input->post('valor');

		$w['id'] = $valor;
		$r = $this->modelo_concejos_regionales->get_concejos_regionales_id($w);

		$temp[''] = 'seleccione';
		$temp[$r['departamentos_id']] = $r['departamentos_nombre'];

		echo form_dropdown('form_departamentos_id', $temp, '');
	}
	function ajax_productores() {

		$comunidad_id = $this->input->post('com');

		//select_productores_list
		$data['rows'] = $this->procedimientos_model->query_select('select_productores_list', " where X.comunidades_id='" . $comunidad_id . "'")->result_array();
		$this->load->view('fichas/ajax_productores', $data);
	}
	function ubigeo_comunidades($codigo_comunidades) {
		$temp = array();
		$variable = $this->procedimientos_model->sp_select('usp_ubigeo_comunidades_get', array($codigo_comunidades))->result_array();
		foreach ($variable as $key => $value) {

			$temp[$value['id']] = $value['nombre'];
		}
		return $temp;
	}
	function completaNumero($num) {
		return ($num < 10) ? '0' . $num : $num;
	}
	function ubigeo($tipo = 1, $a = 0, $b = 0, $c = 0) {
		$p = array();
		$p[] = $tipo;
		$p[] = $a;
		$p[] = $b;
		$p[] = $c;

		if ($tipo == 1) {
			$col = 'departamentos_id';
		}

		if ($tipo == 2) {
			$col = 'provincias_id';
		}

		if ($tipo == 3) {
			$col = 'distritos_id';
		}

		$temp = array();
		$variable = $this->procedimientos_model->sp_select('usp_ubigeo_get', $p)->result_array();
		foreach ($variable as $key => $value) {

			$temp[$value[$col]] = $value['nombre'];
		}
		return $temp;
	}
	public function cambiar_estado() {
		$dni = $this->uri->segment(3);

		$temp = _helper_info_productor($dni);

		if (empty($temp)):
			echo "El dni  ingresado no existe.";
			exit();
		else:
			$estado = $temp['estado'];
			if ($estado == 'transicion'):
				$temp['estado'] = 'certificado';

			else:

				$temp['estado'] = 'transicion';
			endif;

			$up['estado'] = $temp['estado'];
			$up['dni'] = $dni;
			$up['modificado'] = date('Y-m-d H:i:s');
			$up['usuario_modifica'] = _helper_usuario_dni();

			$this->modelo_productores->updateProductor($up);
		endif;
		redirect(site_url('fichas/basica_productor_listar/'));
	}
	public function basica_productor() {
		$data = array();
		$data['tab'] = 1;
		$data['html'] = ' class="form-control" ';
		$data['productores'] = array();

		//echo _helper_usuario_concejo_regional();

		//  listado de nucleo local
		$temp = $this->procedimientos_model->sp_select('usp_nucleos_listado', array());
		$form_nucleos = $temp->result_array();
		$data['form_nucleos'] = rows_array($form_nucleos, array('id', 'nombre'));

		//  listado de nucleo local
		//$temp = $this->procedimientos_model->sp_select('usp_consejos_regionales_listado', array());
		// $concejo_regional_id = (int)_helper_usuario_concejo_regional();

		// $temp = $this->modelo_concejos_regionales->get_concejos_regionales_id(array('id' => $concejo_regional_id));

		/**
		 * Guardamos   los concejos regionales asignados al usuario.
		 */
		$usuario_concejos_regionales = array_keys(_helper_usuario_concejos_regionales());

		/**
		 * obtenemos informacion de los concejos regionales desde la base de datos
		 */
		$temp = $this->modelo_concejos_regionales->get_concejos_regionales_id($usuario_concejos_regionales);

		$form_consejos_regionales = ($temp);

		/**
		 * Generamos un array "ID:NOMBRE" de los concejos regionales
		 * para crear la lista desplegable
		 */
		$data['form_consejos_regionales'] = rows_array($form_consejos_regionales, array('id', 'nombre'));

		/***************************************/
		$this->load->library('form_validation');
		$this->form_validation->set_rules('form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {

			/* guardamos la informacion */

			// recepcionamos los datos del  formulario :)
			//select   sgpteam.usp_productores_ins_upd
			//(0,0,'codigo','42864625','Renee','Morales','calhua','2015-06-07')
			$parametros['consejos_regionales_id'] = (int) $this->input->post('form_consejos_regionales_id');
			$parametros['nucleos_id'] = (int) $this->input->post('form_nucleos_id');
			$parametros['codigo'] = $this->input->post('form_codigo');
			$temp_dni = $parametros['dni'] = $this->input->post('form_dni');
			$parametros['nombres'] = strtoupper($this->input->post('form_nombres'));
			$parametros['paterno'] = strtoupper($this->input->post('form_paterno'));
			$parametros['materno'] = strtoupper($this->input->post('form_materno'));
			$parametros['fnacimiento'] = $this->input->post('form_fnacimiento');
			$parametros['departamentos_id'] = (int) $this->input->post('form_departamentos_id');
			$parametros['provincias_id'] = (int) $this->input->post('form_provincias_id');
			$parametros['distritos_id'] = (int) $this->input->post('form_distritos_id');
			$parametros['comunidades_id'] = (int) $this->input->post('form_comunidades_id');
			$parametros['celular'] = $this->input->post('form_celular');
			$parametros['telefono'] = $this->input->post('form_telefono');
			$parametros['email'] = $this->input->post('form_email');
			$parametros['direccion'] = $this->input->post('form_direccion');
			$parametros['periodos_id'] = _helper_periodo('id');
			if (empty($parametros['fnacimiento'])):
				$parametros['fnacimiento'] = NULL;
			endif;

			//print_r($parametros);
			//$temp = $this->procedimientos_model->sp_ins_upd('usp_productores_ins_upd', $parametros);
			$parametros['creado'] = date('Y-m-d H:i:s');
			$parametros['usuario_crea'] = _helper_usuario_dni();
			$parametros['usuario_modifica'] = _helper_usuario_dni();
			$parametros['estado'] = 'transicion';
			$temp = $this->modelo_productores->insertProductor($parametros);

			// $temp = _help_get_message($temp);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo el productor de dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No se pudo crear el productor');

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_1/' . $temp_dni));
		}
		$data['departamentos'] = $this->ubigeo();

		$data['provincias'] = $this->ubigeo(2, 1);
		$data['distritos'] = $this->ubigeo(3, 1, 1);
		$codigo_comunidades = '521';
		$data['comunidades'] = $this->ubigeo_comunidades($codigo_comunidades);
		$data['subcontent'] = $this->load->view('fichas/basica_productor_ficha', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}
	public function basica_productor_seccion_1() {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 1;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);

		//#borrarSP
		//$temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);

		$temp = $this->modelo_productores->getProductor($param);

		/*if ($temp->num_rows() <= 0):
			                                    $this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			                                    redirect(site_url('fichas/basica_productor'));
			                            endif;
		*/
		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;

		// $data['productores'] = $temp->row_array();
		$data['productores'] = $temp;
		$temp = NULL;

		//#borrarsp
		//$temp = $this->procedimientos_model->sp_select('usp_seccion1_por_dni', $param);
		//$data['row'] = $temp->row_array();

		$temp = $this->modelo_productores->getProductorSeccion1($temp_dni);
		$data['row'] = $temp;
		$data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_1', $data, true);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {

			/* guardamos la informacion */

			// recepcionamos los datos del  formulario :)
			//select   sgpteam.usp_productores_ins_upd
			//(0,0,'codigo','42864625','Renee','Morales','calhua','2015-06-07')
			$temp_dni = $parametros['productores_dni'] = $this->input->post('form_dni');
			$parametros['conyuge_nombres'] = strtoupper(_empty($this->input->post('form_conyuge_nombres')));
			$parametros['conyuge_paterno'] = strtoupper(_empty($this->input->post('form_conyuge_paterno')));
			$parametros['conyuge_materno'] = strtoupper(_empty($this->input->post('form_conyuge_materno')));
			$parametros['mayores_mujeres'] = (int) $this->input->post('form_mayores_mujeres');
			$parametros['mayores_hombres'] = (int) $this->input->post('form_mayores_hombres');
			$parametros['menores_mujeres'] = (int) $this->input->post('form_menores_mujeres');
			$parametros['menores_hombres'] = (int) $this->input->post('form_menores_hombres');
			$parametros['estudios'] = $this->input->post('form_estudios');
			$parametros['vivienda_propiedad'] = $this->input->post('form_vivienda_propiedad');
			$parametros['vivienda_material'] = $this->input->post('form_vivienda_material');
			$parametros['vivienda_cocina'] = $this->input->post('form_vivienda_cocina');
			$parametros['servicios_luz'] = $this->input->post('form_servicios_luz');
			$parametros['servicios_agua'] = $this->input->post('form_servicios_agua');
			$parametros['servicios_desague'] = $this->input->post('form_servicios_desague');
			$parametros['servicios_telefono'] = $this->input->post('form_servicios_telefono');
			$parametros['servicios_banio'] = $this->input->post('form_servicios_banio');
			$parametros['control_nutricional'] = $this->input->post('form_control_nutricional');
			$parametros['consume_prod_eco'] = $this->input->post('form_consume_prod_eco');
			$parametros['consume_prod_eco_ubi'] = $this->input->post('form_consume_prod_eco_ubi');

			//$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion1_ins_upd', $parametros);

			$temp = $this->modelo_productores->updateProductorSeccion1($parametros);

			//echo $this->procedimientos_model->query();
			//$temp = _help_get_message($temp);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo el productor de dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'Se registro el productor de dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_2/' . $temp_dni));
		}
		$data['subcontent'] = $this->load->view('fichas/basica_productor_ficha_secciones', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}
	public function basica_productor_seccion_2() {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 2;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param1['dni'] = $param[] = (string) $this->uri->segment(3);
		// $temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);

		$temp = $this->modelo_productores->get_productor($param1);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$temp = $this->procedimientos_model->sp_select('usp_seccion2_por_dni', $param);
		$data['row'] = $temp->row_array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {

			/* guardamos la informacion */

			// recepcionamos los datos del  formulario :)
			//select   sgpteam.usp_productores_ins_upd
			//(0,0,'codigo','42864625','Renee','Morales','calhua','2015-06-07')
			$temp_dni = $parametros[] = $this->input->post('form_dni');
			$parametros[] = $this->input->post('form_practicas_forestales');
			$parametros[] = $this->input->post('form_recoleccion');
			$parametros[] = $this->input->post('form_recoleccion_texto');
			$parametros[] = $this->input->post('form_basura');
			$parametros[] = $this->input->post('form_rastrojos');
			$parametros[] = $this->input->post('form_desperdicios_animales');
			$parametros[] = $this->input->post('form_contaminacion_cerca');
			$parametros[] = $this->input->post('form_contaminacion_cerca_texto');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion2_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			$temp = _help_get_message($temp);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo el productor de dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'Se registro el productor de dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_3/' . $temp_dni));
		}
		$data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_2', $data, true);
		$data['subcontent'] = $this->load->view('fichas/basica_productor_ficha_secciones', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}
	public function basica_productor_seccion_3() {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 3;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		// $temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);

		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$data['row'] = array();

		/**
		 * extraemos informacion de la seccion 3 y lo pasamos ala  vista.
		 */
		$param = array();
		$param[] = $temp_dni;
		$temp = $this->procedimientos_model->sp_select('usp_seccion3_get', $param)->row_array();
		$data['seccion3'] = $temp;

		//  recuperando las parcelas
		$param = array();
		$param[] = $temp_dni;
		$temp = $this->procedimientos_model->sp_select('usp_parcelas_por_dni', $param);
		$form_name = $this->input->post('js_form_name');
		if ($form_name == '_agregar_parcela_1'):
			$this->basica_productor_seccion_3_parcela_1();
		elseif ($form_name == '_agregar_parcela_2'):
			if (!isset($_POST['borrar_parcela_1'])):
				$this->basica_productor_seccion_3_parcela_2();
			else:
				$this->basica_productor_seccion_3_parcela_3();
			endif;
		elseif ($form_name == '_agregar_parcela_4'):
			$this->basica_productor_seccion_3_parcela_4();
		elseif ($form_name == '_accion_guardar_seccion_3'):
			$this->basica_productor_seccion_3_ins_upd();
		endif;
		$data['parcelas'] = $temp->result_array();
		$data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_3', $data, true);
		$data['subcontent'] = $this->load->view('fichas/basica_productor_ficha_secciones', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}
	public function basica_productor_seccion_3_parcela_2() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_nombre', 'Nombre de parcela', 'required');
		$this->form_validation->set_rules('js_form_suelo_tipo', 'Tipo de suelo', 'required');
		$this->form_validation->set_rules('js_form_suelo_color', 'Color de suelo', 'required');
		$this->form_validation->set_rules('js_form_suelo_pedregosidad', 'Pedregocidad', 'required');
		$this->form_validation->set_rules('js_form_terreno_tenencia', 'Tenecia', 'required');
		$this->form_validation->set_rules('js_form_terreno_geografia', 'Geografia', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$tag = $this->input->post('js_form_tag');
			$parcela_id = $parametros[] = $this->input->post('js_form_id');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = $this->input->post('js_form_nombre');
			$parametros[] = $this->input->post('js_form_suelo_tipo');
			$parametros[] = $this->input->post('js_form_suelo_color');
			$parametros[] = $this->input->post('js_form_suelo_pedregosidad');
			$parametros[] = $this->input->post('js_form_terreno_tenencia');
			$parametros[] = $this->input->post('js_form_terreno_geografia');
			$parametros[] = $this->input->post('js_form_puntos');
			$parametros[] = $this->input->post('js_form_area');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_parcelas1_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo la parcela #' . $parcela_id . ' de dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_3/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_3_parcela_4() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('form_riego_secano', 'Riego/secano', 'required');
		$this->form_validation->set_rules('form_agua_fuente', 'Fuente de agua', 'required');
		$this->form_validation->set_rules('form_tipo_riego', 'Tipo de riego', 'required');
		$this->form_validation->set_rules('form_agua_calidad', 'Calidad del agua', 'required');
		$this->form_validation->set_rules('form_agua_disponibilidad', 'Disponibilidad del agua', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$tag = $this->input->post('js_form_tag');
			$parcela_id = $parametros[] = $this->input->post('js_form_id');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = $this->input->post('form_riego_secano');
			$parametros[] = $this->input->post('form_agua_fuente');
			$parametros[] = $this->input->post('form_tipo_riego');
			$parametros[] = $this->input->post('form_agua_calidad');
			$parametros[] = $this->input->post('form_agua_disponibilidad');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_parcelas2_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo la parcela #' . $parcela_id . ' de dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_3/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_3_parcela_3() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$tag = $this->input->post('js_form_tag');
			$parametros = array();
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parcela_id = $parametros[] = $this->input->post('js_form_id');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_parcelas_borrar', $parametros);

			//echo $this->procedimientos_model->query();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se borro la parcela #' . $parcela_id . ' de dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe la parcela:' . $parcela_id);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_3/' . $temp_dni . '#' . $tag));
		}
	}
	public function validar_parcela($str) {

		//$temp=_help_get_message($temp) ;
		$param[] = $this->uri->segment(3);
		$param[] = $str;
		$temp = $this->procedimientos_model->sp_select('usp_parcelas_buscar', $param);

		//$this->form_validation->set_message('validar_parcela','YA  existe una parcela con ese nombre!');
		//$this->form_validation->set_message('validar_parcela', 'The %s field can not be the word "test"');
		return (bool) _help_get_message($temp);
	}
	public function basica_productor_seccion_3_parcela_1() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_nombre', 'Nombre de parcela', 'required|callback_validar_parcela', array('validar_parcela' => 'Ya existe una parcela con el mismo nombre'));
		$this->form_validation->set_rules('js_form_suelo_tipo', 'Tipo de suelo', 'required');
		$this->form_validation->set_rules('js_form_suelo_color', 'Color de suelo', 'required');
		$this->form_validation->set_rules('js_form_suelo_pedregosidad', 'Pedregocidad', 'required');
		$this->form_validation->set_rules('js_form_terreno_tenencia', 'Tenecia', 'required');
		$this->form_validation->set_rules('js_form_terreno_geografia', 'Geografia', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$tag = $this->input->post('js_form_tag');
			$parcela_id = $parametros[] = $this->input->post('js_form_id');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = $this->input->post('js_form_nombre');
			$parametros[] = $this->input->post('js_form_suelo_tipo');
			$parametros[] = $this->input->post('js_form_suelo_color');
			$parametros[] = $this->input->post('js_form_suelo_pedregosidad');
			$parametros[] = $this->input->post('js_form_terreno_tenencia');
			$parametros[] = $this->input->post('js_form_terreno_geografia');
			$parametros[] = $this->input->post('js_form_puntos');
			$parametros[] = $this->input->post('js_form_area');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_parcelas1_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo la parcela #' . $parcela_id . ' de dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_3/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_3_ins_upd() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('form_temperatura', 'Temperatura', 'required');
		$this->form_validation->set_rules('form_altitud', 'altitud', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$tag = $this->input->post('form_tag');
			$temp_dni = $parametros[] = $this->input->post('form_dni');
			$parametros[] = $this->input->post('form_altitud');
			$parametros[] = $this->input->post('form_temperatura');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion3_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente   dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_3/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4() {
		$data = array();
		$data['tab'] = 2;
		$data['subtab'] = 4;

		/*  extraemos informacion del  productor ! */
		$param = array();
		$temp_dni = $param['dni'] = (string) $this->uri->segment(3);
		// $temp = $this->procedimientos_model->sp_select('usp_productores_por_dni', $param);
		$temp = $this->modelo_productores->get_productor($param);

		if (count($temp) <= 0):
			$this->session->set_flashdata('mensajes', 'El  productor(DNI: ' . $temp_dni . ') no existe.');
			redirect(site_url('fichas/basica_productor'));
		endif;
		$data['productores'] = $temp; //->row_array();
		$temp = NULL;
		$param = array();
		$param[] = $temp_dni;
		$data['row'] = $this->procedimientos_model->sp_select('usp_seccion4_get', $param)->row_array();
		$param = array();
		$param[] = $temp_dni;
		$data['cultivos'] = $this->procedimientos_model->sp_select('usp_cultivos_get', $param)->result_array();
		$param = array();
		$param[] = $temp_dni;
		$data['animales'] = $this->procedimientos_model->sp_select('usp_animales_get', $param)->result_array();
		$form_name = $this->input->post('js_form_name');
		if ($form_name == 'panel1') {
			$this->basica_productor_seccion_4_1();
		} elseif ($form_name == 'panel2') {
			$this->basica_productor_seccion_4_2();
		} elseif ($form_name == 'panel3') {
			$this->basica_productor_seccion_4_3();
		} elseif ($form_name == 'panel4') {
			$this->basica_productor_seccion_4_4();
		} elseif ($form_name == 'panel5') {
			$this->basica_productor_seccion_4_5();
		} elseif ($form_name == 'panel6') {
			if (!isset($_POST['borrar_cultivo_1'])):
				$this->basica_productor_seccion_4_6();
			else:
				$this->basica_productor_seccion_4_6_borrar();
			endif;
		} elseif ($form_name == 'panel7') {
			$this->basica_productor_seccion_4_7();
		} elseif ($form_name == 'panel8') {
			if (!isset($_POST['borrar_animales_1'])):
				$this->basica_productor_seccion_4_8();
			else:
				$this->basica_productor_seccion_4_8_borrar();
			endif;
		}
		$data['seccion'] = $this->load->view('fichas/basica_productor_ficha_seccion_4', $data, true);
		$data['subcontent'] = $this->load->view('fichas/basica_productor_ficha_secciones', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}
	public function basica_productor_seccion_4_1() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = $this->input->post('js_form_practicas_curvas');
			$parametros[] = $this->input->post('js_form_practicas_barreras');
			$parametros[] = $this->input->post('js_form_practicas_fertilizantes');
			$parametros[] = $this->input->post('js_form_practicas_terrazas');
			$parametros[] = $this->input->post('js_form_practicas_agroforesteria');
			$parametros[] = $this->input->post('js_form_practicas_rotacion');
			$parametros[] = $this->input->post('js_form_practicas_coberturas');
			$parametros[] = $this->input->post('js_form_practicas_asociacion');
			$parametros[] = $this->input->post('js_form_practicas_majadeo');
			$parametros[] = $this->input->post('js_form_practicas_fertilizantes_texto');
			$parametros[] = $this->input->post('js_form_practicas_otras_texto');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion4_1_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_2() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = _clean($this->input->post('js_form_herramientas_actividad_agricola', true));
			$parametros[] = _clean($this->input->post('js_form_herramientas_actividad_pecuaria', true));
			$parametros[] = _clean($this->input->post('js_form_herramientas_actividad_procesamiento', true));

			//$parametros=$this->security->xss_clean($parametros);
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion4_2_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_3() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = _clean($this->input->post('js_form_insumos_actividad_agricola', true));
			$parametros[] = _clean($this->input->post('js_form_insumos_actividad_pecuaria', true));
			$parametros[] = _clean($this->input->post('js_form_insumos_actividad_procesamiento', true));
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion4_3_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_4() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = ($this->input->post('js_form_cria_animales', true));
			$parametros[] = _clean($this->input->post('js_form_cria_animales_texto', true));
			$parametros[] = ($this->input->post('js_form_cria_animales_distancia', true));
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion4_4_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_5() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = ($this->input->post('js_form_mercado_dirigida', true));
			$parametros[] = _clean($this->input->post('js_form_mercado_dirigida_texto', true));
			$parametros[] = _clean($this->input->post('js_form_mercado_lugar_venta', true));
			$parametros[] = _clean($this->input->post('js_form_mercado_comercializacion', true));
			$parametros[] = "";
			$temp = $this->procedimientos_model->sp_ins_upd('usp_seccion4_5_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro una nueva parcela para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_6() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_cultivo_nombre', 'Nombre del cultivo', 'required|callback_validar_cultivos', array('validar_cultivos' => 'El productor ya registro un cultivo con este nombre.'));
		$this->form_validation->set_rules('js_form_cultivo_plaga', 'Plaga', 'required');
		$this->form_validation->set_rules('js_form_cultivo_metodo', 'Metodo de control', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_id');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = strtoupper(_clean($this->input->post('js_form_cultivo_nombre', true)));
			$parametros[] = _clean($this->input->post('js_form_cultivo_plaga', true));
			$parametros[] = _clean($this->input->post('js_form_cultivo_metodo', true));
			$temp = $this->procedimientos_model->sp_ins_upd('usp_cultivos1_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro un nuevo cultivo para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_6_borrar() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_id', 'ID', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$temp_id = $parametros[] = $this->input->post('js_form_id');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_cultivos_borrar', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se borrar correctamente el cultivo #' . $temp_id . '. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro un nuevo cultivo para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_7() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_cultivo_seleccion', 'seleccion', 'required');
		$this->form_validation->set_rules('js_form_cultivo_empacado', 'Empacado', 'required');
		$this->form_validation->set_rules('js_form_cultivo_almacenamiento', 'Almacenamiento', 'required');
		$this->form_validation->set_rules('js_form_cultivo_transporte', 'Transporte', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_id');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = _clean($this->input->post('js_form_cultivo_seleccion', true));
			$parametros[] = _clean($this->input->post('js_form_cultivo_empacado', true));
			$parametros[] = _clean($this->input->post('js_form_cultivo_almacenamiento', true));
			$parametros[] = _clean($this->input->post('js_form_cultivo_transporte', true));
			$temp = $this->procedimientos_model->sp_ins_upd('usp_cultivos2_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente el cultivo. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se registro un nuevo cultivo para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_8() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_animales_nombres', 'Nombre', 'required|callback_validar_animales', array('validar_animales' => 'El productor ya registro un animal con este nombre.'));
		$this->form_validation->set_rules('js_form_animales_numero', 'Numero', 'required');
		$this->form_validation->set_rules('js_form_animales_manejo', 'Manejo', 'required');
		$this->form_validation->set_rules('js_form_animales_control', 'Metodo de control', 'required');
		$this->form_validation->set_rules('js_form_animales_alimentacion', 'Alimentacion', 'required');
		$this->form_validation->set_rules('js_form_animales_uso', 'Uso', 'required');
		$this->form_validation->set_rules('js_form_animales_area', 'Area', 'required');
		$this->form_validation->set_rules('js_form_animales_techo', 'Techo', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_id');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$parametros[] = _clean($this->input->post('js_form_animales_nombres', true));
			$parametros[] = _clean($this->input->post('js_form_animales_numero', true));
			$parametros[] = _clean($this->input->post('js_form_animales_manejo', true));
			$parametros[] = _clean($this->input->post('js_form_animales_control', true));
			$parametros[] = _clean($this->input->post('js_form_animales_alimentacion', true));
			$parametros[] = _clean($this->input->post('js_form_animales_uso', true));
			$parametros[] = _clean($this->input->post('js_form_animales_area', true));
			$parametros[] = _clean($this->input->post('js_form_animales_techo', true));
			$temp = $this->procedimientos_model->sp_ins_upd('usp_animales_ins_upd', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se actualizo correctamente. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se agrego un nuevo registro para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function basica_productor_seccion_4_8_borrar() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('js_form_dni', 'DNI', 'required');
		$this->form_validation->set_rules('js_form_id', 'id', 'required');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$parametros = array();
			$tag = $this->input->post('js_form_tag');
			$temp_dni = $parametros[] = $this->input->post('js_form_dni');
			$temp_id = $parametros[] = $this->input->post('js_form_id');
			$temp = $this->procedimientos_model->sp_ins_upd('usp_animales_borrar', $parametros);

			//echo $this->procedimientos_model->query();
			// exit();
			$temp = _help_get_message($temp);
			$this->session->set_flashdata('mensaje_zona', $tag);
			if ($temp == 1):

				// UPDATE
				$this->session->set_flashdata('mensaje', 'Se borro el registro. dni:' . $temp_dni);
			elseif ($temp == 3):
				$this->session->set_flashdata('mensaje', 'Se agrego un nuevo registro para el dni:' . $temp_dni);
			else:
				$this->session->set_flashdata('mensaje', 'No existe el  dni:' . $temp_dni);

				// insert

			endif;
			redirect(site_url('fichas/basica_productor_seccion_4/' . $temp_dni . '#' . $tag));
		}
	}
	public function validar_cultivos($str) {

		//$temp=_help_get_message($temp) ;
		if (!isset($_POST['agregar_cultivo_1'])):
			$param[] = $this->uri->segment(3);
			$param[] = $str;
			$temp = $this->procedimientos_model->sp_select('usp_cultivos_buscar', $param);

			//$this->form_validation->set_message('validar_parcela','YA  existe una parcela con ese nombre!');
			//$this->form_validation->set_message('validar_parcela', 'The %s field can not be the word "test"');
			return (bool) _help_get_message($temp);
		else:
			return true;
		endif;
	}
	public function validar_animales($str) {

		//$temp=_help_get_message($temp) ;
		if (!isset($_POST['agregar_animales_1'])):
			$param[] = $this->uri->segment(3);
			$param[] = $str;
			$temp = $this->procedimientos_model->sp_select('usp_animales_buscar', $param);

			//$this->form_validation->set_message('validar_parcela','YA  existe una parcela con ese nombre!');
			//$this->form_validation->set_message('validar_parcela', 'The %s field can not be the word "test"');
			return (bool) _help_get_message($temp);
		else:
			return true;
		endif;
	}
	public function basica_productor_listar() {
		$data = array();
		$data['tab'] = 3;
		$data['html'] = ' class="form-control" ';
		$p = array();

		//#borrarsp
		//$data['rows'] = $this->procedimientos_model->sp_select('usp_productores_get', $param)->result_array();
		//$p['consejos_regionales_id'] = _concejo_regional();

		$p = array_keys(_helper_usuario_concejos_regionales());

		$data['rows'] = $this->modelo_productores->get_productores_por_concejos_regionales($p);

		$data['subcontent'] = $this->load->view('fichas/basica_productor_listar', $data, true);
		$data['content'] = $this->load->view('fichas/basica_productor', $data, true);
		$this->load->view('template', $data);
	}
}

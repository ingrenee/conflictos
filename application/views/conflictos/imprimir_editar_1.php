<h4>Actor que reporta el conflicto -  la denuncia</h4>
<div class="form-group">
	<label class="control-label col-sm-3 col-md-3" for="email">Nombres y apellidos:</label>
	<div class="col-sm-9 col-md-9">
		<p><?php _vi($row, 'actor_reporta_nombre');?></p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">DNI:</label>
	<div class="col-sm-4">
		<p><?php _vi($row, 'actor_reporta_dni');?></p>
	</div>
	<label class="control-label col-sm-2" for="email">Tipo de actor:</label>
	<div class="col-sm-3">
		<p><?php echo $row['actor_reporta_tipo']; ?></p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="email">Observación:</label>
	<div class="col-sm-9">
		<p><?php _vi($row, 'actor_reporta_observacion');?></p>
	</div>
</div>
<h4>Otras fuentes de información</h4>
<div id="otras_fuentes_content">
	<?php if (count($otras) > 0): ?>
	<?php foreach ($otras as $kk => $vv):
?>
	<div id="otras_fuentes" class="otras_fuentes">
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Nombres y apellidos:</label>
			<div class="col-sm-9">
				<p><?php _vi($vv, 'nombres');?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Tipo:</label>
			<div class="col-sm-9">
				<p>(<?php echo $vv['tipo']; ?>)

				 <?php _vi($vv, 'descripcion');?></p>
			</div>
		</div>
		<?php if (!empty($vv['documento'])): ?>
		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Documento Adjunto:</label>
			<div class="col-sm-9">
				<p class="form-control-static">
			  <?php echo $vv['documento']; ?>
				</p>
			</div></div>
			<?php else: ?>

			<?php endif;?>

			<hr>
		</div>
		<?php endforeach;?>
		<?php else: ?>

		<?php endif?>
	</div>

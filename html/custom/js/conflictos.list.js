 $(document).ready(function () {
            var url = "../sampledata/data.php";
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'firstname' },
                    { name: 'lastname' },
                    { name: 'productname'},
                    { name: 'quantity', type: 'int' },
                    { name: 'price', type: 'float' },
                    { name: 'total', type: 'float' }
                ],
                id: 'id',
                url: url,
                root: 'data'
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
                width: 850,
                source: dataAdapter,
                columnsresize: true,
                columns: [
                  { text: 'First Name', dataField: 'firstname', width: 200 },
                  { text: 'Last Name', dataField: 'lastname', width: 200 },
                  { text: 'Product', dataField: 'productname', width: 180 },
                  { text: 'Quantity', dataField: 'quantity', width: 80, cellsalign: 'right' },
                  { text: 'Unit Price', dataField: 'price', width: 90, cellsalign: 'right', cellsformat: 'c2' },
                  { text: 'Total', dataField: 'total', cellsalign: 'right', minwidth: 100, cellsformat: 'c2' }
                ]
            });
        });
<div class="panel panel-default">
	<div class="panel-heading">
		Editar Grupo  : <label> <?php
echo $row['name']; ?> </label>
	</div>
	<div class="panel-body">
		<?php
_help_mensajes();?>
		<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
		<form method="post" class="_form_validate">
			<input type="hidden" class="form-control" name="id" value="<?php
echo $id; ?>">
			<div class="form-group">

				<div class="row">
					<div class="col-sm-4"> Nombre del grupo: </div>
					<div class="col-sm-4"> <input class="form-control"
					name="name"  data-rule-required="true" value="<?php
echo $row['name']; ?>"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Descripción: </div>
					<div class="col-sm-4">  <textarea class="form-control"  data-rule-required="true" name="description"><?php echo $row['description']; ?></textarea> </div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">   <button type="submit"  class="btn btn-primary"> Actualizar  </button> </div>
			</div>
		</form>
	</div>
</div>
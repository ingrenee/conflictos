

 <div class="panel panel-info">
 	<div class="panel-heading">
 		<h3 class="panel-title">Grupos registrados</h3>
 	</div>
 	<div class="panel-body">

<?php _help_mensajes();?>
<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
<table class="table table-condensed table-striped">
	<thead>
		<tr> <th> #id </th>
		<!-- <td> Nombre de usuario  </td> -->
		<td> Nombre del grupo </td>
		<td> Descripcion </td>

		<td>   </td>
	</tr>
</thead>
<tbody>
	<?php
foreach ($rows as $k => $v):
?>
	<tr> <td> <?php echo $v['id']; ?> </td>

	<td> <?php echo $v['name']; ?>  </td>
	<td> <?php echo $v['description']; ?>  </td>

	<td> <a href="<?php echo site_url('grupos/editar/' . $v['id']); ?>" class="btn btn-primary btn-xs"> <?php echo _helper_icono('pencil'); ?> Editar </a> </td>

</tr>
<?php
endforeach;
?>
</tbody>
</table>


 	</div>
 </div>
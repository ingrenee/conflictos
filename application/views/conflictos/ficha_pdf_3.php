<h4>Actores involucrados</h4>
<div class="row">
	<label class=" col-sm-12" for="email">Actores Primarios: </label>
	<div class="col-sm-12">
		<table border=1>
			<tr>
				<th>Posiciones</th>
				<th>Interes</th>
				<th>Valores</th>
				<th>Necesidades</th>
			</tr>
			<?php if (count($primarios) > 0): ?>
			<?php foreach ($primarios as $k => $v): ?>
			<tr>
				<td><?php echo $v['posiciones']; ?></td>
				<td><?php echo $v['intereses']; ?></td>
				<td><?php echo $v['valores']; ?></td>
				<td><?php echo $v['necesidades']; ?></td>
			</tr>
			<?php endforeach?>
			<?php else: ?>
			<?php endif?>
		</table>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<label class="   " for="email">Actores Secundarios: </label>
		<p><?php _vi($row, 'actores_2');?></p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<label class="   " for="email">Actores Terciarios: </label>
		<p><?php _vi($row, 'actores_3');?></p>
	</div>
</div>



<h1 class="titular"> Roles del usuario </h1>

<div class="row" style="margin-bottom:20px">
	<div class="col-md-12">

		<ul class="nav nav-tabs">
			<li role="presentation" class="<?php echo ($tab == 1) ? 'active' : ''; ?>">
				<a href="<?php echo site_url('usuarios/index'); ?>">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					Agregar nuevo usuario
				</a>
			</li>
			<li role="presentation" class="<?php echo ($tab == 2) ? 'active' : ''; ?>"  >
				<a href=" <?php echo site_url('usuarios/editar'); ?>">
					<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
					Editar  usuario
				</a>
			</li>
			<li role="presentation" class="<?php echo ($tab == 3) ? 'active' : ''; ?>">
				<a href="<?php echo site_url('usuarios/listar'); ?>">
					<span class="glyphicon glyphicon-list" aria-hidden="true"></span>	Listar
					usuarios
				</a>
			</li>

		</ul>
	</div>


</div>


<?php echo $subcontent; ?>


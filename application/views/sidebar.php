   <div class="panel-a panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                            </span>Conflictos</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="<?php echo site_url('conflictos/index'); ?>">Registrar nuevo</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="<?php echo site_url('conflictos/list_editar'); ?>">Editar/modificar </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-file text-info"></span><a href="<?php echo site_url('monitoreo/listar_editar'); ?>">Monitoreo</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-comment text-success"></span><a href="<?php echo site_url('evaluacion/listar_editar'); ?>">Evaluacion</a>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                            </span>Usuarios</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">


                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("usuarios/listar"); ?>">Listar usuarios </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url('usuarios/index'); ?>">Nuevo usuario</a>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                            </span>Reportes</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list"></span>
                                        <a href="<?php echo site_url('reportes/conflictos_por_estado'); ?>">Conflictos por estado</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list"></span><a href="<?php echo site_url('reportes/conflictos_por_tipo'); ?>">Conflictos por tipo</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list"></span><a href="<?php echo site_url('reportes/acciones_por_conflicto'); ?>">Acciones por conflicto</a>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

                                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour1"><span class="glyphicon glyphicon-file">
                            </span>Configuración</a>
                        </h4>
                    </div>
                    <div id="collapseFour1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php echo site_url("grupos/listar"); ?>">Grupos</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php
echo site_url('estados/listar'); ?>">Estados</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php echo site_url('tipos/listar'); ?>">Tipos</a>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
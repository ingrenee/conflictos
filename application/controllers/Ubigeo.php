<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubigeo extends CI_Controller {

	public function provincias() {
		$w['tipo'] = 'provincia';
		$w['departamentos_id'] = $this->input->post('id');

		$rows = $this->db->select('DISTINCT   (provincias_id),id,nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();

		$cad = '<option> Seleccione </option>';
		foreach ($rows as $k => $v) {
			$cad .= '<option value="' . $v['id'] . '">' . $v['nombre'] . '</option>';
		}

		echo $cad;
	}

	public function distritos() {
		$w['tipo'] = 'distrito';
		$w['provincias_id'] = $this->input->post('id');

		$rows = $this->db->select('DISTINCT   (distritos_id),id,nombre')->where($w)->get('conflictos.ubigeo2006')->result_array();

		$cad = '<option> Seleccione </option>';
		foreach ($rows as $k => $v) {
			$cad .= '<option value="' . $v['id'] . '">' . $v['nombre'] . '</option>';
		}

		echo $cad;
	}

}

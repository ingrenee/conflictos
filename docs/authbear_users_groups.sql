/*
Navicat PGSQL Data Transfer

Source Server         : pg
Source Server Version : 90217
Source Host           : localhost:5434
Source Database       : siarayacucho
Source Schema         : conflictos

Target Server Type    : PGSQL
Target Server Version : 90217
File Encoding         : 65001

Date: 2016-09-21 10:32:18
*/


-- ----------------------------
-- Table structure for "conflictos"."authbear_users_groups"
-- ----------------------------

CREATE TABLE "conflictos"."authbear_users_groups" (
"id" int4 DEFAULT nextval('"conflictos".users_groups_id_seq'::regclass) NOT NULL,
"authbear_users_id" int4 NOT NULL,
"authbear_groups_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of authbear_users_groups
-- ----------------------------
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('1', '1', '1');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('2', '1', '2');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('4', '5', '1');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('5', '5', '2');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('6', '4', '2');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('7', '4', '1');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('8', '4', '3');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('9', '1', '3');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('10', '6', '3');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('11', '7', '2');
INSERT INTO "conflictos"."authbear_users_groups" VALUES ('12', '9', '3');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Uniques structure for table "conflictos"."authbear_users_groups"
-- ----------------------------
ALTER TABLE "conflictos"."authbear_users_groups" ADD UNIQUE ("authbear_users_id", "authbear_groups_id");

-- ----------------------------
-- Checks structure for table "conflictos"."authbear_users_groups"
-- ----------------------------
ALTER TABLE "conflictos"."authbear_users_groups" ADD CHECK (authbear_users_id >= 0);
ALTER TABLE "conflictos"."authbear_users_groups" ADD CHECK (authbear_groups_id >= 0);
ALTER TABLE "conflictos"."authbear_users_groups" ADD CHECK (id >= 0);

-- ----------------------------
-- Primary Key structure for table "conflictos"."authbear_users_groups"
-- ----------------------------
ALTER TABLE "conflictos"."authbear_users_groups" ADD PRIMARY KEY ("id");

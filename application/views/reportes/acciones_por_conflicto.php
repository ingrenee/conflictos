<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Acciones por conflicto</h3>
	</div>
	<div class="panel-body">
       <div id="toolbar" class=" ">
          <select class="form-control">

             <option value="">Exportar</option>
             <option value="all">Exportar todo</option>
             <option value="selected">Exportar selección</option>
          </select>
       </div>

 <table id="table" data-toggle="table" data-url="<?php echo site_url('conflictos/listar_acciones_por_conflicto'); ?>"

			data-side-pagination="server"
               data-pagination="true"
               data-page-list="[5, 10, 20, 50, 100, 200]"
               data-search="true"

           data-filter-show-clear="true"
           data-filter-control="true"


           data-show-export="true"

           data-toolbar="#toolbar"


 >
    <thead>
        <tr>
            <th data-field="id" data-sortable="true">ID</th>
            <th data-field="fecha" data-sortable="true" data-filter-control="datepicker">Fecha</th>
            <th data-field="codigo" data-sortable="true" data-filter-control="input">C&oacute;digo</th>
            <th data-field="nombre" data-sortable="true" data-filter-control="input">Nombre del conflicto</th>
            <th data-field="departamento" data-sortable="true"  data-filter-control="input">Depart</th>
            <th data-field="provincia" data-sortable="true"  data-filter-control="input">Provincia</th>
            <th data-field="distrito" data-tableexport-value="export title" data-sortable="true"
              data-filter-control="input">Distrito</th>
            <th data-field="total"  data-sortable="true"  data-filter-control="input" >Total Acciones</th>

            <th data-field="id"  data-formatter="accionesFormatter">Acciones</th>
        </tr>
    </thead>
</table>



	</div>
</div>
 <script type="text/javascript">

 	function accionesFormatter(value, row) {
       // var icon = row.id % 2 === 0 ? 'glyphicon-star' : 'glyphicon-star-empty'
       var icon='glyphicon-signal';
       var icon2='glyphicon glyphicon-print';
link= _helper_site_url('reportes/mostrar_grafico_acciones/'+row.id);
link2= _helper_site_url('conflictos/imprimir/'+row.id);
       return '<a href="#" data-conflicto-id="'+row.id+'" data-toggle="modal" data-target="#modal_grafico_acciones" class="btn btn-primary  btn-xs"><i class="glyphicon ' + icon + '"></i>  Gráfico </a> ';
    }


    var $table = $('#table');
    $(function () {
    });

 </script>


 <div class="modal fade" id="modal_grafico_acciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Historial de acciones por conflicto</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $('#modal_grafico_acciones').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('conflicto-id') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  //

  var modal = $(this)
link= _helper_site_url('reportes/mostrar_grafico_acciones/'+id);
  $.ajax({
      url: link,
      type: 'post',
      data: {},
      success: function (data) {

modal.find('.modal-body').html(data);
      }
    });

})


</script>


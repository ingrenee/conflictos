<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Modulo de Conflictos Socioambientales</title>
<script type="text/javascript">

var  _site_url="<?php echo site_url(); ?>";

</script>
<script type="text/javascript" src="<?php echo base_url('html/jquery/dist/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('html/jquery.md5.js'); ?>"></script>


<script type="text/javascript" src="<?php echo base_url('html/jquery.PrintArea.js_4.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('html/core.print.js'); ?>"></script>


<script type="text/javascript" src="<?php echo base_url('html/moment/min/moment-with-locales.min.js'); ?>"></script>


<script type="text/javascript" src="<?php echo base_url('html/bootstrap/dist/js/bootstrap.min.js'); ?>" data-home=""></script>
<script type="text/javascript" src="<?php echo base_url('html/jquery-validation/dist/jquery.validate.min.js'); ?>"data-home=""></script>


<script type="text/javascript" src="<?php echo base_url('html/jquery-validation/src/localization/messages_es_PE.js'); ?>"data-home=""></script>


<link rel="shortcut icon"   href="<?php echo base_url('favicon.ico'); ?>"/>



	  <link rel="stylesheet" type="text/css" href="<?php echo base_url('html/custom/bootstrap.min.css'); ?>" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('html/custom/bootstrap.min.css'); ?>" media="print">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('html/custom/print.css'); ?>" media="print">

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url('html/bootstrap-datepicker/dist/css/bootstrap-datepicker.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('html/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>" data-home=""></script>
 <script type="text/javascript">

$.fn.datepicker.defaults.format = "yyyy/mm/dd";

 </script>
<link rel="stylesheet" href="<?php echo base_url('html/bootstrap-table/dist/bootstrap-table.min.css'); ?>">

<script type="text/javascript" src="<?php echo base_url('html/bootstrap-table/dist/bootstrap-table.min.js'); ?>" data-home=""></script>


<script type="text/javascript" src="<?php echo base_url('html/bootstrap-table/dist/locale/bootstrap-table-es-ES.js'); ?>" data-home=""></script>

<script type="text/javascript" src="<?php echo base_url('html/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.js'); ?>" data-home=""></script>

<script type="text/javascript"
src="<?php echo base_url('html/bootstrap-table/dist/extensions/export/bootstrap-table-export.js'); ?>"></script>


<script src="<?php echo base_url('html/bootstrap-table/dist/extensions/export/tableExport.js'); ?>">


</script>

<script src="<?php echo base_url('html/custom/code/highcharts.js'); ?>"></script>
	<script src="<?php echo base_url('html/jquery-highchartTable-plugin-master/jquery.highchartTable.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('html\jsPDF\dist'); ?>/jspdf.min.js"></script>
<script type="text/javascript" src="http://cdn.uriit.ru/jsPDF/libs/adler32cs.js/adler32cs.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2014-11-29/FileSaver.min.js
"></script>

<script type="text/javascript" src="<?php echo base_url('html\jsPDF\plugins/addimage.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('html\jsPDF\plugins'); ?>/standard_fonts_metrics.js"></script>
<script type="text/javascript" src="<?php echo base_url('html\jsPDF\plugins'); ?>/split_text_to_size.js"></script>
<script type="text/javascript" src="<?php echo base_url('html\jsPDF\plugins'); ?>/from_html.js"></script>


<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="http://canvg.github.io/canvg/rgbcolor.js"></script>
<script type="text/javascript" src="http://canvg.github.io/canvg/StackBlur.js"></script>
<script type="text/javascript" src="http://canvg.github.io/canvg/canvg.js"></script>

 <?php

_help_jqxwidget();

?>

<script type="text/javascript" src="<?php echo base_url('html/custom/js/clonar.js'); ?>?<?php echo rand(99, 999999999999); ?>" data-home=""></script>

<script type="text/javascript" src="<?php echo base_url('html/custom/js/main.js'); ?>?<?php echo rand(99, 999999999999); ?>" data-home=""></script>

 <script type="text/javascript">
 	jQuery.validator.addMethod("validDate", function(value, element) {

        return this.optional(element) || moment(value,"DD/MM/YYYY").isValid();
    }, "Please enter a valid date in the format DD/MM/YYYY");


 </script>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('html/site/site.css'); ?>">

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

.panel-a  .glyphicon { margin-right:10px; }
.panel-a .panel-body { padding:0px; }
.panel-a  .panel-body table tr td { padding-left: 15px }
.panel-a  .panel-body .table {margin-bottom: 0px; }

label.error
{
	color:red;
}

.otras_fuentes:first-child a.boton_quitar
{
	display: none;
}
	</style>

 <script type="text/javascript">

 	jQuery(function(){

jQuery('.fecha').datepicker({format:"dd/mm/yyyy"});

		jQuery('#form_agregar_conflicto').validate();
		jQuery('._form_validate').validate();

		jQuery('#boton_form_1').click(function(e){

			e.preventDefault();

			if(jQuery('#form_1').validate().form())
			{
				jQuery('#form_1').hide();
				jQuery('#form_2').show();
			}
		});


		jQuery('#boton_form_2').click(function(e){

			e.preventDefault();

			if(jQuery('#form_2').validate().form())
			{
				jQuery('#form_2').hide();
				jQuery('#form_3').show();
			}
		});



		jQuery('#boton_form_2_back').click(function(e){

			e.preventDefault();


				jQuery('#form_2').hide();
				jQuery('#form_3').hide();
				jQuery('#form_1').show();

		});

			jQuery('#boton_form_3_back').click(function(e){

			e.preventDefault();


				jQuery('#form_2').show();
				jQuery('#form_3').hide();
				jQuery('#form_1').hide();

		});

 	});
 </script>
</head>
<body>
 <div class="container-fluid">
<?php
$this->load->view('menu');
?>

 <div class="row">

 	<div class="col-sm-2">

<?php
$this->load->view('sidebar');
?>

 		</div>
 	<div class="col-sm-10">

<?php echo $content; ?>

 	</div>
 </div>

 </div>

  <div class="container-fluid">


  	<div class="row">

  		<div class="col-sm-12 text-center">

<img src="<?php echo base_url('html/img/logo_CTB_3.jpg'); ?>">
Está interface ha sido desarrollada con el apoyo del

<a href="http://prodern.minam.gob.pe/">Programa de Desarrollo Estratégico de los Recursos Naturales PRODERN II</a>



 y la

<a href="http://www.btcctb.org/">Agencia Belga para el Desarrollo.</a>



  		</div>
  	</div>
  </div>

</body>
</html>
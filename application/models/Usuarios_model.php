<?php
class Usuarios_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function __construct() {

		// Call the CI_Model constructor
		parent::__construct();
	}

	public function listing() {

		$rows = $this->db->select("*")->get('conflictos.authbear_users');
		return $rows;
	}

	public function listing_groups() {

		return $this->db->select('*')->get('conflictos.authbear_groups');
	}

	public function listing_rol() {

		return $this->db->select('*')->get('conflictos.authbear_rol');
	}

	/*SELECT * FROM sgpteam.authbear_users_groups where authbear_users_id=p_id*/
//insert  into sgpteam.authbear_users_groups (authbear_groups_id, authbear_users_id)  values(p_groups_id,p_users_id);
	//

	public function adding_users_groups($authbear_users_id, $authbear_groups_id) {
		$w['authbear_users_id'] = $authbear_users_id;
		$w['authbear_groups_id'] = $authbear_groups_id;

		$this->db->where($w)->delete('conflictos.authbear_users_groups');

		$this->db->insert('conflictos.authbear_users_groups', $w);

	}

	public function adding_users_rol($authbear_users_id, $authbear_rol_id) {
		$w['authbear_users_id'] = $authbear_users_id;
		$w['authbear_rol_id'] = $authbear_rol_id;

		$this->db->where($w)->delete('conflictos.authbear_users_rol');

		$this->db->insert('conflictos.authbear_users_rol', $w);

	}

	public function deleting_users_rol($id) {
		$this->db->select('*')->where('id', $id)->limit(1)->delete('conflictos.authbear_users_rol');
	}

	public function deleting_users_groups($id) {
		$this->db->select('*')->where('id', $id)->limit(1)->delete('conflictos.authbear_users_groups');
	}
	public function listing_users_groups($id) {

		return $this->db->select('*')->where('authbear_users_id', $id)->get('conflictos.authbear_users_groups');
	}

	public function listing_users_rol($id) {

		return $this->db->select('*')->where('authbear_users_id', $id)->get('conflictos.authbear_users_rol');
	}
	public function get_usuarios_por_id($id) {

		$rows = $this->db->select('*')->where('id', $id)->get('conflictos.authbear_users')->row_array();

		return $rows;
	}

	public function get_usuarios_por_dni($dni) {

		$rows = $this->db->select('*')->where('dni', $dni)->get('conflictos.authbear_users')->row_array();

		return $rows;
	}

	public function get_verifica_dni($str, $id) {

		$rows = $this->db->select('dni')->where_not_in('id', $id)->where('dni', $str)->get('conflictos.authbear_users')->num_rows();

		return $rows;
	}
	public function get_verifica_email($str, $id) {

		$rows = $this->db->select('email')->where_not_in('id', $id)->where('email', $str)->get('conflictos.authbear_users')->num_rows();

		return $rows;
	}
	public function set_usuarios_estado($str = 1, $id = 99999) {
		$str = ($str == 1) ? 1 : 2;

		return $this->update_usuarios(array('active' => $str), $id);
	}
	public function get_verifica_username($str, $id) {

		$rows = $this->db->select('dni')->where_not_in('id', $id)->where('username', $str)->get('conflictos.authbear_users')->num_rows();

		return $rows;
	}

	function insert_usuarios($p = array()) {

		$this->db->insert('conflictos.authbear_users', $p);
		return $this->db->insert_id();

	}

	public function update_usuarios($p = array(), $id) {

		$this->db->where('id', $id)->update('conflictos.authbear_users', $p);
		return 1;
	}

}

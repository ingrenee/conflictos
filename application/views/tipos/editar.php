<div class="panel panel-default">
	<div class="panel-heading">
		Editar tipo  : <label> <?php
echo $row['nombre']; ?> </label>
	</div>
	<div class="panel-body">
		<?php
_help_mensajes();?>
		<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
		<form method="post" method="post"  class="_form_validate">
			<input type="hidden" class="form-control" name="id" value="<?php
echo $id; ?>">
			<div class="form-group">

				<div class="row">
					<div class="col-sm-4"> Nombre: </div>
					<div class="col-sm-4"> <input type="text"  data-rule-required="true"  class="form-control"
					name="nombre" value="<?php
echo $row['nombre']; ?>"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Descripción: </div>
					<div class="col-sm-4">  <textarea class="form-control" data-rule-required="true" name="descripcion"><?php echo $row['descripcion']; ?></textarea> </div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">   <button type="submit"  class="btn btn-primary"> Actualizar  </button> </div>
			</div>
		</form>
	</div>
</div>
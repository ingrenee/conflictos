<div class="panel panel-default">
	<div class="panel-heading">
		Nuevo usuario
	</div>
	<div class="panel-body">
		<?php _help_mensajes();?>
		<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
		<form method="post">
			<!--
			<div class="form-group">
								<div class="row">
													<div class="form-group">
																		<div class="col-sm-4"> Nombre usuario: </div>
																		<div class="col-sm-4"> <input class="form-control" name="username"> </div>
													</div>
								</div>
			</div>
			-->
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Nombres: </div>
					<div class="col-sm-4"> <input class="form-control" name="nombres" value=""> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Cargo: </div>
					<div class="col-sm-4"> <input   class="form-control" name="cargo"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Institución: </div>
					<div class="col-sm-4"> <input  class="form-control" name="institucion"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Email: </div>
					<div class="col-sm-4"> <input class="form-control " type="email"  name="email" value=""> </div>
				</div>

				</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Dni: </div>
					<div class="col-sm-4"> <input class="form-control numero numeros" maxlength="8" name="dni" value=""> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Clave: </div>
					<div class="col-sm-4"> <input class="form-control" type="password" name="password"> </div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4"> Estado </div>
					<div class="col-sm-4">
						<?php $activo[] = 'seleccione';
$activo[] = 'Activo';
$activo[] = 'Bloqueado';
?>
						<?php echo form_dropdown('active', $activo); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">   <button type="submit"  class="btn btn-primary"> Agregar </button> </div>
			</div>
		</form>
	</div>
</div>
</div>
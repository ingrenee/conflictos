

 <div class="panel panel-info">
 	<div class="panel-heading">
 		<h3 class="panel-title">Estados registrados</h3>
 	</div>
 	<div class="panel-body">

<?php _help_mensajes();?>
<?php
echo validation_errors('<div class="sgp error label label-danger  ">', '</div>'); ?>
<table class="table table-condensed table-striped">
	<thead>
		<tr> <th> #id </th>
		<!-- <td> Nombre de usuario  </td> -->
		<th> Nombre  </th>
		<th> Descripción </th>
<th> Estado</th>
		<td>   </td>
	</tr>
</thead>
<tbody>
	<?php
foreach ($rows as $k => $v):
?>
	<tr> <td> <?php echo $v['id']; ?> </td>

	<td> <?php echo $v['nombre']; ?>  </td>
	<td> <?php echo $v['descripcion']; ?>  </td>
		<td>

		<?php echo _helper_status($v['status']); ?>

<a class=" pull-right btn btn-primary btn-xs" href="<?php echo site_url('estados/change_status/' . $v['id'] . '/' . (int) $v['status']); ?>">Cambiar</a>
		 </td>

	<td> <a href="<?php echo site_url('estados/editar/' . $v['id']); ?>" class="btn btn-primary btn-xs"> <?php echo _helper_icono('pencil'); ?> Editar </a> </td>

</tr>
<?php
endforeach;
?>
</tbody>
</table>


 	</div>
 </div>
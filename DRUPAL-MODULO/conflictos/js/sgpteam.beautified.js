var _site_url_ajax_ubigeo = sgpteam_url_data + "/fichas/json_ubigeo";

var _site_url_ajax_productores = sgpteam_url_data + "/fichas/json_productores";

var _site_url_iframe = sgpteam_url_data + "/m/s_index?";

var _url_tabs_parcelas = sgpteam_url_data + "/fichas/json_productor_parcelas";

var _url_modal_productor_parcela = sgpteam_url_data + "/fichas/json_productor_parcelas_info";

/*
    _site_url_ajax_ubigeo='https://pure-gorge-5085.herokuapp.com/index.php/fichas/json_ubigeo';
    _site_url_ajax_productores='https://pure-gorge-5085.herokuapp.com/index.php/fichas/json_productores';
    _site_url_iframe="https://pure-gorge-5085.herokuapp.com/index.php/m/s_index?";
*/
/* 
  
*/
var departamentos_id = sgpteam_id_departamento;

function modal_init() {
    jQuery("#modal-parcelas").fadeOut("300", function() {});
}

function loadIframe(t) {
    jQuery("#sgp-iframe").attr("src", _site_url_iframe + "points=" + t);
}

function mas_informacion() {
    jQuery(".sgp .mas-informacion").click(function(event) {
        /* obtenemos datos de la parcela seleccionada actualmente. */
        var obj = jQuery(".sgp #tabs-content ul li.active a");
        console.log(obj);
        var dni = obj.attr("data-dni");
        var parcelas_id = obj.attr("data-parcela-id");
        var periodos_id = obj.attr("data-periodos-id");
        if (typeof dni === "undefined") {
            alert("El productor no ha registrado parcelas.");
            return 0;
        }
        console.log("dni" + dni);
        console.log("parcelas_id" + parcelas_id);
        console.log("periodos_id=" + periodos_id);
        jQuery.ajax({
            url: _url_modal_productor_parcela,
            type: "GET",
            dataType: "jsonp",
            data: {
                dni: dni,
                parcelas_id: parcelas_id,
                periodos_id: periodos_id
            }
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template-parcelas-modal").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx = data;
            html = tmpl(ctx);
            console.log(data);
            console.log(html);
            jQuery(".sgp #modal-parcelas").html(html);
            jQuery("#modal-parcelas").fadeIn(200, function() {});
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    });
}

/*****************************************************************/
function loadParcelas(dni) {
    $.ajax({
        url: _url_tabs_parcelas,
        type: "GET",
        dataType: "jsonp",
        data: {
            dni: dni
        }
    }).done(function(data, textStatus, jqXHR) {
        var stemplate = jQuery("#template-tabs-parcelas").html();
        var tmpl = Handlebars.compile(stemplate);
        var ctx = {};
        ctx.rows = data;
        html = tmpl(ctx);
        console.log(data);
        console.log(html);
        jQuery(".sgp #tabs #tabs-content").html(html);
        jQuery(".sgp #tabs #tabs-content").find("li:first").addClass("active");
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}

/*****************************************************************/
jQuery(function() {
    if (typeof jQuery.easing[jQuery.easing.def] == "function") {
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    }
    modal_init();
    mas_informacion();
    /****************************************************************************/
    sgpteam_nav();
    /****************************************************************************/
    /* Cada ves que  se hace click sobre un productor*/
    sgpteam_productor();
    /****************************************************************************/
    /* Cada vez que se hace click sobre  una parcela */
    sgpteam_parcela();
    /****************************************************************************/
    jQuery("body").on("click", "#modal-info-parcelas-content .cerrar", function() {
        jQuery("#modal-parcelas").fadeOut("300", function() {});
    });
    sgpteam_provincias_cambiar();
    sgpteam_distritos_cambiar();
    sgpteam_buscar();
    sgpteam_productores_load_init();
    sgpteam_ubigeo_load_init();
    jQuery("#sgp-iframe").attr("src", _site_url_iframe);
});

function sgpteam_ubigeo_load_init() {
    jQuery.ajax({
        // En data puedes utilizar un objeto JSON, un array o un query string
        data: {
            tipo: 2,
            a: departamentos_id,
            b: 0,
            c: 0
        },
        //Cambiar a type: POST si necesario
        type: "post",
        // Formato de datos que se espera en la respuesta
        dataType: "jsonp",
        // URL a la que se enviará la solicitud Ajax
        url: _site_url_ajax_ubigeo
    }).done(function(data, textStatus, jqXHR) {
        var stemplate = jQuery("#template").html();
        var tmpl = Handlebars.compile(stemplate);
        var ctx = {};
        ctx.rows = data;
        html = tmpl(ctx);
        jQuery("#form_provincias_id").html(html);
        jQuery("#form_distritos_id").html('<option value=""> Seleccione </option>');
        if (console && console.log) {
            console.log("La solicitud se ha completado correctamente.");
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function sgpteam_productores_load_init() {
    jQuery("#sgp-listado-productores").html("cargando...");
    jQuery.ajax({
        // En data puedes utilizar un objeto JSON, un array o un query string
        data: {
            departamentos_id: departamentos_id,
            com: ""
        },
        //Cambiar a type: POST si necesario
        type: "post",
        // Formato de datos que se espera en la respuesta
        dataType: "jsonp",
        // URL a la que se enviará la solicitud Ajax
        url: _site_url_ajax_productores
    }).done(function(data, textStatus, jqXHR) {
        var stemplate = jQuery("#template2").html();
        var tmpl = Handlebars.compile(stemplate);
        var ctx = {};
        ctx.rows = data;
        html = tmpl(ctx);
        jQuery("#sgp-listado-productores").html(html);
        if (console && console.log) {
            console.log("La solicitud se ha completado correctamente.");
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function sgpteam_parcela() {
    jQuery("body").on("click", "#tabs-content li a", function() {
        jQuery(this).closest("ul").find("li").removeClass("active");
        jQuery(this).closest("li").addClass("active");
        t = jQuery(this).attr("data-puntos");
        jQuery("#sgp-iframe").attr("src", _site_url_iframe + "points=" + t);
    });
}

function sgpteam_productor() {
    jQuery("body").on("click", ".sgp-productor", function() {
        jQuery(".sgp-productor").removeClass("sgp-active");
        jQuery(this).toggleClass("sgp-active");
        t = jQuery(this).attr("data-puntos");
        dni = jQuery(this).attr("data-dni");
        jQuery("#sgp-iframe").attr("src", _site_url_iframe + "points=" + t);
        loadParcelas(dni);
    });
}

function sgpteam_nav() {
    jQuery(".sgp-nav ul li a").click(function(event) {
        event.preventDefault();
        jQuery(".sgp-nav ul li a").removeClass("active");
        jQuery(this).addClass("active");
        jQuery(".sgp-tab").hide();
        jQuery(jQuery(this).attr("href")).show();
    });
}

function sgpteam_provincias_cambiar() {
    jQuery("#form_provincias_id").change(function(event) {
        _valor_departamento_id = departamentos_id;
        _valor = jQuery(this).val();
        /*
 jQuery.post(_site_url_ajax_ubigeo, { tipo: 3,a:_valor_departamento_id,b:_valor,c:0 }, function(data){
            
 var stemplate = jQuery("#template").html();
 var tmpl = Handlebars.compile(stemplate);
 var ctx = {};
 ctx.rows=data;
 html = tmpl(ctx);
            jQuery("#form_distritos_id").html(html);
          
          },'json'); 
*/
        /*
*************************************************************************************
 */
        jQuery("#form_distritos_id").html("<option>Cargando...</option>");
        jQuery.ajax({
            // En data puedes utilizar un objeto JSON, un array o un query string
            data: {
                tipo: 3,
                a: _valor_departamento_id,
                b: _valor,
                c: 0
            },
            //Cambiar a type: POST si necesario
            type: "post",
            // Formato de datos que se espera en la respuesta
            dataType: "jsonp",
            // URL a la que se enviará la solicitud Ajax
            url: _site_url_ajax_ubigeo
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            html = tmpl(ctx);
            jQuery("#form_distritos_id").html(html);
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
    });
}

function sgpteam_distritos_cambiar() {
    jQuery("#form_distritos_id").change(function(event) {
        _valor_departamento_id = departamentos_id;
        _valor_provincia_id = jQuery("#form_provincias_id").val();
        _valor_distrito_id = jQuery(this).val();
        /*
 jQuery.post(_site_url_ajax_ubigeo, 
  { tipo: 4,a:_valor_departamento_id,b:_valor_provincia_id,c:_valor_distrito_id },
   function(data){
     var stemplate = jQuery("#template").html();
 var tmpl = Handlebars.compile(stemplate);
 var ctx = {};
 ctx.rows=data;
 html = tmpl(ctx);
            jQuery("#form_comunidades_id").html(html);
          },'jsonp'); 
*/
        /*
*************************************************************************************
 */
        jQuery("#form_comunidades_id").html("<option>Cargando...</option>");
        jQuery.ajax({
            // En data puedes utilizar un objeto JSON, un array o un query string
            data: {
                tipo: 4,
                a: _valor_departamento_id,
                b: _valor_provincia_id,
                c: _valor_distrito_id
            },
            //Cambiar a type: POST si necesario
            type: "post",
            // Formato de datos que se espera en la respuesta
            dataType: "jsonp",
            // URL a la que se enviará la solicitud Ajax
            url: _site_url_ajax_ubigeo
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            html = tmpl(ctx);
            jQuery("#form_comunidades_id").html(html);
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
    });
}

function sgpteam_buscar() {
    /*
*************************************************************************************
 */
    jQuery("#sgp-buscar").click(function(event) {
        event.preventDefault();
        _dep = departamentos_id;
        _pro = jQuery("#form_provincias_id").val();
        _dis = jQuery("#form_distritos_id").val();
        _com = jQuery("#form_comunidades_id").val();
        /*
 jQuery.post(_site_url_ajax_productores, { com:_com}, function(data){
            jQuery("#sgp-listado-productores").html(data);
          });     
*/
        /*
*************************************************************************************
 */
        jQuery.ajax({
            // En data puedes utilizar un objeto JSON, un array o un query string
            data: {
                departamentos_id: _dep,
                provincias_id: _pro,
                distritos_id: _dis,
                com: _com
            },
            //Cambiar a type: POST si necesario
            type: "post",
            // Formato de datos que se espera en la respuesta
            dataType: "jsonp",
            // URL a la que se enviará la solicitud Ajax
            url: _site_url_ajax_productores
        }).done(function(data, textStatus, jqXHR) {
            var stemplate = jQuery("#template2").html();
            var tmpl = Handlebars.compile(stemplate);
            var ctx = {};
            ctx.rows = data;
            html = tmpl(ctx);
            if (html.length > 4) {
                jQuery("#sgp-listado-productores").html(html);
            } else {
                jQuery("#sgp-listado-productores").html("<p> No hay productores  registrados en esta comunidad </p>");
            }
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
    });
}
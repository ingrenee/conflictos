<div id="imprimir" class="c">

<div class="well well-sm">

	<div class="row">
		<div class="col-sm-2"> <label for="">Nombre del conflicto:</label> </div>
		<div class="col-sm-10">

			<p>
				<?php echo $conflicto['nombre']; ?>
			</p>
		</div>
	</div>

</div>

<!--  monitoreo -->

<div class="well well-sm">

	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Fase del conflicto:</label>
		<div class="col-sm-3">
			 <p><?php echo $row['fase']; ?></p>
		</div>
		<label class=" col-sm-3 control-label">Fecha:</label>
		<div class="col-sm-3">
			<p><?php
$row['fecha'] = date('d/m/Y', strtotime($row['fecha']));
_vi($row, 'fecha');?></p>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-3 control-label">Desencadenante:</label>
		<div class="col-sm-9">
			 <p><?php echo $row['desencadenante']; ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Mecanismo de protesta:</label>
		<div class="col-sm-3">
			 <p><?php echo $row['mecanismo_1']; ?></p>
		</div>
		<div class="col-sm-3">
		<p><?php echo $row['mecanismo_2']; ?></p>
		</div>
	</div>
	 <div class="clearfix"></div>
</div>
<section id="otros_responsables_content">

		<?php
foreach ($responsables as $k => $v): ?>

		<div class="row  ">


			<div class="col-sm-11">
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Responsable del caso:</label>
					<div class="col-sm-9">
						<p><?php echo $v['responsable']; ?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Observaciones:</label>
					<div class="col-sm-9">
					 <p><?php echo $v['observaciones']; ?></p>
					</div>
				</div>

				<hr>
			</div>
		</div>
		<?php endforeach?>

</section>





<!--   fin monitoreo  -->


 </div>

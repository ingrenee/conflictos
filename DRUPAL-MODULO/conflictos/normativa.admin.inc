<?php
/**
 * @file
 * Callbacks de administración del módulo saludar.
 */

/**
 * Define el formulario de opciones de configuración.
 */
function normativa_configuracion() {  
  /*
  $form['conflictos_id_normativa'] = array(
    '#title' => 'ID del  nodo de la normativa',  
    '#type' => 'textarea',
    '#default_value' => variable_get('conflictos_id_normativa', '00'),    
  );
*/
   $form['conflictos_id_departamento'] = array(
    '#title' => 'Ingrese  el  codigo del departamento',  
    '#type' => 'textarea',
    '#default_value' => variable_get('conflictos_id_departamento', '00'),    
  );

      $form['conflictos_url_data'] = array(
    '#title' => 'Ingrese la url   de destino',  
    '#type' => 'textarea',
    '#default_value' => variable_get('conflictos_url_data', 'url'),    
  );

      $form['conflictos_pagina'] = array(
    '#title' => 'Ingrese numero productores por pagina',  
    '#type' => 'textarea',
    '#default_value' => variable_get('conflictos_pagina', '4'),    
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Guardar configuración',
  );
  $form['#submit'][] = 'normativa_configuracion_submit';  
  return $form;
}

/**
 * Valida el formulario de opciones de configuración.
 */
function normativa_configuracion_validate($form, &$form_state) {
 /* if (trim($form_state['values']['conflictos_id_normativa']) == '') {
    form_set_error('conflictos_id_normativa', 'El ID del nodo no puede estar vacio.');
  }
  */
}

/**
 * Procesa el envío del formulario de opciones de configuración.
 */
function normativa_configuracion_submit($form, $form_state) {
 // variable_set('conflictos_id_normativa', $form_state['values']['conflictos_id_normativa']);

    variable_set('conflictos_id_departamento', $form_state['values']['conflictos_id_departamento']);
       variable_set('conflictos_url_data', $form_state['values']['conflictos_url_data']);
  variable_set('conflictos_pagina', $form_state['values']['conflictos_pagina']);

  drupal_set_message("Se han guardado las opciones de configuración.");
}
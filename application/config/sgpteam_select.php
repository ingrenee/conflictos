<?php 
// 1: numerico , 2:literal , 3: date or null

//$config['select']['select_plan_produccion_list']='select X.*,Y.codigo, Y.nombres, Y.paterno, Y.materno from conflictos.plan_produccion X, conflictos.productores Y  where X.productores_dni=Y.dni';


$config['select']['select_conflictos_list']="select X.dni, 
x.nombres, x.paterno, x.materno,
(x.nombres || ' ' || x.paterno || ' '  || x.materno) as nombres,

(select Y.cultivo_nombre from conflictos.cultivos Y where Y.productores_dni=X.dni limit 1  offset 0) as  cultivo_1,
(select Y.cultivo_nombre from conflictos.cultivos Y where Y.productores_dni=X.dni limit 1  offset 1) as  cultivo_2,
(select Y.cultivo_nombre from conflictos.cultivos Y where Y.productores_dni=X.dni limit 1  offset 2) as  cultivo_3,
(select Y.cultivo_nombre from conflictos.cultivos Y where Y.productores_dni=X.dni limit 1  offset 3) as  cultivo_4,

(select Y.mercado_lugar_venta from conflictos.seccion4 Y where Y.productores_dni=X.dni limit 1  offset 0) as  lugar_de_venta,
(select Y.puntos from conflictos.parcelas Y where Y.productores_dni=X.dni limit 1  offset 0) as  puntos,
(select Y.nombre from conflictos.ubigeo2006 Y where Y.id=X.departamentos_id  and  Y.tipo='departamento' limit 1  offset 0) as  departamento,

(select Y.nombre from conflictos.ubigeo2006 Y where Y.id=X.provincias_id    and Y.tipo='provincia' limit 1  offset 0) as  provincia,

(select Y.nombre from conflictos.ubigeo2006 Y where Y.id=X.distritos_id  and   Y.tipo='distrito' limit 1  offset 0) as  distrito
 

  from  conflictos.productores X  ";

  $config['select']['select_productor_parcelas']='select X.productores_dni as dni, X.puntos, X.nombre,X.id from 

conflictos.parcelas X ';
<h4>Actores involucrados</h4>
<div class="form-group">
	<label for="input-id" class="col-sm-3 control-label">
		Acciones GT
	</label>
	<div class="col-sm-3">
		<select name="acciones" class="control-form">
			<option value="no-acciones">0: no acciones /intervenciones</option>
			<option value="tomado-conocimiento">1: Tomado conocimiento</option>
			<option value="revisado">2: Revisado</option>
			<option value="denuncia-hecho">3: Denuncia hecho</option>
			<option value="acciones-frecuentes">4: Acciones frecuentes</option>
		</select>
	</div>
	<div class="form-group">
		<label for="input" class="col-sm-3 control-label">Fecha:</label>
		<div class="col-sm-2">
			<input type="text" name="fecha_2" id="inputFecha_2" class="form-control fecha" value="" required="required" pattern="" title="">
		</div>
	</div>
</div>


<div class="form-group">
	 <label for="" class="control-label col-sm-3">Efecto de acciones</label>
	  <div class="col-sm-9"><input type="text" name="efectos" id="inputEfectos" class="form-control" value="" required="required" pattern="" title=""></div>
</div>

<div class="form-group">
	 <label for="" class="control-label col-sm-3">Acuerdos</label>
	  <div class="col-sm-9"><input type="text" name="acuerdos" id="acuerdos" class="form-control" value="" required="required" pattern="" title=""></div>
</div>

<div class="form-group">
	 <label for="" class="control-label col-sm-3">Responsable</label>
	  <div class="col-sm-9"><input type="text" name="responsable_2" id="responsable" class="form-control" value="" required="required" pattern="" title=""></div>
</div>


<div class="form-group">
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-default" id="boton_form_2_back">Volver</button>
	</div>
	<div class="col-sm-6">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_3">Guardar</button>
	</div>
</div>
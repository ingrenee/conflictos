

 <div class="row">

 	 <div class="col-sm-12">
 	 	 <label> </label>
 	 	  <h2 style="display: block; text-align: center;"> FICHA DE REGISTRO DEL CONFLICTO</h2>
 	 </div>
 </div>
<h4>Actor que reporta el conflicto -  la denuncia</h4>

	<div class="row">
		<div class="col-sm-12">
			<label>Nombres y apellidos:</label>

			<font style="display: inline;"><?php _vi($row, 'actor_reporta_nombre');?>  </font>

		</div>
	</div>

<div class="">
	<div class="row"><div class="col-sm-6">
		<label class="control-label  " for="email">DNI:</label>
		<font><?php _vi($row, 'actor_reporta_dni');?></font>
	</div><div class="col-sm-6">
	<label class="control-label  " for="email">Tipo de actor:</label>
	<font><?php echo $row['actor_reporta_tipo']; ?></font>
</div>
</div>
</div>
<div class="form-group">
<div class="row"><div class="col-sm-12">
<label class="control-label -sm-3" for="email">Observación:</label>
<font><?php _vi($row, 'actor_reporta_observacion');?></font>
</div>
</div>
</div>
<h4>Otras fuentes de información</h4>
<div id="otras_fuentes_content">
<?php if (count($otras) > 0): ?>
<?php foreach ($otras as $kk => $vv):
?>
<div id="otras_fuentes" class="otras_fuentes">
<div class="form-group">
<div class="row">
	<div class="col-sm-9">
		<label class="control-label -sm-3" for="email">Nombres y apellidos:</label>
		<font><?php _vi($vv, 'nombres');?></font>
	</div>
</div>
</div>
<div class="form-group">
<div class="row">
	<div class="col-sm-9">
		<label class="control-label -sm-3" for="email">Tipo:</label>
		<font>(<?php echo $vv['tipo']; ?>),
		</font>
		<br>
		<label>Descripción:</label>
		<font><?php _vi($vv, 'descripcion');?></font>
	</div>
</div>
</div>
<?php if (!empty($vv['documento'])): ?>
	<!--
<div class="form-group">
<label class="control-label -sm-3" for="email">Documento Adjunto:</label>
<div class="col-sm-9">
	<p class="form-control-static">
		<?php echo $vv['documento']; ?>
	</font>
</div></div>-->
<?php else: ?>
<?php endif;?>
<hr>
</div>
<?php endforeach;?>
<?php else: ?>
<?php endif?>
</div>
<div class="well well-sm">
	<h4>Monitoreo</h4>
 	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Fase del conflicto:</label>
		<div class="col-sm-3">
			<select name="fase" class="form-control">
				<option  value="latente">Latente
				</option>
				<option  value="manifiesto">Manifiesto</option>
				<OPTION value="dialogo">Diálogo</OPTION>
				<OPTION value="transformacion">Transformación</OPTION>
			</select>
		</div>
		<label class=" col-sm-3 control-label">Fecha:</label>
		<div class="col-sm-3">
			<input class="fecha form-control" name="fecha">
		</div>
	</div>
	<div class="form-group">
	<label class=" col-sm-3 control-label">Desencadenante:</label>
		<div class="col-sm-9">
			<input type="text" name="desencadenante" id="inputDesencadenante" class="form-control" value="" required="required" pattern="" title="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Mecanismo de protesta:</label>
		<div class="col-sm-3">
			<select name="mecanismo_1" id="mecanismo_1" class="form-control" required="required">
				<option value="institucional">Institucional</option>
				<option value="no-institucional">No Institucional</option>
			</select>
		</div>
		<div class="col-sm-3">
			<select name="mecanismo_2" id="mecanismo_2" class="form-control" required="required">
				<option value="oficios">Oficios</option>
				<option value="cartas">Cartas</option>
				<option value="memoriales">Memoriales</option>
			</select>
		</div>
	</div>

	</div>


	<section id="otras_fuentes_content">
		<div id="otras_fuentes" class="otras_fuentes">

	<div class="form-group">
		<label for="" class="col-sm-3 control-label">Responsable del caso</label>
		<div class="col-sm-9">
			<input type="text" name="responsable[]" id="inputResponsable" class="form-control" value="" required="required" pattern="" title="">
		</div>
	</div>
	<div class="form-group">
		<label for="" class="col-sm-3 control-label">Observaciones</label>
		<div class="col-sm-9">
			<input type="text" name="observaciones[]" id="observaciones" class="form-control" value="" required="required" pattern="" title="">
		</div>
	</div>
			<div class="form-group">
				<div class="col-sm-12">
					<a href="javascript:void(0);" class="btn btn-danger boton_quitar pull-right"> Quitar </a>
				</div>
			</div>
			<hr>
		</div>
	</section>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-9">
		<button type="button" class="btn btn-default btn-block " id="boton_clonar">Agregar </button>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-block btn-primary" id="boton_form_1">Continuar</button>
	</div>
</div>